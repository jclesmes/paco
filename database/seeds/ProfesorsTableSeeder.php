<?php

use Illuminate\Database\Seeder;

class ProfesorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Profesor::create(['id' => 1 ,'nombre_completo' => 'Profesor de Prueba 1', 
        'telefono' => 123123123, 'correo' => 'profesor@prueba.es', 'empresa_id' => 1]);
        App\Profesor::create(['id' => 2,'nombre_completo' => 'Profesor de Prueba 2', 
        'telefono' => 321321321, 'correo' => 'profesor2@prueba.es', 'empresa_id' => 1]);
    }
}

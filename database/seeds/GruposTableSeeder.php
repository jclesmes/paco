<?php

use Illuminate\Database\Seeder;

class GruposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Grupo::create(['id' => 1 ,'nombre' => 'Casa1']);
        App\Grupo::create(['id' => 2 ,'nombre' => 'Casa2']);
        App\Grupo::create(['id' => 3 ,'nombre' => 'Casa3']);

        App\Grupo::create(['id' => 4 ,'nombre' => 'Centro1']);
        App\Grupo::create(['id' => 5 ,'nombre' => 'Centro2']);
        App\Grupo::create(['id' => 6 ,'nombre' => 'Centro3']);
        App\Grupo::create(['id' => 7 ,'nombre' => 'Centro4']);
        App\Grupo::create(['id' => 8 ,'nombre' => 'Centro5']);

        App\Grupo::create(['id' => 9 ,'nombre' => 'Matas1']);
        App\Grupo::create(['id' => 10 ,'nombre' => 'Matas2']);
        App\Grupo::create(['id' => 11 ,'nombre' => 'Matas3']);

        App\Grupo::create(['id' => 12 ,'nombre' => 'Exterior1']);
        App\Grupo::create(['id' => 13 ,'nombre' => 'Exterior2']);
        App\Grupo::create(['id' => 14 ,'nombre' => 'Exterior3']);
    }
}

<?php

use Illuminate\Database\Seeder;

class EmpresasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Empresa::create(['id' => 1 ,'nombre_empresa' => 'Empresa de Prueba 1', 'cif' => 'B123123', 
        'razon_social' => 'Empresa de Prueba 1 S.L.', 'direccion' => 'Calle de prueba', 'provincia' => 'MADRID',
         'ciudad'=>'MADRID', 'pais'=>'ESPAÑA', 'cp' => 28080, 'web' => 'www.prueba.es', 
         'actividad'=>'actividad de prueba','telefono'=>91123123, 'fax'=>91321321]);
    }
}

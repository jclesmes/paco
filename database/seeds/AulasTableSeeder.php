<?php

use Illuminate\Database\Seeder;

class AulasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Aula::create(['id' => 1 ,'nombre' => 'Centro-A1']);
        App\Aula::create(['id' => 2 ,'nombre' => 'Centro-B1']);
        App\Aula::create(['id' => 3 ,'nombre' => 'Centro-B2']);
        App\Aula::create(['id' => 4 ,'nombre' => 'Centro-B3']);
        App\Aula::create(['id' => 5 ,'nombre' => 'Centro-B4']);

        App\Aula::create(['id' => 6 ,'nombre' => 'Centro-Cocina']);
        App\Aula::create(['id' => 7 ,'nombre' => 'Centro-Salón de actos']);
        App\Aula::create(['id' => 8 ,'nombre' => 'Centro-Control 2']);

        App\Aula::create(['id' => 9 ,'nombre' => 'Casa-A1']);
        App\Aula::create(['id' => 10 ,'nombre' => 'Casa-A2']);
        App\Aula::create(['id' => 11 ,'nombre' => 'Casa-A3']);
        App\Aula::create(['id' => 12 ,'nombre' => 'Casa-A4']);
        App\Aula::create(['id' => 13 ,'nombre' => 'Casa-A5']);
        App\Aula::create(['id' => 14 ,'nombre' => 'Casa-A6']);
        App\Aula::create(['id' => 15 ,'nombre' => 'Casa-A7']);
        
        App\Aula::create(['id' => 16 ,'nombre' => 'Matas-A1']);
        App\Aula::create(['id' => 17 ,'nombre' => 'Matas-Salón de actos']);

        App\Aula::create(['id' => 18 ,'nombre' => 'Exterior']);
        
    }
}

<?php

use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Categoria::create(['id' => 1 ,'nombre' => 'Aire libre y familia']);
        App\Categoria::create(['id' => 2 ,'nombre' => 'Música y artes escénicas']);
        App\Categoria::create(['id' => 3 ,'nombre' => 'Imagen y nuevas tecnologías']);
        App\Categoria::create(['id' => 4 ,'nombre' => 'Habilidades domésticas']);
        App\Categoria::create(['id' => 5 ,'nombre' => 'Formación y empleo']);
        App\Categoria::create(['id' => 6 ,'nombre' => 'Más ocio']);
        App\Categoria::create(['id' => 7 ,'nombre' => 'Certámenes']);
        App\Categoria::create(['id' => 8 ,'nombre' => 'Centros escolares']);
    }
}

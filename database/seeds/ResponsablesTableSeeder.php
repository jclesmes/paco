<?php

use Illuminate\Database\Seeder;

class ResponsablesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Responsable::create(['nombre' => 'Ana']);
        App\Responsable::create(['nombre' => 'Ángela']);
        App\Responsable::create(['nombre' => 'Carlos']);
        App\Responsable::create(['nombre' => 'Charlie']);
        App\Responsable::create(['nombre' => 'Cristina']);
        App\Responsable::create(['nombre' => 'Fito']);
        App\Responsable::create(['nombre' => 'Inés']);
        App\Responsable::create(['nombre' => 'J. Carlos']);
        App\Responsable::create(['nombre' => 'Jesús']);
        App\Responsable::create(['nombre' => 'Lourdes']);
        App\Responsable::create(['nombre' => 'Manuel']);
        App\Responsable::create(['nombre' => 'María']);
        App\Responsable::create(['nombre' => 'Trini']);        
    }
}

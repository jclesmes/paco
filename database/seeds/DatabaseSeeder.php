<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(CampaniasTableSeeder::class);
        //$this->call(EmpresasTableSeeder::class);
        //$this->call(ProfesorsTableSeeder::class);
        //$this->call(ContactosTableSeeder::class);
        $this->call(AulasTableSeeder::class);
        $this->call(ResponsablesTableSeeder::class);
        $this->call(GruposTableSeeder::class);
        $this->call(CategoriasTableSeeder::class);
    }
}

<?php

use Illuminate\Database\Seeder;

class ContactosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Contacto::create(['empresa_id' => 1, 'id' => 1, 'nombre_completo' => 'Jóse María Fayos Mestre', 'telefono' => 915348121, 'movil' => 606583671, 'correo' => 'AJEDREZBYN@GMIAL.COM', 'puesto' => 'Formador']);
        App\Contacto::create(['empresa_id' => 1, 'id' => 2, 'nombre_completo' => 'Lucely Orozco Soto', 'movil' => 687262553, 'correo' => 'costurita.cr@gmail.com', 'puesto' => 'Formadora']);
        App\Contacto::create(['empresa_id' => 1, 'id' => 3, 'nombre_completo' => 'María Feduchi', 'correo' => 'asociacionmentespositivas@gmail.com', 'puesto' => 'Formadora']);
        App\Contacto::create(['empresa_id' => 1, 'id' => 4, 'nombre_completo' => 'Álvaro Ponte García', 'movil' => 618741651, 'correo' => 'defensaymeditacion@hotmail.com', 'puesto' => 'Formador']);
        App\Contacto::create(['empresa_id' => 1, 'id' => 5, 'nombre_completo' => 'Alfredo Ruíz García', 'telefono' => 918284315, 'movil' => 670388300, 'correo' => 'info@escuelacaninalatejera.es', 'puesto' => 'Formador']);
        App\Contacto::create(['empresa_id' => 1, 'id' => 6, 'nombre_completo' => 'Daniel Reverte', 'telefono' => 910133824, 'movil' => 677928910, 'correo' => 'dreverte@kitchenacademy.es', 'puesto' => 'Formador']);
        App\Contacto::create(['empresa_id' => 1, 'id' => 7, 'nombre_completo' => 'Pedro del Tanago', 'movil' => 620171187, 'correo' => 'info@amadablam.es', 'puesto' => 'Formador']);
        App\Contacto::create(['empresa_id' => 1, 'id' => 8, 'nombre_completo' => 'Andrés Jiménez Caballero', 'movil' => 609904824, 'correo' => 'andresjimenezcaballero@gmail.com', 'puesto' => 'Formador']);
        App\Contacto::create(['empresa_id' => 1, 'id' => 9, 'nombre_completo' => 'Brigida Higueras Madsen', 'movil' => 619211214, 'correo' => 'brigittehmadsen@gmail.com', 'puesto' => 'Formadora']);
        App\Contacto::create(['empresa_id' => 1, 'id' => 10, 'nombre_completo' => 'Javier Muñoz', 'telefono' => 912612044, 'movil' => 692690213, 'correo' => 'javier@aguanorte.com', 'puesto' => 'Gerente']);
        App\Contacto::create(['empresa_id' => 1, 'id' => 11, 'nombre_completo' => 'Rocio Fuente Hidalgo', 'telefono' => 913860773, 'correo' => 'rocio@controlmicrobiologico.com', 'puesto' => 'Formadora']);
        App\Contacto::create(['empresa_id' => 1, 'id' => 12, 'nombre_completo' => 'Jorge Arias Martín', 'telefono' => 902881995, 'movil' => 627344866, 'correo' => 'jorge.arias@dioxinet.com', 'puesto' => 'Gerente']);
        App\Contacto::create(['empresa_id' => 1, 'id' => 13, 'nombre_completo' => 'Juan Pedro Llerena', 'telefono' => 917104182, 'movil' => 645293482, 'correo' => 'jllerena@laroboteca.es', 'puesto' => 'Responsable Dpto. tecnológico Respira']);
        App\Contacto::create(['empresa_id' => 1, 'id' => 14, 'nombre_completo' => 'María Sánchez Lillo', 'telefono' => 687405651, 'movil' => 638525425, 'correo' => 'lunaresblancos@hotmail.es', 'puesto' => 'Oficina']);
        App\Contacto::create(['empresa_id' => 1, 'id' => 15, 'nombre_completo' => 'Javier Martínez e Inma Palenzuela ', 'telefono' => 670294678, 'movil' => 672341681, 'correo' => 'javierm@utopia21.es', 'puesto' => 'Coordinación y Recursos Humanos']);
    }
}

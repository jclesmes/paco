<?php

use Illuminate\Database\Seeder;

class CampaniasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Campania::create(['id' => 0, 'nombre' => 'Campaña por defecto']);
    }
}

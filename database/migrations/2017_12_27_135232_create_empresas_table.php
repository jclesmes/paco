<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('empresas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_empresa', 100);
            $table->string('cif', 100)->nullable();
            $table->string('razon_social', 100)->nullable();
            $table->string('direccion', 100)->nullable();
            $table->string('provincia', 100)->nullable();
            $table->string('ciudad', 100)->nullable();
            $table->string('pais', 100)->nullable();
            $table->integer('cp')->nullable();
            $table->string('web', 100)->nullable();
            $table->string('correo', 100)->nullable();
            $table->string('actividad', 100)->nullable();
            $table->integer('telefono')->nullable();
            $table->integer('fax')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}

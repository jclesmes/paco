<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActividadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('actividades', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('campania_id')->unsigned()->nullable();
            $table->foreign('campania_id')->references('id')->on('campanias')->onDelete('set null');
            $table->string('nombre', 100);
            $table->integer('grupo_id')->unsigned()->nullable();
            $table->foreign('grupo_id')->references('id')->on('grupos')->onDelete('set null');
            $table->date('fecha_inicio')->nullable();
            $table->date('fecha_fin')->nullable();
            $table->integer('par_min')->nullable();
            $table->integer('par_max')->nullable();
            $table->integer('precio')->nullable();
            $table->integer('edad_minima')->nullable();
            $table->integer('edad_maxima')->nullable();
            $table->integer('aula_id')->unsigned()->nullable();
            $table->foreign('aula_id')->references('id')->on('aulas')->onDelete('set null');
            $table->date('fecha_inscripciones')->nullable();
            $table->time('hora_inicio')->nullable();
            $table->time('hora_fin')->nullable();
            $table->text('incluye')->nullable();
            $table->string('dias', 100)->nullable();
            $table->text('texto_previo_folleto')->nullable();
            $table->string('lugar_celebracion', 100)->nullable();
            $table->text('observaciones')->nullable();
            $table->integer('categoria_id')->unsigned()->nullable();
            $table->foreign('categoria_id')->references('id')->on('categorias')->onDelete('set null');
            $table->integer('responsable_id')->unsigned()->nullable();
            $table->foreign('responsable_id')->references('id')->on('responsables')->onDelete('set null');
            $table->integer('profesor_id')->unsigned()->nullable();
            $table->foreign('profesor_id')->references('id')->on('profesors')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actividades');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetalicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metalicos', function (Blueprint $table) {
            $table->increments('id');
            $table->char('concepto', 100);
            $table->char('notas', 100)->nullable();
            $table->date('fecha_necesidad');
            $table->date('fecha_justificacion')->nullable();
            $table->decimal('importe', 8, 2);
            $table->decimal('dinero_gastado', 8, 2)->nullable();
            $table->integer('actividad_id')->unsigned()->nullable();
            $table->foreign('actividad_id')->references('id')->on('actividades')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metalicos');
    }
}

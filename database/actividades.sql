-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 26-01-2018 a las 14:19:31
-- Versión del servidor: 5.5.59-log
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `actividades`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividades`
--

CREATE TABLE `actividades` (
  `id` int(10) UNSIGNED NOT NULL,
  `campania_id` int(10) UNSIGNED DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `grupo_id` int(10) UNSIGNED DEFAULT NULL,
  `fecha_inicio` datetime DEFAULT NULL,
  `fecha_fin` datetime DEFAULT NULL,
  `par_min` int(11) DEFAULT NULL,
  `par_max` int(11) DEFAULT NULL,
  `precio` int(11) DEFAULT NULL,
  `edad_minima` int(11) DEFAULT NULL,
  `edad_maxima` int(11) DEFAULT NULL,
  `aula_id` int(10) UNSIGNED DEFAULT NULL,
  `fecha_inscripciones` datetime DEFAULT NULL,
  `hora_inicio` time DEFAULT NULL,
  `hora_fin` time DEFAULT NULL,
  `incluye` text COLLATE utf8mb4_unicode_ci,
  `dias` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `texto_previo_folleto` text COLLATE utf8mb4_unicode_ci,
  `lugar_celebracion` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `observaciones` text COLLATE utf8mb4_unicode_ci,
  `categoria_id` int(10) UNSIGNED DEFAULT NULL,
  `responsable_id` int(10) UNSIGNED DEFAULT NULL,
  `profesor_id` int(10) UNSIGNED DEFAULT NULL,
  `coste_estimado_maximo` int(11) DEFAULT NULL,
  `coste_real` int(11) DEFAULT NULL,
  `hecha` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `actividades`
--

INSERT INTO `actividades` (`id`, `campania_id`, `nombre`, `grupo_id`, `fecha_inicio`, `fecha_fin`, `par_min`, `par_max`, `precio`, `edad_minima`, `edad_maxima`, `aula_id`, `fecha_inscripciones`, `hora_inicio`, `hora_fin`, `incluye`, `dias`, `texto_previo_folleto`, `lugar_celebracion`, `observaciones`, `categoria_id`, `responsable_id`, `profesor_id`, `coste_estimado_maximo`, `coste_real`, `hecha`) VALUES
(1, 1, 'Bateria', 4, '2018-04-13 00:00:00', '2018-06-15 00:00:00', 4, 6, 50, 16, 25, 7, '2018-04-06 00:00:00', '09:15:00', '12:00:00', 'profesor y materiales', 'v', 'ven a tocar la bateria y aprende los ritmos básicos', NULL, NULL, 2, 7, 1, NULL, NULL, 0),
(3, 1, 'Ajedrez iniciación', 4, '2018-01-26 00:00:00', '2018-02-09 00:00:00', 8, 18, 50, 6, 14, 2, '2018-01-19 00:00:00', '10:00:00', '11:20:00', 'tableros', 's', 'ven a jugar al ajedrez', NULL, NULL, 6, 6, 1, NULL, NULL, 0),
(4, 1, 'Corte y confección', 4, '2018-02-02 00:00:00', '2018-02-23 00:00:00', 8, 14, NULL, 18, 35, 1, '2018-01-19 00:00:00', '10:00:00', '13:00:00', NULL, 's', 'Aprende todos los pasos para la elaboración de una prenda', NULL, NULL, 4, 1, NULL, NULL, NULL, 0),
(5, 1, 'monitor', 1, '2018-01-12 00:00:00', '2018-01-16 00:00:00', 5, 15, 78, 4, 8, 1, '2018-01-02 00:00:00', '00:00:00', '02:00:00', NULL, 'v', NULL, NULL, NULL, 5, 1, NULL, NULL, NULL, 0),
(6, 1, 'curso de foto', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '00:00:00', '00:00:00', NULL, NULL, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, 0),
(7, 1, 'certamen de relato corto', 1, '2018-01-10 00:00:00', '2018-01-08 00:00:00', NULL, NULL, NULL, NULL, NULL, 1, NULL, '00:00:00', '00:00:00', NULL, NULL, NULL, NULL, NULL, 7, 1, NULL, NULL, NULL, 0),
(8, 1, 'Dinamizadores', 1, '2018-01-09 00:00:00', '2018-01-16 00:00:00', NULL, NULL, NULL, NULL, NULL, 1, NULL, '00:00:00', '00:00:00', NULL, NULL, NULL, NULL, NULL, 8, 1, NULL, NULL, NULL, 0),
(9, 1, 'camarero', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, '00:00:00', '00:00:00', NULL, NULL, NULL, NULL, NULL, 5, 1, NULL, NULL, NULL, 0),
(10, 1, 'Dinamizadores2', 1, '2018-01-09 00:00:00', '2018-01-16 00:00:00', NULL, NULL, NULL, NULL, NULL, 1, NULL, '00:00:00', '00:00:00', NULL, NULL, NULL, NULL, NULL, 8, 1, NULL, NULL, NULL, 0),
(11, 1, 'Construcción de iglús 2.0', 12, '2018-03-05 00:00:00', '2018-03-05 00:00:00', 12, 20, 50, 6, 99, 1, '2018-01-26 00:00:00', '09:00:00', '12:00:00', 'Monitores expertos, material grupal, seguros', 's', 'Construye tu propio igul y aprende como viven los esquimales', 'Sierra de Guadarrama', NULL, 1, 10, NULL, NULL, NULL, 0),
(12, NULL, 'Actividad 1', 4, '2019-01-02 00:00:00', '2019-01-26 00:00:00', 8, 12, 20, 17, NULL, 1, '2018-01-01 00:00:00', '20:00:00', '21:00:00', 'Lo que incluye la actividad', 'l,m,s', 'Texto para el folleto', NULL, 'Traer portátil para programar', 1, 2, 1, 1500, NULL, 0),
(13, NULL, 'Actividad 2', 5, '2019-01-02 00:00:00', '2019-01-26 00:00:00', 8, 12, 20, 17, NULL, 1, '2018-01-01 00:00:00', '21:00:00', '22:00:00', 'Lo que incluye la actividad', 'l,m,s', 'Texto para el folleto', NULL, 'Traer portátil para programar', 1, 2, 1, 1500, NULL, 0),
(14, 1, 'Dinamiz<adores', 1, '2018-01-10 00:00:00', '2018-01-25 00:00:00', 7, 60, 0, 11, 18, 11, '2018-01-11 00:00:00', '16:00:00', '19:00:00', 'kdhnkjhfrkjfkjaf', 'l,j', 'gddztfdujfjyfgj', NULL, 'hnfghfgh', 8, 2, NULL, 0, NULL, 0),
(15, 3, 'Educación canina en familia', 12, '2018-05-15 00:00:00', '2018-06-21 00:00:00', 6, 12, 110, 6, NULL, 18, '2018-05-08 00:00:00', '19:00:00', '20:30:00', 'Formadores y material común.', 'm,j', 'Si tu perro es uno más de la familia y quieres enseñarle y aprender con él, este curso te ofrece participar activamente en su adiestramiento y disfrutar de la educación canina.', 'Parque Canino el Cantizal', 'Necesario presentar una fotocopia de vacunación actualizada del perro.', 1, 2, 3, 2052, NULL, 0),
(16, 3, 'Cremas e higiene personal', 4, '2018-04-07 00:00:00', '2018-04-07 00:00:00', 6, 10, 12, 18, 35, 6, '2018-03-30 00:00:00', '10:30:00', '13:30:00', NULL, 's', 'Recetas básicas y sencillas para la elaboración de cremas para distintos tipos de piel, gel, champú y protector labial.', NULL, 'Recordatorio: traed botes de cristal pequeños de casa para poder llevaros algunas muestras.', 4, 1, 4, 135, NULL, 0),
(17, 3, 'Jabones naturales', 4, '2018-04-21 00:00:00', '2018-04-21 00:00:00', 6, 10, 12, 18, 35, 6, '2018-03-30 00:00:00', '10:30:00', '13:30:00', NULL, 's', 'Recetas básicas y sencillas para la elaboración de jabones de glicerina, jabones artesanos y bombas de baño.', NULL, NULL, 4, 1, 4, 135, NULL, 0),
(18, 3, 'Corte y confección', 4, '2018-05-05 00:00:00', '2018-06-16 00:00:00', 10, 14, 64, 18, 35, 1, '2018-04-27 00:00:00', '10:30:00', '13:30:00', NULL, 's', 'Un taller en el que aprenderás técnicas básicas de costura a mano y a máquina para desarrollar proyectos textiles a nivel doméstico.', NULL, 'Recordatorio: llevar máquina de coser y costurero básico (quien tenga).', 4, 1, 5, 960, NULL, 0),
(19, 3, 'El rimadero', 1, '2018-04-09 00:00:00', '2018-06-18 00:00:00', 10, 15, 5, 12, 18, 9, '2018-03-30 00:00:00', '17:30:00', '19:30:00', NULL, 'l', 'Aprenderás técnicas de rima, recursos y trucos para componer tus propias rimas de rap, a la vez que reflexionas con otros jóvenes sobre distintos temas de discriminación: bullyng, violencia de género,…', NULL, NULL, 2, 1, 6, 756, NULL, 0),
(20, 3, 'Intercambio de idiomas', 1, '2018-04-09 00:00:00', '2018-06-18 00:00:00', 8, 14, 0, 16, 35, 13, '2018-03-30 00:00:00', '18:30:00', '20:00:00', NULL, 'l', 'Espacio para mejorar tu expresión oral en otros idiomas a la vez que conoces gente joven con la misma motivación.', NULL, 'En función de las características de los participantes y de los idiomas que deseen practicar, se verá la forma más idónea de estructurar las sesiones, se definirá la dinámica semanal, se harán grupos,…  En caso de que no haya jóvenes nativos, en lugar de hacer intercambio un intercambio real nos plantearemos otras alternativas como crear un grupo de conversación entre los jóvenes asistentes. No son clases de idiomas, es un espacio de intercambio. El grupo lo conformarán los participantes a esta actividad; no habrá ningún profesor de idiomas presente.  \r\n\r\nLo que salio el trimestre pasado fue:\r\n18.30h-19.00h: español\r\n19.00-20.00h: inglés\r\nCada día, uno de los participantes se prepara el tema del debate.', 6, 1, 7, 0, NULL, 0),
(21, 3, 'Premonitores de tiempo libre', 4, '2018-04-13 00:00:00', '2018-05-25 00:00:00', 15, 20, 30, 14, 16, 7, '2018-04-06 00:00:00', '17:00:00', '20:00:00', 'Incluye: formadoras especializadas, material, seguro y alojamiento, manutención y transporte durante la salida.', 'v', '¿Quieres prepararte para ser monitor de tiempo libre? En este curso desarrollarás tus capacidades, aprenderás técnicas, juegos, canciones, talleres, veladas,… y compartirás la experiencia con otros jóvenes como tú.', NULL, 'Cuando vengan a inscribirse hay que entregarles la HOJA INFORMATIVA DE LA SALIDA y que rellenen la FICHA MÉDICA específica de premonis.', 5, 1, 7, 700, NULL, 0),
(22, 3, 'Dinamizadores', 1, '2017-10-06 00:00:00', '2018-06-28 00:00:00', 1, 30, 0, 12, 18, 11, NULL, '16:30:00', '17:30:00', 'Formadoras y material común.', 'j', 'Dirigido a jóvenes de instituto que quieran aprender a organizar actividades dentro y fuera de su instituto.', NULL, 'Las sesiones de los jueves son gratuitas y las salidas y otras actividades que se realicen pueden tener un coste adicional.', 5, 10, 9, 0, NULL, 0),
(23, 3, 'Cocina infantil centro 1', 4, '2018-05-04 00:00:00', '2018-05-18 00:00:00', 8, 10, 25, 8, 12, 6, '2018-03-30 00:00:00', '18:00:00', '19:30:00', 'Ingredientes grupales y menaje de cocina', 'v', 'Un taller de tres sesiones prácticas donde aprender a elaborar menús caseros, sanos y divertidos.', NULL, NULL, 4, 4, 15, 363, NULL, 0),
(24, 3, 'Cocina infantil centro 2', 5, '2018-06-01 00:00:00', '2018-06-15 00:00:00', 8, 10, 25, 8, 12, 6, '2018-03-30 00:00:00', '18:00:00', '19:30:00', 'Ingredientes grupales y menaje de cocina', 'v', 'Un taller de tres sesiones prácticas donde aprender a elaborar menús caseros, sanos y divertidos.', NULL, NULL, 4, 4, 15, 363, NULL, 0),
(25, 3, 'Cocina infantil Matas', 9, '2018-04-06 00:00:00', '2018-04-20 00:00:00', 8, 10, 25, 8, 12, 18, '2018-03-30 00:00:00', '18:00:00', '19:30:00', 'Ingredientes grupales y menaje de cocina', 'v', 'Un taller de tres sesiones prácticas donde aprender a elaborar menús caseros, sanos y divertidos.', NULL, NULL, 4, 4, 15, 363, NULL, 0),
(26, 3, 'Fotografía práctica iniciación', 4, '2018-05-19 00:00:00', '2018-06-09 00:00:00', 7, 15, 40, 14, 30, 7, '2018-03-30 00:00:00', '10:00:00', '12:00:00', 'Actividades prácticas', 's', 'Taller totalmente práctico con profesores jóvenes para aprender fotografía de forma dinámica, sin contenido teórico y aprendiendo mientras te diviertes.', NULL, 'En esta actividad está especialmente diseñada para acercar la fotografía a los jóvenes de manera práctica, dinámica con la ayuda de profesores jóvenes con experiencia en el campo audiovisual, aprenderán las técnicas básicas de la fotografía desde la práctica pura, sin temarios y con contenidos teóricos mismos. A través de actividades especialmente diseñadas, irán descubriendo ellos mismos los mecanismos de la fotografía y se valdrán de ella para experimentar su creatividad y su forma de transmitir, de comunicarse y de contar cosas al mundo.', 3, 3, 10, 550, NULL, 0),
(27, 3, 'Fotografía práctica avanzado', 5, '2018-05-19 00:00:00', '2018-06-09 00:00:00', 7, 15, 40, 14, 30, 7, '2018-05-11 00:00:00', '12:00:00', '14:00:00', 'Actividades prácticas', 's', 'Si has hecho el curso de iniciación, éste es tu taller. Podrás seguir aprendiendo, practicando y sobre todo pasándotelo en grande con la cámara en mano. Prácticas avanzadas para fotógrafos dinámicos.', NULL, 'La descripción es exactamente igual que para el taller de iniciación, pero este taller está recomendado para quienes ya hayan hecho la primera parte o quieres tengan unos conocimientos avanzado y consolidados de un primer año de fotografía. No será un taller técnico, sino un taller para seguir practicando.', 3, 3, 10, 550, NULL, 0),
(28, 3, 'Guitarra iniciación', 9, '2018-04-11 00:00:00', '2018-06-13 00:00:00', 2, 6, 50, 13, 17, 18, '2018-04-04 00:00:00', '18:45:00', '19:45:00', NULL, 'x', 'Comienza a tocar la guitarra y rápidamente aprenderás unas cuantas canciones y lo más básico para tocar varios acordes.', NULL, 'Cuando se llega al segundo se le puede cobrar y hay que avisar al primero, o a Juan Carlos para que lo haga él.', 2, 8, 11, NULL, NULL, 0),
(29, 3, 'Foto y vídeo con el móvil', 6, '2018-05-12 00:00:00', '2018-05-12 00:00:00', 6, 12, 30, 14, 30, 7, '2018-05-04 00:00:00', '10:00:00', '14:00:00', 'Actividades y prácticas para exprimir la cámara de tu móvil.', 's', 'Un taller adaptado a la palma de la mano, con él aprenderemos a sacarle partido a las fotografías que realizamos en el día a día con nuestra tableta o teléfono móvil. También analizaremos diversas aplicaciones de edición digital básica basadas en dichos dispositivos portátiles.Conceptos básicos de grabación audiovisual aplicados al móvil. Grabando pequeños clips podrás editar y crear un video desde tu móvil, sin ordenador ni complicaciones.', NULL, 'En esta actividad aprenderán a hacer fotografías y vídeos con dispositivos móviles, utilizando los controles de cámara que traen estos dispositivos. Los asistentes solo deberán traer el móvil propio. Aquellos cuyos móviles dispongan de mayores opciones manuales podrán sacar myor provecho pero cualquier móvil con cámara bastará para hacer el taller.', 3, 3, 10, 240, NULL, 0),
(30, 3, 'Book de fotos de estudio', 7, '2018-04-21 00:00:00', '2018-04-21 00:00:00', 6, 12, 15, 14, 30, 7, '2018-04-13 00:00:00', '12:00:00', '14:00:00', 'Fondos de estudio fotográfico, iluminación profesional de estudio, fotógrafo profesional con cámara de alta gama, dirección de sesión, edición de fotografías para los participantes, envío de fotografías editadas a los participantes.', 's', '¿te gustaría hacerse un book de fotos en un estudio con iluminación profesional y luego poderte llevar las fotos a casa? ¡Apúntate a este taller!.', NULL, 'En esta actividad de dos horas y media comenzará con una bienvenida a los participantes y explicación de la actividad. Más tarde se explicará el funcionamiento básico de una sesión de moda y se enseñarán algunos ejemplos de las fotografías que se van a realizar. Se procederá a la elección de vestuario y orden de turnos de posados. Se explicarán las nociones básicas de poses de estudios y se comenzará con la actividad donde los participantes podrán ser dirigidos por un fotógrafo profesional para realizar una sesión de fotos en clave dinámica y haciéndola divertida acorde a un grupo joven. En ella se realizarán muchas tomas tanto individuales como colectivas según los participantes lo deseen. Se realizarán varias rondas probando diferente esquemas de iluminación.', 3, 3, 10, 120, NULL, 0),
(31, 3, 'Guitarra iniciación', 10, '2018-04-11 00:00:00', '2018-06-13 00:00:00', 2, 6, 50, 18, 35, 18, '2018-04-04 00:00:00', '19:45:00', '20:45:00', NULL, 'x', 'Comienza a tocar la guitarra y rápidamente aprenderás unas cuantas canciones y lo más básico para tocar varios acordes.', NULL, 'Cuando se llega al segundo se le puede cobrar y hay que avisar al primero, o a Juan Carlos para que lo haga él.', 2, 8, 11, NULL, NULL, 0),
(32, 3, 'Microficción literaria para jóvenes', 1, '2018-05-18 00:00:00', '2018-05-25 00:00:00', 7, 10, 25, 14, 30, 10, '2018-05-11 00:00:00', '17:30:00', '20:00:00', 'Juegos y dinámicas de escritura. Actividades relacionadas con el mundo audiovisual, videos, literatura en el hiphop, música, películas.', 'v', 'En este taller podrás explotar tu creatividad escribiendo microficción, jugando con la imaginación y las palabras, creando un mundo nuevo o describiendo los detalles del actual. A través de diferentes actividades prácticas iremos disfrutando de escribir y dar vida a aquellos personajes, sentimientos o expresiones que viven dentro de nuestro lado creativo.', NULL, 'Es una actividad que ha sido recomendada por varios profesores de Lengua y Literatura de la Comunidad de Madrid hacia sus alumnos y clasificada como “lo que debería hacerse en una clase de lengua y literatura”, ya que a través de actividades en un lenguaje joven, los participantes podrán redescubrir la escritura en el mundo que les rodea: en las letras de la música que escuchan, en el cine y películas que ven, en las redes sociales que utilizan. La literatura está en todas partes y podrán descubrir la belleza de leerla, interpretarla y entenderla. O también a través de actividades muy dinámicas podrán poner a prueba su creatividad dejando salir esos personajes o sentimientos que llevan dentro o dando vida a nuevas formas de expresarse. Quienes lo prueban, repiten.', 6, 3, 10, 175, NULL, 0),
(33, 3, 'Imagen digital creativa-Photoshop iniciación', 1, '2018-05-12 00:00:00', '2018-06-09 00:00:00', 7, 10, 25, 13, 18, 9, '2018-05-04 00:00:00', '12:00:00', '14:00:00', 'Juegos y dinámicas de escritura. Actividades relacionadas con el mundo audiovisual, videos, literatura en el hiphop, música, películas.', 's', 'Curso de edición de imagen y arte digital. Cómo usar las herramientas adoptando buenas costumbres.', NULL, 'Atajos de teclado.Tamaños de imagen según finalidad. Preparar imagen para imprimir. Niveles y curvas. Tono y saturación. Maneras de seleccionar. Cómo recortar cómodamente. Estilos de capa. Máscara de capa. Clonar y parchear', 3, 3, 13, 242, NULL, 0),
(34, 3, 'Imagen digital creativa-Photoshop pinceles personalizados', 1, '2018-05-25 00:00:00', '2018-05-25 00:00:00', 7, 10, 12, 13, 18, 9, '2018-05-04 00:00:00', '17:00:00', '19:00:00', 'Ordenadores, Photoshop CS2', 'v', 'Curso de edición de imagen y arte digital. aprende a usar los pinceles y personalizarlos.', NULL, 'Es una clase única sobre un aspecto particular del programa photoshop`.', 3, 3, 13, 182, NULL, 0),
(35, 3, 'Imagen digital creativa-Photoshop anaglifos 3D', 1, '2018-06-09 00:00:00', '2018-06-09 00:00:00', 7, 10, 12, 13, 18, 9, '2018-06-01 00:00:00', '12:00:00', '14:00:00', 'Ordenadores, Photoshop CS2', 's', 'Curso de edición de imagen y arte digital. aprende a crear imágenes 3D para ser vistas con las gafas para tres dimensiones', NULL, 'Es una clase única sobre un aspecto particular del programa photoshop.', 3, 3, 13, 182, NULL, 0),
(36, 3, 'Comprender el arte contemporáneo', 1, '2018-06-09 00:00:00', '2018-06-09 00:00:00', 7, 10, 15, 13, 18, 11, '2018-06-01 00:00:00', '17:00:00', '19:00:00', 'Ordenadores, Photoshop CS2', 'v', 'Curso de edición de imagen y arte digital. aprende a crear imágenes 3D para ser vistas con las gafas para tres dimensiones', NULL, 'Es una clase única sobre un aspecto particular del programa photoshop.', 6, 3, 13, 242, NULL, 0),
(37, 3, 'Guitarra iniciación', 1, '2018-04-12 00:00:00', '2018-06-14 00:00:00', 2, 6, 50, 13, 17, 10, '2018-04-04 00:00:00', '18:00:00', '19:00:00', NULL, 'j', 'Comienza a tocar la guitarra y rápidamente aprenderás unas cuantas canciones y lo más básico para tocar varios acordes.', NULL, 'Cuando se llega al segundo se le puede cobrar y hay que avisar al primero, o a Juan Carlos para que lo haga él.', 2, 8, 11, NULL, NULL, 0),
(38, 3, 'Guitarra iniciación', 2, '2018-04-12 00:00:00', '2018-06-14 00:00:00', 2, 6, 50, 13, 17, 10, '2018-04-04 00:00:00', '19:00:00', '20:00:00', NULL, 'j', 'Comienza a tocar la guitarra y rápidamente aprenderás unas cuantas canciones y lo más básico para tocar varios acordes.', NULL, 'Cuando se llega al segundo se le puede cobrar y hay que avisar al primero, o a Juan Carlos para que lo haga él.', 2, 8, 11, NULL, NULL, 0),
(39, 3, 'Guitarra iniciación', 4, '2018-04-13 00:00:00', '2018-06-15 00:00:00', 2, 6, 50, 18, 35, 8, '2018-04-04 00:00:00', '12:00:00', '13:00:00', NULL, 'v', 'Comienza a tocar la guitarra y rápidamente aprenderás unas cuantas canciones y lo más básico para tocar varios acordes.', NULL, 'Cuando se llega al segundo se le puede cobrar y hay que avisar al primero, o a Juan Carlos para que lo haga él.', 2, 8, 11, NULL, NULL, 0),
(40, 3, 'Guitarra iniciación', 5, '2018-04-13 00:00:00', '2018-06-15 00:00:00', 2, 6, 50, 13, 17, 8, '2018-04-04 00:00:00', '16:00:00', '17:00:00', NULL, 'v', 'Comienza a tocar la guitarra y rápidamente aprenderás unas cuantas canciones y lo más básico para tocar varios acordes.', NULL, 'Cuando se llega al segundo se le puede cobrar y hay que avisar al primero, o a Juan Carlos para que lo haga él.', 2, 8, 11, NULL, NULL, 0),
(41, 3, 'Guitarra iniciación', 6, '2018-04-13 00:00:00', '2018-06-15 00:00:00', 2, 6, 50, 13, 17, 8, '2018-04-04 00:00:00', '18:00:00', '19:00:00', NULL, 'v', 'Comienza a tocar la guitarra y rápidamente aprenderás unas cuantas canciones y lo más básico para tocar varios acordes.', NULL, 'Cuando se llega al segundo se le puede cobrar y hay que avisar al primero, o a Juan Carlos para que lo haga él.', 2, 8, 11, NULL, NULL, 0),
(42, 3, 'Guitarra iniciación', 7, '2018-04-14 00:00:00', '2018-06-16 00:00:00', 2, 6, 50, 13, 17, 8, '2018-04-04 00:00:00', '10:00:00', '11:00:00', NULL, 's', 'Comienza a tocar la guitarra y rápidamente aprenderás unas cuantas canciones y lo más básico para tocar varios acordes.', NULL, 'Cuando se llega al segundo se le puede cobrar y hay que avisar al primero, o a Juan Carlos para que lo haga él.', 2, 8, 11, NULL, NULL, 0),
(43, 3, 'Guitarra iniciación', 8, '2018-04-14 00:00:00', '2018-06-16 00:00:00', 2, 6, 50, 13, 17, 5, '2018-04-04 00:00:00', '12:00:00', '13:00:00', NULL, 's', 'Comienza a tocar la guitarra y rápidamente aprenderás unas cuantas canciones y lo más básico para tocar varios acordes.', NULL, 'Cuando se llega al segundo se le puede cobrar y hay que avisar al primero, o a Juan Carlos para que lo haga él.', 2, 7, 12, NULL, NULL, 0),
(44, 3, 'Guitarra intermedio', 1, '2018-04-12 00:00:00', '2018-06-14 00:00:00', 2, 6, 50, 13, 18, 10, '2018-04-04 00:00:00', '20:00:00', '21:00:00', NULL, 'j', 'Si ya sabes tocar escalas, te salen bien las cejillas y quieres seguir aprendiendo, este es tu taller.', NULL, 'Cuando se llega al segundo se le puede cobrar y hay que avisar al primero, o a Juan Carlos para que lo haga él. La prueba de nivel para los nuevos se realizará el 23 de marzo a partir de las 18 h.', 2, 8, 11, NULL, NULL, 0),
(45, 3, 'Guitarra intermedio', 4, '2018-04-13 00:00:00', '2018-06-15 00:00:00', 2, 6, 50, 13, 18, 8, '2018-04-04 00:00:00', '17:00:00', '18:00:00', NULL, 'v', 'Si ya sabes tocar escalas, te salen bien las cejillas y quieres seguir aprendiendo, este es tu taller.', NULL, 'Cuando se llega al segundo se le puede cobrar y hay que avisar al primero, o a Juan Carlos para que lo haga él. La prueba de nivel para los nuevos se realizará el 23 de marzo a partir de las 18 h.', 2, 8, 11, NULL, NULL, 0),
(46, 3, 'Guitarra intermedio', 5, '2018-04-14 00:00:00', '2018-06-16 00:00:00', 2, 6, 50, 13, 18, 8, '2018-04-04 00:00:00', '12:00:00', '14:00:00', NULL, 's', 'Si ya sabes tocar escalas, te salen bien las cejillas y quieres seguir aprendiendo, este es tu taller.', NULL, 'Cuando se llega al segundo se le puede cobrar y hay que avisar al primero, o a Juan Carlos para que lo haga él. La prueba de nivel para los nuevos se realizará el 23 de marzo a partir de las 18 h.', 2, 8, 11, NULL, NULL, 0),
(47, 3, 'Guitarra avanzado', 4, '2018-04-13 00:00:00', '2018-06-15 00:00:00', 2, 8, 50, 13, 25, 8, '2018-04-04 00:00:00', '19:00:00', '21:00:00', NULL, 'v', 'Ven a perfeccionar tu manera de tocar en este taller pensado para aquellos que ya llevan tiempo tocando.', NULL, 'Se puede cobrar desde el primero. La prueba de nivel para los nuevos se realizará el 23 de marzo a partir de las 18 h.', 2, 8, 11, NULL, NULL, 0),
(48, 3, 'Guitarra infantil', 9, '2018-04-11 00:00:00', '2018-06-13 00:00:00', 2, 6, 50, 8, 12, 18, '2018-04-04 00:00:00', '17:45:00', '18:45:00', NULL, 'x', 'La edad no es un impedimento para aprender a tocar tus canciones favoritas, ven a nuestro taller y compruébalo.', NULL, 'Cuando se llega al segundo se le puede cobrar y hay que avisar al primero, o a Juan Carlos para que lo haga él.', 2, 8, 11, NULL, NULL, 0),
(49, 3, 'Guitarra infantil', 4, '2018-04-14 00:00:00', '2018-06-16 00:00:00', 2, 6, 50, 8, 12, 8, '2018-04-04 00:00:00', '11:00:00', '13:00:00', NULL, 's', 'La edad no es un impedimento para aprender a tocar tus canciones favoritas, ven a nuestro taller y compruébalo.', NULL, 'Cuando se llega al segundo se le puede cobrar y hay que avisar al primero, o a Juan Carlos para que lo haga él.', 2, 8, 11, NULL, NULL, 0),
(50, 3, 'Guitarra infantil', 5, '2018-04-14 00:00:00', '2018-06-16 00:00:00', 2, 6, 50, 8, 12, 5, '2018-04-04 00:00:00', '11:00:00', '12:00:00', NULL, 's', 'La edad no es un impedimento para aprender a tocar tus canciones favoritas, ven a nuestro taller y compruébalo.', NULL, 'Cuando se llega al segundo se le puede cobrar y hay que avisar al primero, o a Juan Carlos para que lo haga él.', 2, 7, 12, NULL, NULL, 0),
(51, 3, 'Guitarra infantil', 6, '2018-04-14 00:00:00', '2018-06-16 00:00:00', 2, 6, 50, 8, 12, 8, '2018-04-04 00:00:00', '13:00:00', '14:00:00', NULL, 's', 'La edad no es un impedimento para aprender a tocar tus canciones favoritas, ven a nuestro taller y compruébalo.', NULL, 'Cuando se llega al segundo se le puede cobrar y hay que avisar al primero, o a Juan Carlos para que lo haga él.', 2, 8, 11, NULL, NULL, 0),
(52, 3, 'Ukelele', 1, '2018-04-12 00:00:00', '2018-06-14 00:00:00', 2, 6, 50, 13, 18, 10, '2018-04-04 00:00:00', '17:00:00', '18:00:00', NULL, 'j', 'El ukelele es un instrumento fácil de tocar con el que podrás hacer bonitos acompañamientos o tocar canciones de forma sencilla.', NULL, 'Cuando se llega al segundo se le puede cobrar y hay que avisar al primero, o a Juan Carlos para que lo haga él.', 2, 8, 11, NULL, NULL, 0),
(53, 3, 'Fotografía iniciación', 1, '2018-05-08 00:00:00', '2018-05-29 00:00:00', 4, 10, 40, 18, 35, 10, '2018-05-01 00:00:00', '12:00:00', '14:00:00', 'cámara, flash, monitor para los ver resultados de las prácticas', 'm', 'Solucionaremos las dudas sobre la exposición (iris, tiempo e ISO) la profundidad, el balance y otros fundamentos de la fotografía de manera teórica y práctica.', NULL, 'las cámaras para fotografía se diseñan para enfrentarse tanto a la inmediatez o instante en el reportaje, con los automatismos, como para la libertad creativa si sabemos trabajar disponiendo manualmente de los controles.', 3, 3, 14, 0, NULL, 0),
(54, 3, 'Fotografía iniciación', 2, '2018-05-10 00:00:00', '2018-05-31 00:00:00', 4, 10, 40, 18, 35, 10, '2018-05-03 00:00:00', '17:00:00', '19:00:00', 'cámara, flash, monitor para los ver resultados de las prácticas', 'j', 'Solucionaremos las dudas sobre la exposición (iris, tiempo e ISO) la profundidad, el balance y otros fundamentos de la fotografía de manera teórica y práctica.', NULL, 'las cámaras para fotografía se diseñan para enfrentarse tanto a la inmediatez o instante en el reportaje, con los automatismos, como para la libertad creativa si sabemos trabajar disponiendo manualmente de los controles.', 3, 3, 14, 0, NULL, 0),
(55, 3, 'Fotografía avanzado', 1, '2018-05-08 00:00:00', '2018-05-29 00:00:00', 4, 10, 40, 18, 35, 10, '2018-05-01 00:00:00', '17:00:00', '19:00:00', 'cámara, flash, monitor para los ver resultados de las prácticas', 'm', 'Exploraremos nuevos controles en la cámara como el horquillado de exposiciones, la superposición de imagen, el nocturno con sujeto y esas otras opciones que nos da la cámara actual.', NULL, 'Las cámaras digitales modernas disponen de una gran cantidad de controles alternativos casi desconocidos para la mayoría de los ususrios.', 3, 3, 14, 0, NULL, 0),
(56, 3, 'Taller de fotografía con el móvil', 1, '2018-05-19 00:00:00', '2018-06-09 00:00:00', 6, 12, 40, 12, 17, 10, '2018-05-11 00:00:00', '12:00:00', '14:00:00', 'cámara, flash, monitor para los ver resultados de las prácticas', 's', 'El objetivo del taller es investigar y conocer mejor este medio que les es tan familiar, así como acercar la fotografía tradicional a los dispositivos\r\nfotográficos móviles, desde la estética fotográfica, (Composición, planos, etc.) hasta la técnica (aplicaciones, accesorios, etc.).', 'Dehesa de navalcarbón', 'Los jóvenes serán capaces de ir desgranando y conociendo todas las decisiones que se toman, de forma consciente o inconsciente, al hacer una fotografía y, así, tendrán más herramientas tanto a la hora de crear como de leer imágenes.', 3, 3, 20, 400, NULL, 0),
(57, 3, 'Repostería adolescentes', 4, '2018-04-20 00:00:00', '2018-04-20 00:00:00', 8, 10, 15, 13, 17, 6, '2018-03-23 00:00:00', '17:30:00', '20:00:00', 'Ingredientes grupales y menaje de cocina', 'v', NULL, NULL, NULL, 4, 4, 17, NULL, NULL, 0),
(58, 3, 'Menú casual', 4, '2018-04-30 00:00:00', '2018-04-30 00:00:00', 8, 10, 15, 12, 17, 6, '2018-03-23 00:00:00', '11:00:00', '13:30:00', 'Ingredientes grupales y menaje de cocina', 'l', 'Una mañana para aprender a elaborar un menú completo, delicioso y equilibrado. No te olvides la tartera para llevarte a casa lo que cocines.', NULL, NULL, 4, 4, 17, NULL, NULL, 0),
(59, 3, 'monográfico cocina adultos', 4, '2018-05-25 00:00:00', '2018-05-25 00:00:00', 8, 10, 15, 18, 35, 6, '2018-05-18 00:00:00', '18:00:00', '20:30:00', 'Ingredientes grupales y menaje de cocina', 'v', NULL, NULL, NULL, 4, 4, 16, NULL, NULL, 0),
(60, 3, 'monográfico cocina adultos', 4, '2018-05-12 00:00:00', '2018-05-12 00:00:00', 8, 10, 15, 18, 35, 6, '2018-05-04 00:00:00', '18:00:00', '20:30:00', 'Ingredientes grupales y menaje de cocina', 'v', NULL, NULL, NULL, 4, 4, 16, NULL, NULL, 0),
(61, 3, 'Pastores por un día', 12, '2018-04-14 00:00:00', '2018-04-14 00:00:00', 15, 25, 8, 6, NULL, 19, '2018-04-07 00:00:00', '11:00:00', '14:00:00', 'Educador ambiental, materiales educativos y seguros.', 's', 'Visita una autentica explotación caprina ecológica, Conoce las técnicas tradicionales\r\ndel pastoreo con perro y aprende a elaborar queso.', 'carretera M610 Miraflores de la Sierra-Bustarviejo', NULL, 1, 4, NULL, 360, NULL, 0),
(62, 3, 'Anillamiento científico de aves y revisión de cajas nido', 12, '2018-05-05 00:00:00', '2018-05-05 00:00:00', 15, 25, 6, 6, NULL, 19, '2018-04-28 00:00:00', '10:30:00', '13:30:00', 'Formación, materiales educativos y seguros.', 's', 'Una jornada de campo para conocer el trabajo científico de los ornitólogos. Visitaremos las redes de captura, recogeremos los pájaros que han caído  e identificaremos y anillaremos. Además, revisaremos unas cajas nidos.', 'Estación de la Anillamiento Finca El Garzo, Las Matas.', NULL, 1, 4, 18, 300, NULL, 0),
(63, 1, 'Actividad de pruebas', 1, '2018-02-01 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, '01:00:00', '02:00:00', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, 0),
(64, 3, 'Multiaventura en la Finca del Pilar', 12, '2018-04-02 00:00:00', '2018-04-02 00:00:00', 10, 20, 0, 12, 17, 19, '2018-03-26 00:00:00', '11:00:00', '13:30:00', 'Formadores y material común.', 'l', NULL, 'Parque de cuerdas Finca del Pilar', NULL, 1, 4, 19, 0, NULL, 0),
(65, 3, 'Danzas del mundo', 4, '2018-04-07 00:00:00', '2018-04-07 00:00:00', 10, 20, 0, 14, 30, 7, '2018-03-30 00:00:00', '11:30:00', '13:30:00', NULL, 's', 'Aprende distintos tipos de danzas del mundo y profundiza en tus capacidades musicales, artísticas, creativas y emocionales como recurso para monitores de tiempo libre.', NULL, NULL, 5, 1, 7, 0, NULL, 0),
(66, 3, 'LATANAIT - Danzas del mundo en familia', 4, '2018-04-06 00:00:00', '2018-04-06 00:00:00', 10, 30, 0, 6, 99, 7, '2018-04-06 00:00:00', '00:00:00', '00:00:00', NULL, 'v', 'Danzas del mundo en familia', NULL, 'No hace falta inscripción.', 6, 1, 7, 0, NULL, 0),
(67, 3, 'LATANAIT - Outlet antishopping', 4, '2018-06-01 00:00:00', '2018-06-01 00:00:00', 10, 200, 0, 6, 99, 9, '2018-06-01 00:00:00', '17:00:00', '21:00:00', NULL, 'v', 'Outlet Antishopping - mercadillo de intercambio de ropa', NULL, NULL, 6, 1, 7, 300, NULL, 0),
(68, 3, 'LATANAIT - Muestra final talleres baile', 4, '2018-06-15 00:00:00', '2018-06-15 00:00:00', 10, 150, 0, 6, 99, 9, '2018-06-15 00:00:00', '21:00:00', '22:30:00', NULL, 'v', 'Muestra final talleres baile', NULL, NULL, 6, 7, NULL, 50, NULL, 0),
(69, 3, 'Técnicas de Estudio', 1, '2018-04-03 00:00:00', '2018-04-24 00:00:00', 8, 15, 25, 12, 17, 11, '2018-03-23 00:00:00', '17:30:00', '19:30:00', NULL, 'm', 'Una buena planificación y conocer algunas técnicas que nos ayuden a mejorar el rendimiento y mantener la motivación para afrontar la recta final del curso.', NULL, NULL, 5, 13, NULL, NULL, NULL, 0),
(70, 3, 'Taller de fotografía para padres e hijos', 1, '2018-05-12 00:00:00', '2018-06-02 00:00:00', 6, 12, 40, 6, 12, 10, '2018-05-11 00:00:00', '10:00:00', '12:00:00', 'cámara, flash, monitor para los ver resultados de las prácticas', 's', 'Dirigido a niños y niñas entre 6 y 12 años acompañados de su madre y/o padre. Es imprescindible que los participantes traigan cámaras digitales.', 'Dehesa de navalcarbón', 'Reflexionaremos sobre la imagen y cómo se pueden comunicar diferentes ideas a través de ella. Esto con una dinámica activa y participativa a través de sesiones muy prácticas con cámara en mano. Los participantes interiorizarán nociones como planos, ángulos, perspectiva, secuencias y composición, y desarrollar criterios de encuadre, selección de motivos e iluminación.', 1, 3, 20, 400, NULL, 0),
(71, 3, 'Taller de fotografía para jovenes', 1, '2018-04-13 00:00:00', '2018-05-18 00:00:00', 6, 12, 40, 12, 17, 10, '2018-05-11 00:00:00', '18:45:00', '20:45:00', 'cámara, flash, monitor para los ver resultados de las prácticas', 's', 'Este taller se estructura a través de 4 - 5 sesiones cámara en mano, totalmente prácticas. Tiene como objetivo desarrollar la capacidad de comprender y crear imágenes apoyándonos en la fotografía\r\ncomo un medio de expresión artística, de comunicación y de información.', NULL, 'La fotografía es un vínculo de comunicación a través de las redes sociales. El objetivo del taller es investigar y conocer mejor este medio que les es tan familiar. Detrás de cada imagen hay, muy a menudo, una construcción más elaborada de la que ésta aparenta a primera vista. Los jóvenes serán capaces de ir desgranando y conociendo todas las decisiones que se toman, de forma consciente o inconsciente, al hacer una fotografía y, así, tendrán más herramientas tanto a la hora de crear como de leer imágenes.', 3, 3, 20, 400, NULL, 0),
(72, 3, 'Taller de fotografía para jovenes', 2, '2018-04-13 00:00:00', '2018-05-18 00:00:00', 6, 12, 40, 16, 35, 10, '2018-05-11 00:00:00', '18:45:00', '20:45:00', 'cámara, flash, monitor para los ver resultados de las prácticas', 's', 'Este taller se estructura a través de 4 - 5 sesiones cámara en mano, totalmente prácticas. Tiene como objetivo desarrollar la capacidad de comprender y crear imágenes apoyándonos en la fotografía\r\ncomo un medio de expresión artística, de comunicación y de información.', NULL, 'La fotografía es un vínculo de comunicación a través de las redes sociales. El objetivo del taller es investigar y conocer mejor este medio que les es tan familiar. Detrás de cada imagen hay, muy a menudo, una construcción más elaborada de la que ésta aparenta a primera vista. Los jóvenes serán capaces de ir desgranando y conociendo todas las decisiones que se toman, de forma consciente o inconsciente, al hacer una fotografía y, así, tendrán más herramientas tanto a la hora de crear como de leer imágenes.', 3, 3, 20, 400, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aulas`
--

CREATE TABLE `aulas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `aulas`
--

INSERT INTO `aulas` (`id`, `nombre`) VALUES
(1, 'Centro-A1'),
(2, 'Centro-B1'),
(3, 'Centro-B2'),
(4, 'Centro-B3'),
(5, 'Centro-B4'),
(6, 'Centro-Cocina'),
(7, 'Centro-Salón de actos'),
(8, 'Centro-Control 2'),
(9, 'Casa-A1'),
(10, 'Casa-A2'),
(11, 'Casa-A3'),
(12, 'Casa-A4'),
(13, 'Casa-A5'),
(14, 'Casa-A6'),
(15, 'Casa-A7'),
(16, 'Casa-Sala de exposiciones'),
(17, 'Matas-Salón de actos'),
(18, 'Matas-Aula1'),
(19, 'Exterior');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `campanias`
--

CREATE TABLE `campanias` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `campanias`
--

INSERT INTO `campanias` (`id`, `nombre`) VALUES
(1, 'Prueba 2018'),
(3, 'Primavera 2018');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `nombre`) VALUES
(1, 'Aire libre y familia'),
(2, 'Música y artes escénicas'),
(3, 'Imagen y nuevas tecnologías'),
(4, 'Habilidades domésticas'),
(5, 'Formación y empleo'),
(6, 'Más ocio'),
(7, 'Certámenes'),
(8, 'Centros escolares');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactos`
--

CREATE TABLE `contactos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre_completo` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` int(11) DEFAULT NULL,
  `movil` int(11) DEFAULT NULL,
  `correo` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `puesto` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `empresa_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `contactos`
--

INSERT INTO `contactos` (`id`, `nombre_completo`, `telefono`, `movil`, `correo`, `puesto`, `empresa_id`) VALUES
(1, 'Jóse María Fayos Mestre', 915348121, 606583671, 'AJEDREZBYN@GMIAL.COM', 'Formador', 1),
(2, 'Javier Martinez', 916300724, 670294678, 'javierm@utopia21.es', 'Coordinación', 11),
(3, 'Inma Palenzuela', 916300724, 672341681, 'rrhh@utopia21.es', 'Recursos Humanos', 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

CREATE TABLE `empresas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre_empresa` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cif` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `razon_social` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provincia` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ciudad` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pais` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cp` int(11) DEFAULT NULL,
  `web` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actividad` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` int(11) DEFAULT NULL,
  `fax` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `empresas`
--

INSERT INTO `empresas` (`id`, `nombre_empresa`, `cif`, `razon_social`, `direccion`, `provincia`, `ciudad`, `pais`, `cp`, `web`, `actividad`, `telefono`, `fax`) VALUES
(1, 'Club Ajedrez Blanco y Negro', 'G 80847536', 'Club Ajedrez Blanco y Negro', 'Conde la Cimera 2, Local 5', 'MADRID', 'MADRID', 'España', 28040, 'www.ajedrezblancoynegro.com', 'Formación en ajedrez', 915348121, NULL),
(2, 'Lucely Orozco Soto', '51139373 T', 'Lucely Orozco Soto', 'C/ Antonio López 154, 3ºB', 'Madrid', 'Madrid', 'España', 28026, 'lucely-costurita.blogspot.com.es', 'Corte y confección', 687262553, NULL),
(5, 'Escuela Canina La Tejera', '02890250 R', 'Escuela Canina La Tejera', 'C/ PICASSO 7', 'MADRID', 'GALAPAGAR', 'España', 28420, 'www.escuelacaninalatejera.es', 'Clases de educación y adiestramiento canino para familias.', 918284315, NULL),
(6, 'Asociación Baithaka', 'G-86047396', 'Asociación Baithaka', 'Avda. España 157', 'Madrid', 'Las Rozas', 'España', 28231, 'https://baithaka.wordpress.com/', 'Cosmética natural.', 629894462, NULL),
(7, 'Ventana Creativa', '51112083B', 'Ignacio Bernad de Lama', 'C/ Montalban, 10', 'Madrid', 'Cercedilla', 'España', 28470, 'https://www.laventanacreativa.org/', 'Rap', 653234154, NULL),
(8, 'Concejalía de Juventud', 'P2812700I', 'Ayuntamiento Las Rozas', 'Plaza Mayor 1', 'Madrid', 'Las Rozas', 'España', 28231, 'www.rozasjoven.es', NULL, 917579600, NULL),
(9, 'Kitchen Academy', 'B-87046223', 'Asibordan SL', 'C/ Castillo de Belmonte, 3', 'Madrid', 'Las Rozas', 'España', 28232, 'https://kitchenacademy.es/', 'Cursos de cocina', 914938856, NULL),
(10, 'L2Q2', 'G84218106', 'asociación fotográfica juvenil LOS LUNES QUE NOS QUEDAN', 'PLAZA  LA CONSTITUCIÓN, 4 - VILLAVICIOSA-', 'MADRID', 'Madrid', 'España', 28670, 'INFO@L2Q2.ES - WWW.L2Q2.ES', 'cursos de fotografía', 627430413, NULL),
(11, 'Utopía XXI', 'B-86022226', 'Utopía XXI', 'C/ Pilar 12', 'Madrid', 'Las Rozas de Madrid', 'España', 28290, 'http://www.utopia21.es/', 'Clases de instrumentos musicales', 916300724, NULL),
(12, 'María Santos Blanco', 'G84218106', 'Barbarella Blow', 'c/ comunidad de madrid, 23, 4ºB', 'MADRID', 'Madrid', 'España', 28231, 'barbarella@barbarellablow.com', 'cursos de photoshop', 689406759, NULL),
(13, 'Enara', 'B86977022', 'ENARA EDUCACION AMBIENTAL SL', 'C/ Estación, 8', 'Madrid', 'Las Rozas', 'España', 28290, 'www.enara.org', 'Educación ambiental / anillamiento científico de aves', 68795573, NULL),
(14, 'Concejalía de Deportes', 'P2812700i', 'Equipo de aire libre', 'Avda. Nuestra Sra. del Retamar s/n', 'Madrid', 'Las Rozas', 'España', 28232, 'http://www.rozasdeportes.org', 'Formación deportiva', 917579811, NULL),
(15, 'Brigida Higueras Madsen', '14310014N', 'Brigida Higueras Madsen', 'C/ Yucatan 5,', 'Las Rozas', 'Madrid', 'España', 28231, NULL, 'Técnicas de estudio', 619211214, NULL),
(16, 'Buen Royo foto', '02887060P', 'María Rocío Bueno Royo', 'c/cabo Tarifa,8', 'MADRID', 'Las Matas', 'España', 28290, 'barbarella@barbarellablow.com', 'cursos de fotografía', 635041419, NULL),
(17, 'Buen Royo foto', '02887060P', 'María Rocío Bueno Royo', NULL, 'MADRID', 'Las Matas', 'España', 28290, 'www.buenroyophoto.com', 'cursos de fotografía', 635041419, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos`
--

CREATE TABLE `grupos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `grupos`
--

INSERT INTO `grupos` (`id`, `nombre`) VALUES
(1, 'Casa1'),
(2, 'Casa2'),
(3, 'Casa3'),
(4, 'Centro1'),
(5, 'Centro2'),
(6, 'Centro3'),
(7, 'Centro4'),
(8, 'Centro5'),
(9, 'Matas1'),
(10, 'Matas2'),
(11, 'Matas3'),
(12, 'Exterior1'),
(13, 'Exterior2'),
(14, 'Exterior3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_12_26_124729_create_campanias_table', 1),
(4, '2017_12_26_182127_create_aulas_table', 1),
(5, '2017_12_27_135232_create_empresas_table', 1),
(6, '2017_12_27_135323_create_contactos_table', 1),
(7, '2017_12_27_144856_create_profesors_table', 1),
(8, '2017_12_27_150109_create_responsables_table', 1),
(9, '2017_12_27_151307_create_categorias_table', 1),
(10, '2017_12_27_155907_create_grupos_table', 1),
(11, '2017_12_29_083607_create_actividades_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesors`
--

CREATE TABLE `profesors` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre_completo` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` int(11) DEFAULT NULL,
  `correo` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `empresa_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `profesors`
--

INSERT INTO `profesors` (`id`, `nombre_completo`, `telefono`, `correo`, `empresa_id`) VALUES
(1, 'José María', 64574574, 'rjy@egter.es', 1),
(3, 'Alfredo Ruíz García', 670388300, 'info@escuelacaninalatejera.es', 5),
(4, 'Ana Lobo', 629894462, 'analoboviajera@hotmail.com', 6),
(5, 'Lucely Orozco Soto', 687262553, 'costurita.cr@gmail.com', 2),
(6, 'Nacho Bernad de Lama', 653234154, 'info@laventanacreativa.com', 7),
(7, 'Ana Saburido', 917579600, 'asaburido@lasrozas.es', 8),
(9, 'Lourdes', NULL, 'lquintana@lasrozas.es', 8),
(10, 'Juanjo Alonso', 627430413, 'INFO@L2Q2.ES - WWW.L2Q2.ES', 7),
(11, 'Tino Veiga', 615156573, 'lebowskitino@gmail.com', 11),
(12, 'Mark Moreno', 666383449, 'marcmoreno@movistar.es', 11),
(13, 'María Santos', 689406759, 'barbarellablow@gmail.com', 6),
(14, 'Carlos de Vega', 696760425, 'cvega@lasrozas.es', 8),
(15, 'Carlota', 678242801, 'dreverte@kitchenacdemy.es', 9),
(16, 'Corina', 637546509, 'dreverte@kitchenacademy.es', 9),
(17, 'Rosa María Nieves Cuesta', 657268074, 'rnieves@kitchenacademy.es', 9),
(18, 'Eva banda', 68795573, 'ebanda@enara.org', 13),
(19, 'Equipo de aire libre de la Concejalía de Deportes', 917579811, NULL, 14),
(20, 'Rocío Bueno', 635041419, 'rbueno@euroecoasesores.com', 16);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `responsables`
--

CREATE TABLE `responsables` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `responsables`
--

INSERT INTO `responsables` (`id`, `nombre`) VALUES
(1, 'Ana'),
(2, 'Ángela'),
(3, 'Carlos'),
(4, 'Charlie'),
(5, 'Cristina'),
(6, 'Fito'),
(7, 'Inés'),
(8, 'J. Carlos'),
(9, 'Jesús'),
(10, 'Lourdes'),
(11, 'Manuel'),
(12, 'María'),
(13, 'Trini');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actividades`
--
ALTER TABLE `actividades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `actividades_campania_id_foreign` (`campania_id`),
  ADD KEY `actividades_grupo_id_foreign` (`grupo_id`),
  ADD KEY `actividades_aula_id_foreign` (`aula_id`),
  ADD KEY `actividades_categoria_id_foreign` (`categoria_id`),
  ADD KEY `actividades_responsable_id_foreign` (`responsable_id`),
  ADD KEY `actividades_profesor_id_foreign` (`profesor_id`);

--
-- Indices de la tabla `aulas`
--
ALTER TABLE `aulas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `campanias`
--
ALTER TABLE `campanias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contactos`
--
ALTER TABLE `contactos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contactos_empresa_id_foreign` (`empresa_id`);

--
-- Indices de la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `profesors`
--
ALTER TABLE `profesors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profesors_empresa_id_foreign` (`empresa_id`);

--
-- Indices de la tabla `responsables`
--
ALTER TABLE `responsables`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actividades`
--
ALTER TABLE `actividades`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT de la tabla `aulas`
--
ALTER TABLE `aulas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `campanias`
--
ALTER TABLE `campanias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `contactos`
--
ALTER TABLE `contactos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `empresas`
--
ALTER TABLE `empresas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `grupos`
--
ALTER TABLE `grupos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `profesors`
--
ALTER TABLE `profesors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `responsables`
--
ALTER TABLE `responsables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `actividades`
--
ALTER TABLE `actividades`
  ADD CONSTRAINT `actividades_aula_id_foreign` FOREIGN KEY (`aula_id`) REFERENCES `aulas` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `actividades_campania_id_foreign` FOREIGN KEY (`campania_id`) REFERENCES `campanias` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `actividades_categoria_id_foreign` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `actividades_grupo_id_foreign` FOREIGN KEY (`grupo_id`) REFERENCES `grupos` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `actividades_profesor_id_foreign` FOREIGN KEY (`profesor_id`) REFERENCES `profesors` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `actividades_responsable_id_foreign` FOREIGN KEY (`responsable_id`) REFERENCES `responsables` (`id`) ON DELETE SET NULL;

--
-- Filtros para la tabla `contactos`
--
ALTER TABLE `contactos`
  ADD CONSTRAINT `contactos_empresa_id_foreign` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `profesors`
--
ALTER TABLE `profesors`
  ADD CONSTRAINT `profesors_empresa_id_foreign` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

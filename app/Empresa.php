<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    /**
     * No mostrar la creacion y actualización.
     *
     * @var boolean
     */
    public $timestamps = false;
       
    /**
     * Los atributos que deberían estar ocultos para los arrays.
     *
     * @var array
     */
    protected $guarded = ['id'];
    
    /**
     * Devuelve los contactos asociados a la empresa.
     *
     * @return \App\Contacto Contactos de la empresa.
     */
    public function Contactos(){
        return $this->hasMany('App\Contacto');
    }
    public function Profesors(){
        return $this->hasMany('App\Profesor');
    }
   

}

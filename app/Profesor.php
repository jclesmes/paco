<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesor extends Model
{
    /**
     * No mostrar la creacion y actualización.
     *
     * @var boolean
     */    
    public $timestamps = false;
    
    /**
     * Los atributos que pueden ser rellenos.
     *
     * @var array
     */
    protected $fillable = ['nombre_completo', 'telefono', 'correo', 'empresa_id'];
    
    /**
     * Los atributos que deberían estar ocultos para los arrays.
     *
     * @var array
     */
    protected $guarded = ['id'];
    
    /**
     * Devuelve la empresa asociada al profesor.
     *
     * @return \App\Empresa Empresa del profesor.
     */    
    public function empresa() {
        return $this->belongsTo('App\Empresa');
      }
}

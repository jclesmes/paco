<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    /**
     * Los atributos rellenables.
     *
     * @var array
     */
    protected $fillable = ['id','fecha_inicio','fecha_fin','nombre', 'eventColor'];
}

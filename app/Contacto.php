<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacto extends Model
{        
    /**
     * No mostrar la creacion y actualización.
     *
     * @var boolean
     */    
    public $timestamps = false;
       
    /**
     * Los atributos que deberían estar ocultos para los arrays.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Devuelve la empresa asociada a la actividad.
     *
     * @return \App\Empresa Empresa de la actividad.
     */
    public function empresa()
    {
        return $this->belongsTo('App\Empresa');
    }
}

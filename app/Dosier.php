<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dosier extends Model
{
    /**
    * Los atributos que deberían estar ocultos para los arrays.
    *
    * @var array
    */
   protected $guarded = ['id'];

    /**
     * Devuelve la actividad asociada al dosier.
     *
     * @return \App\Actividad Actividad del dosier.
     */
    public function actividad() {
        return $this->belongsTo('App\Actividad');
      }

}

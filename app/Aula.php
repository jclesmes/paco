<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aula extends Model
{
    /**
     * No mostrar la creacion y actualización.
     *
     * @var boolean
     */    
    public $timestamps = false;
       
    /**
     * Los atributos que pueden ser rellenos.
     *
     * @var array
     */
    protected $fillable = ['nombre'];
}

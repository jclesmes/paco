<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/*
 * Modelo actividad
 */
class Actividad extends Model
{
    /**
     * No mostrar la creacion y actualización.
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Los atributos que deberían estar ocultos para los arrays.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Nombre de la tabla.
     *
     * @var
     */
    protected $table = 'actividades';

    /**
     * Devuelve la campaña asociada a la actividad.
     *
     * @return \App\Campania Campaña de la actividad.
     */
    public function campania() {
      return $this->belongsTo('App\Campania');
    }

    /**
     * Devuelve el grupo asociada a la actividad.
     *
     * @return \App\Grupo Grupo de la actividad.
     */
    public function grupo() {
      return $this->belongsTo('App\Grupo');
    }  

    /**
     * Devuelve el responsable asociado a la actividad.
     *
     * @return \App\Responsable Responsable de la actividad.
     */
    public function responsable() {
         return $this->belongsTo('App\Responsable');
    }
    
    /**
     * Devuelve el aula asociada a la actividad.
     *
     * @return \App\Aula Aula de la actividad.
     */
    public function aula() {
      return $this->belongsTo('App\Aula');
    }

    /**
     * Devuelve el profesor asociado a la actividad.
     *
     * @return \App\Profesor Grupo de la actividad.
     */
    public function profesor() {
      return $this->belongsTo('App\Profesor');
    }

    /**
     * Devuelve la categoria asociada a la actividad.
     *
     * @return \App\Categoria Grupo de la actividad.
     */
    public function categoria() {
      return $this->belongsTo('App\Categoria');
    }

    /**
     * Devuelve los metálicos asociados a la actividad.
     *
     * @return \App\Metalico Grupo de la actividad.
     */
    public function metalicos(){
      return $this->hasMany('App\Metalico');
    }
    /**
     * Devuelve el dosier asociado a la actividad.
     *
     * @return \App\Dosier 
     */
    public function dosier(){
      return $this->hasOne('App\Dosier');
  }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metalico extends Model
{
    /**
     * Los atributos que deberían estar ocultos para los arrays.
     *
     * @var array
     */
    protected $guarded = ['id'];
    
    /**
     * Devuelve la actividad asociada al metálico.
     *
     * @return \App\Actividad Actividad del metálico.
     */
    public function actividad() {
        return $this->belongsTo('App\Actividad');
      }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MYPDF extends \TCPDF {
    
        /**  
         *  Definimos una cabecera nueva genérica para TCPDF
         */
        public function Header() {
            // Logo usado
            $image_file = public_path().'/plantilla/img/logo.jpg';
            //Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array())
           $this->Image($image_file, 10, 10, 25, '', 'JPG', '', 'T', false, 75, '', false, false, 0, false, false, false);
            // Tipo de fuente
            $this->SetFont('helvetica', 'B', 16);
            // Título
            $this->Cell(0, 125, 'FICHA DE ACTIVIDAD', 0, false, 'C', 0, '', 0, false, 'M', 'M');
        }
    
        /**  
         *  Definimos un nuevo footer para TCPDF
         */
        public function Footer() {
            // Position at 15 mm from bottom
            $this->SetY(-15);
            // Tipo de fuente
            $this->SetFont('helvetica', 'I', 8);
            // Numeración de página
            $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        }
    }
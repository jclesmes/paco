<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ActividadFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|max:100',
            'campania_id' => 'required',
            'fecha_inicio' => 'required',
            'fecha_fin' => 'required',
            'fecha_inscripciones' => 'required',
            'texto_previo_folleto' => 'required|max:220|min:20',
            'dias' => 'required',
            'horas' => 'required',
            'precio' => 'required'
        ];
    }
    
    public function messages()
    {
        return [
            'nombre.required' => 'Debes de añadir un nombre.',
            'nombre.max' => 'Los caracteres máximos del nombre son 100.',
            'campania_id.required' => 'Debes seleccionar una campaña antes.',
            'fecha_inicio.required' => 'Debes de seleccionar la fecha de inicio.',
            'fecha_fin.required' => 'Debes de seleccionar la fecha de fin.',
            'fecha_inscripciones.required' => 'Debes de seleccionar la fecha de fin de inscripciones.',
            'texto_previo_folleto.required' => 'Debes de rellenar el campo Texto previo del folleto.',
            'texto_previo_folleto.max' => 'El máximo de caracteras de Texto previo del folleto es 220 corrígelo.',
            'texto_previo_folleto.min' => 'No has llegado al mínimo en Texto previo del folleto corrígelo.',
            'dias.required' => 'Debes de rellenar el campo días',
            'horas.required' => 'Debes de poner en número total de horas de la actividad',
            'precio.required' => 'Debes de poner el precio'
        ];
    }

}

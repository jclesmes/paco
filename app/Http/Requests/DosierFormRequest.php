<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DosierFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'descripcion_dosier'    => 'required',
            'contenido_dosier'      => 'required',
            'url_imagen'            => 'mimes:jpg,jpeg,png',
        ];
    }

    public function messages()
    {
        return [
            'descripcion_dosier.required'   => 'Debes rellenar la descripción',
            'contenido_dosier.required'     => 'Debes de rellenar el contenido',
            'url_imagen.required'           => 'Debes de añadir una imagen',
            'url_imagen.mimes'              => 'Imagen inválida, solo aceptamos jpg, jpeg y png',
        ];
    }
}

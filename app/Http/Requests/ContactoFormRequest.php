<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactoFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre_completo' => 'required',
            'empresa_id' => 'required',
            'telefono' => 'max:9',
            'movil' => 'max:9',
        ];
    }
    public function messages()
    {
        return [
            'empresa_id.required' => 'Debes seleccionar una empresa para el contacto.',
            'telefono.min' => 'Revisa el teléfono, que no es correcto debe de tener 9 dígitos',
            'movil.min' => 'Revisa el móvil, que no es correcto debe de tener 9 dígitos',
            'telefono.max' => 'Revisa el teléfono, que no es correcto debe de tener 9 dígitos',
            'movil.max' => 'Revisa el móvil, que no es correcto debe de tener 9 dígitos',
        ];
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Request\CampaniaFormResquest;
use App\Campania;
use App\Actividad;
class CampaniasController extends Controller
{
    /**
     * Muestra todas las campañas.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $campanias = Campania::orderBy('id', 'desc')->get();
        return view('campanias.index', compact('campanias'));
    }

    /**
     * Muestra el formulario para crear una nueva campaña.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('campanias.crear');
    }

    /**
     * Guarda una nueva campaña.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campania = new Campania;
        $campania->nombre = $request->nombre;
        $campania->save();
        return redirect('/campanias')->with('mensaje', 'Campaña guardada');  
    }

    /**
     * Muestra una campaña específica.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $campania = Campania::whereId($id)->firstOrFail();
        return view('campanias.show', compact('campania'));
    }

    /**
     * Muestra el formulario para editar una campaña.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $campania = Campania::whereId($id)->firstOrFail();
        return view('campanias.edit', compact('campania'));
    }

    /**
     * Actualiza y guarda una campaña.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $campania = Campania::whereId($id)->firstOrFail();
        $campania->nombre = $request->get('nombre');
        $campania->save();
        $id_campania_actual = $request->session()->get('campaniaActiva')[1];
        $campania_actual = Campania::whereId($id_campania_actual)->firstOrFail();
        session()->flush();
        session()->push('campaniaActiva', $campania_actual->nombre);
        session()->push('campaniaActiva', $campania_actual->id);
        return redirect('/campanias')->with('mensaje', 'Campaña modificada');
    }

    /**
     * Elimina una campaña específica.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        session()->flush();
        $campania = Campania::whereId($id)->firstOrFail();
        $campania->delete();
        $campania = Campania::All()->last();
        if($campania!=null){
            session()->push('campaniaActiva', $campania->nombre);
            session()->push('campaniaActiva', $campania->id);
        }
        else {
            session(['sin_campania' => 'Debes de crear una campaña para continuar']);
        }
        return redirect('/campanias')->with('mensaje', 'Campaña eliminada');
    }

    /**
     * Muestra el aviso para eliminar una campaña.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function borrar($id)
    {
        $actividades = Actividad::where('campania_id', $id)->get();
        $campania = Campania::whereId($id)->firstOrFail();
        return view('campanias.borrar', compact('campania', 'actividades'));
    }

    /**
     * Cambio de campaña.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cambiar($id)
    {
        session()->flush();
        $campania = Campania::whereId($id)->firstOrFail();
        session()->push('campaniaActiva', $campania->nombre);
        session()->push('campaniaActiva', $campania->id);
        return redirect('/')->with('mensaje', 'Campaña cambiada');
    }
    
}

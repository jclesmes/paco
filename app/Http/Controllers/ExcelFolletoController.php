<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use App\Categoria;
use App\Actividad;

use Carbon\Carbon;
class ExcelFolletoController extends Controller
{
     /**
     * Muestra la página de selección de descargas por categoría Indesing.
     *
     * @return \Illuminate\Http\Response
     */
    public function menu()
    {  
        $categorias = Categoria::all();
        return view('descargas.folleto', compact('categorias') );
    }

    /**
     * Nos crea un excel con los datos de las actividades de la campaña en sesión,
     * este arhivo sirve para importar las actividades de la campaña en InDesing, preparado
     * aparte y reducir el trabajo de hacer a mano este paso.
     * 
     *  @return .csv
     */   
    public function folleto(int $categoria_id)
    {
        if($categoria_id == 0){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            date_default_timezone_set('Europe/Madrid');
            $phpExcel = new  Spreadsheet();
            // Encabezados
            $titulosColumnas = array('ACTIVIDAD','EDAD','TEXTO PREVIO FOLLETO', 'INICIO-FIN', 
                                    'PRECIO','HORARIO', 'HORAS', 'LUGAR DE CELEBRACIÓN','INCLUYE',                     
                                    );
            // Propiedades del documento
            $phpExcel->getProperties()->setCreator("Francisco Piedras Pérez")
                                        ->setLastModifiedBy("Francisco Piedras Pérez")
                                        ->setTitle(session('campaniaActiva')[0]." - Actividades Trimestrales Folleto")
                                        ->setSubject("Rozas Joven - Concejalía de Juventud")
                                        ->setDescription("Resumen de actividadedes para la concejalía de juventud en Las Rozas Folleto.")
                                        ->setKeywords("csv actividades folleto")
                                        ->setCategory("Actividades");
            $i = 1; //Comienzo de las filas
            $phpExcel   ->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i,  $titulosColumnas[0])
                        ->setCellValue('B'.$i,  $titulosColumnas[1])
                        ->setCellValue('C'.$i,  $titulosColumnas[2])
                        ->setCellValue('D'.$i,  $titulosColumnas[3])
                        ->setCellValue('E'.$i,  $titulosColumnas[4])
                        ->setCellValue('F'.$i,  $titulosColumnas[5])
                        ->setCellValue('G'.$i,  $titulosColumnas[6])
                        ->setCellValue('H'.$i,  $titulosColumnas[7])
                        ->setCellValue('I'.$i,  $titulosColumnas[8]);
            $i++;
    
            $categorias = Categoria::All();
            
            // Ponemos Carbon en español, para fechas
            Carbon::setLocale('es');
            setlocale(LC_TIME, 'es_ES.UTF-8', 'spanish');
            
            foreach ($categorias as $categoria)
            {
                $campania_id = session()->get('campaniaActiva')[1];
                $actividades = Actividad::where([
                    ['categoria_id', $categoria->id], 
                    ['campania_id', $campania_id]
                    ])->get();
                foreach ($actividades as $actividad) {
                    // Aplicamos reglas
                    $nombre = $actividad->nombre;
    
                    if( $actividad->edad_maxima < 1 )
                        $edad = 'A partir de ' . $actividad->edad_minima . ' años';
                    else
                        $edad = 'De ' .  $actividad->edad_minima . ' a ' .  $actividad->edad_maxima . ' años';
    
                    $texto_previo_folleto = $actividad->texto_previo_folleto;
                    $incluye = $actividad->incluye;
                    $precio = $actividad->precio . ' €';
    
                    $lugar = '';
                    if (strpos($actividad->aula->nombre, 'Exterior') !== false) {
                        $lugar = 'En ' . $actividad->lugar_celebracion;
                    }
                    else{
                        if(strpos($actividad->aula->nombre, 'Casa') !== false){
                            $lugar = \App\Funciones\Centros::CASA;
                        }
                        if(strpos($actividad->aula->nombre, 'Centro') !== false){
                            $lugar = \App\Funciones\Centros::JUVENTUD;
                        }
                        if(strpos($actividad->aula->nombre, 'Matas') !== false){
                            $lugar = \App\Funciones\Centros::MATAS;
                        }
                    }
                    // Creamos fechas según son el mismo día o en un periodo
                    $inicio_fin = 'inicio - fin';
                
                    if($actividad->fecha_inicio == $actividad->fecha_fin){
                        setlocale(LC_TIME, 'es_ES.UTF-8', 'spanish');
                        $inicio_fin =  strftime( '%A %e de %B', strtotime($actividad->fecha_inicio) ).'.';
                    }
                    else{
                        setlocale(LC_TIME, 'es_ES.UTF-8', 'spanish');
                        $inicio_fin =  strftime( 'Del %A %e de %B', strtotime($actividad->fecha_inicio) ).' al '.strftime('%A %e de %B', strtotime($actividad->fecha_fin) ).'.';
                    }
                    // Creamos la fecha de horario
                    $horario =  ucfirst(Carbon::parse($actividad->fecha_inicio)->formatLocalized('%A') ) . ' de ' . Carbon::parse( $actividad->hora_inicio)->format('G:i') . ' a ' . Carbon::parse( $actividad->hora_fin)->format('G:i') . ' h.';
  
                    $horas = $actividad->horas . ' h';
                    
                    $phpExcel   ->setActiveSheetIndex(0)
                                ->setCellValue('A'.$i, $nombre)
                                ->setCellValue('B'.$i, $edad)
                                ->setCellValue('C'.$i, $texto_previo_folleto)
                                ->setCellValue('D'.$i, $inicio_fin )
                                ->setCellValue('E'.$i, $precio )
                                ->setCellValue('F'.$i, $horario)
                                ->setCellValue('G'.$i, $horas)
                                ->setCellValue('H'.$i, $lugar)
                                ->setCellValue('I'.$i, $incluye);
                    $i++;
                }
               
            }
            // Renombramos la hoja
            $phpExcel->getActiveSheet()->setTitle('Folleto Completo');
    
            // Activamos la primera hoja como activa, así Excel abre esta como primera
            $phpExcel->setActiveSheetIndex(0);
    
             // // Cabeceras para descargar el archivo csv
            // header('Content-Type: text/csv');
            // header('Content-Disposition: attachment;filename="'.session()->get('campaniaActiva')[0] .' - folleto de actividades - '.$categoria->nombre.'.csv"');
            // header('Cache-Control: max-age=0');
            // // If you're serving to IE 9, then the following may be needed
            // header('Cache-Control: max-age=1');

            // // If you're serving to IE over SSL, then the following may be needed
            // header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            // header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            // header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            // header ('Pragma: public'); // HTTP/1.0  */
    
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Csv($phpExcel);
            // Delimitador la coma ,
            $writer->setDelimiter(",");
            // Fin del dato entre comillas "
            $writer->setEnclosure('"');
            // Fin de la linea el retorno de carro y salto de línea
            $writer->setLineEnding("\r\n");
            $writer->setSheetIndex(0);

            $writer->setUseBOM(true);
            
            $writer->setPreCalculateFormulas(false);
            $writer->save("csv/demo.csv");

            $file_data = file_get_contents('csv/demo.csv');
            $utf8_file_data = utf8_encode($file_data);
            $csv = iconv("UTF-8", "WINDOWS-1252//IGNORE", $file_data);
            $new_file_name = 'csv/demo2.csv';
            file_put_contents($new_file_name , $csv );
            $file_data = file_get_contents('csv/demo2.csv');
            // collect the data to the be returned to the user, no need to save to disk
            // unless you really want to, if so, use file_put_contents()
            $dataForFile="test data to be returned to user in a file";
            
            header('Content-type: application/x-download');
            header('Content-Disposition: attachment; filename="'.session()->get('campaniaActiva')[0] .' - folleto de actividades - '.$categoria->nombre.'.csv"');
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: '.strlen($file_data));
            set_time_limit(0);
            echo $file_data;
    
            exit;
        }
        

        else{
            // Recuperamos la categoría pasada
            $categoria = Categoria::findOrFail($categoria_id);

            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            date_default_timezone_set('Europe/Madrid');
            
            // Encabezados
            $titulosColumnasAire = array('ACTIVIDAD','EDAD','TEXTO PREVIO FOLLETO', 'INICIO-FIN', 
                                        'PRECIO','LUGAR DE CELEBRACIÓN','INCLUYE',                     
                                    );
            $titulosColumnas = array('ACTIVIDAD','EDAD','TEXTO PREVIO FOLLETO', 'INICIO-FIN', 
                                    'PRECIO','HORARIO', 'HORAS', 'LUGAR DE CELEBRACIÓN'                     
                                    );
    
            $phpExcel = new  Spreadsheet();
            // Propiedades del documento
            $phpExcel->getProperties()->setCreator("Francisco Piedras Pérez")
                                        ->setLastModifiedBy("Francisco Piedras Pérez")
                                        ->setTitle(session('campaniaActiva')[0]." - Actividades Trimestrales Folleto - '.$categoria->nombre.'")
                                        ->setSubject("Rozas Joven - Concejalía de Juventud")
                                        ->setDescription("Resumen de actividadedes para la concejalía de juventud en Las Rozas Folleto.")
                                        ->setKeywords("csv actividades folleto")
                                        ->setCategory("Actividades");
            // Tenemos 2 tipos, las de aire libre y el resto
            $i = 1; //Comienzo de las filas
            if($categoria_id == 1){
                $phpExcel   ->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i,  $titulosColumnasAire[0])
                            ->setCellValue('B'.$i,  $titulosColumnasAire[1])
                            ->setCellValue('C'.$i,  $titulosColumnasAire[2])
                            ->setCellValue('D'.$i,  $titulosColumnasAire[3])
                            ->setCellValue('E'.$i,  $titulosColumnasAire[4])
                            ->setCellValue('F'.$i,  $titulosColumnasAire[5])
                            ->setCellValue('G'.$i,  $titulosColumnasAire[6]);
            }
            else{
                $phpExcel   ->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i,  $titulosColumnas[0])
                            ->setCellValue('B'.$i,  $titulosColumnas[1])
                            ->setCellValue('C'.$i,  $titulosColumnas[2])
                            ->setCellValue('D'.$i,  $titulosColumnas[3])
                            ->setCellValue('E'.$i,  $titulosColumnas[4])
                            ->setCellValue('F'.$i,  $titulosColumnas[5])
                            ->setCellValue('G'.$i,  $titulosColumnas[6])
                            ->setCellValue('H'.$i,  $titulosColumnas[7]);
            }
            
            $i++;
                    
            // Ponemos Carbon en español, para fechas
            Carbon::setLocale('es');
            setlocale(LC_TIME, 'es_ES.UTF-8', 'spanish');
            
            $campania_id = session()->get('campaniaActiva')[1];
            $actividades = Actividad::where([
                                                ['categoria_id', $categoria_id], 
                                                ['campania_id', $campania_id]
                                            ])->get();
            foreach ($actividades as $actividad) {
                // Aplicamos reglas
                $nombre = $actividad->nombre;
                // Texto para la edad
                if( $actividad->edad_maxima < 1 )
                    $edad = 'A partir de ' . $actividad->edad_minima . ' años';
                else
                    $edad = 'De ' .  $actividad->edad_minima . ' a ' .  $actividad->edad_maxima . ' años';

                $texto_previo_folleto = $actividad->texto_previo_folleto;
                $incluye = $actividad->incluye;
                $precio = $actividad->precio . ' €';

                $lugar = '';
                if (strpos($actividad->aula->nombre, 'Exterior') !== false) {
                    $lugar = 'En ' . $actividad->lugar_celebracion;
                }
                else{
                    if(strpos($actividad->aula->nombre, 'Casa') !== false){
                        $lugar = \App\Funciones\Centros::CASA;
                    }
                    if(strpos($actividad->aula->nombre, 'Centro') !== false){
                        $lugar = \App\Funciones\Centros::JUVENTUD;
                    }
                    if(strpos($actividad->aula->nombre, 'Matas') !== false){
                        $lugar = \App\Funciones\Centros::MATAS;
                    }
                }
                // Creamos fechas según son el mismo día o en un periodo
                $inicio_fin = 'inicio - fin';
            
                if($actividad->fecha_inicio == $actividad->fecha_fin){
                    setlocale(LC_TIME, 'es_ES.UTF-8');
                    $inicio_fin =  ucfirst( strftime('%A %e de %B',strtotime($actividad->fecha_inicio) ) ).'.';
                }
                else{
                    setlocale(LC_TIME, 'es_ES.UTF-8');
                    if( strftime( '%B', strtotime($actividad->fecha_inicio) ) == strftime('%B', strtotime($actividad->fecha_fin))){
                        $inicio_fin =  strftime( 'Del %e', strtotime($actividad->fecha_inicio) ).' al '.strftime('%e de %B', strtotime($actividad->fecha_fin)).'.';
                        $inicio_fin = preg_replace('/( ){2,}/u',' ',$inicio_fin);
                        $inico_fin = str_replace('bado', 'bados', $inicio_fin);
                    }            
                    else
                        $inicio_fin =  ucfirst(Carbon::parse($actividad->fecha_inicio)->formatLocalized('%A') ) . strftime( ' del %e de %B', strtotime($actividad->fecha_inicio) ).' al '.strftime('%e de %B', strtotime($actividad->fecha_fin)).'.';
                }
                // Creamos la fecha de horario
                $horario =  'De ' . Carbon::parse( $actividad->hora_inicio)->format('G:i') . ' a ' . Carbon::parse( $actividad->hora_fin)->format('G:i') . ' h.';
                // Horas de la actividad
                $horas = $actividad->horas . ' horas.';
                
                if($categoria_id == 1)
                {
                    $phpExcel   ->setActiveSheetIndex(0)
                                ->setCellValue('A'.$i, $nombre)
                                ->setCellValue('B'.$i, $edad)
                                ->setCellValue('C'.$i, $texto_previo_folleto)
                                ->setCellValue('D'.$i, $inicio_fin )
                                ->setCellValue('E'.$i, $precio )
                                ->setCellValue('F'.$i, $lugar)
                                ->setCellValue('G'.$i, $incluye);
                    $i++;
                }
                else{
                    $phpExcel   ->setActiveSheetIndex(0)
                                ->setCellValue('A'.$i, $nombre)
                                ->setCellValue('B'.$i, $edad)
                                ->setCellValue('C'.$i, $texto_previo_folleto)
                                ->setCellValue('D'.$i, $inicio_fin )
                                ->setCellValue('E'.$i, $precio )
                                ->setCellValue('F'.$i, $horario)
                                ->setCellValue('G'.$i, $horas)
                                ->setCellValue('H'.$i, $lugar);
                    $i++;
                }
            }
            
            // Renombramos la hoja
            $phpExcel->getActiveSheet()->setTitle('Folleto - '.$categoria->id);

            // Activamos la primera hoja como activa, así Excel abre esta como primera
            $phpExcel->setActiveSheetIndex(0);

            // // Cabeceras para descargar el archivo csv
            // header('Content-Type: text/csv');
            // header('Content-Disposition: attachment;filename="'.session()->get('campaniaActiva')[0] .' - folleto de actividades - '.$categoria->nombre.'.csv"');
            // header('Cache-Control: max-age=0');
            // // If you're serving to IE 9, then the following may be needed
            // header('Cache-Control: max-age=1');

            // // If you're serving to IE over SSL, then the following may be needed
            // header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            // header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            // header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            // header ('Pragma: public'); // HTTP/1.0  */
    
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Csv($phpExcel);
            // Delimitador la coma ,
            $writer->setDelimiter(",");
            // Fin del dato entre comillas "
            $writer->setEnclosure('"');
            // Fin de la linea el retorno de carro y salto de línea
            $writer->setLineEnding("\r\n");
            $writer->setSheetIndex(0);

            $writer->setUseBOM(true);
            
            $writer->setPreCalculateFormulas(false);
            $writer->save("csv/demo.csv");

            $file_data = file_get_contents('csv/demo.csv');
            $utf8_file_data = utf8_encode($file_data);
            $csv = iconv("UTF-8", "WINDOWS-1252//IGNORE", $file_data);
            $new_file_name = 'csv/demo2.csv';
            file_put_contents($new_file_name , $csv );
            $file_data = file_get_contents('csv/demo2.csv');
            // collect the data to the be returned to the user, no need to save to disk
            // unless you really want to, if so, use file_put_contents()
            $dataForFile="test data to be returned to user in a file";
            
            header('Content-type: application/x-download');
            header('Content-Disposition: attachment; filename="'.session()->get('campaniaActiva')[0] .' - folleto de actividades - '.$categoria->nombre.'.csv"');
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: '.strlen($file_data));
            set_time_limit(0);
            echo $file_data;

            exit;
        }
    }
}
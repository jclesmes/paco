<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\DosierFormRequest;

use App\Actividad;
use App\Dosier;

use Carbon\Carbon;

//Almacenado
use Illuminate\Support\Facades\Storage;

class DosierController extends Controller
{
    public function index(){
        $actividades = Actividad::all();
        return view('pdfs/indexDosier', compact('actividades'));
    }

    /**
     * Muestra el formulario para crear un nuevo dosier.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $actividad = Actividad::whereId($id)->firstOrFail();
        return view('dosier.crear', compact('actividad'));
    }

    /**
     * Actualiza y guarda el dosier.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \App\Http\Requests\DosierFormRequest
     */
    public function store(DosierFormRequest $request, $id)
    {
        $actividad = Actividad::whereId($id)->firstOrFail();
        $dosier = Dosier::where('actividad_id', $id)->first();
        
        
        if (count($dosier)<1){
            $dosier = new Dosier();
            if( $request->file('url_imagen'))
            {
                $path = Storage::disk('public')->put('dosier', $request->file('url_imagen'));
                $dosier->url_imagen =  asset('storage/'.$path);
            }
            $dosier->actividad_id = $id;
            $dosier->descripcion_dosier = $request->input('descripcion_dosier');
            $dosier->contenido_dosier = $request->input('contenido_dosier');
            $dosier->save();
            return redirect('/descargas/pdf/dosier/')->with('mensaje', 'Dosier guardado');  
        }
        else{
            if( $request->file('url_imagen'))
            {
                $path = Storage::disk('public')->put('dosier', $request->file('url_imagen'));
                $dosier->url_imagen =  asset('storage/'.$path);
            }
            $dosier->descripcion_dosier = $request->input('descripcion_dosier');
            $dosier->contenido_dosier = $request->input('contenido_dosier');
            $dosier->update();
            return redirect('/descargas/pdf/dosier/')->with('mensaje', 'Dosier modificado');  
        }
        
    }

    /*public function crearDosier($id){
        $actividad = Actividad::whereId($id)->firstOrFail();
        $nombre = $actividad->nombre;
        $fechas = '';
        
        if( $actividad->edad_maxima < 1 )
            $edad = 'A partir de ' . $actividad->edad_minima . ' años';
        else
            $edad = 'De ' .  $actividad->edad_minima . ' a ' .  $actividad->edad_maxima . ' años';
        $precio = $actividad->precio . ' €';
        $incluye = $actividad->incluye;
        $descripcion = $actividad->dosier->descripcion_dosier;
        $contenido = $actividad->dosier->contenido_dosier;

        $lugar = '';
        if (strpos($actividad->aula->nombre, 'Exterior') !== false) {
            $lugar = 'En ' . $actividad->lugar_celebracion;
        }
        else{
            if(strpos($actividad->aula->nombre, 'Casa') !== false){
                $lugar = \App\Funciones\Centros::CASA;
            }
            if(strpos($actividad->aula->nombre, 'Centro') !== false){
                $lugar = \App\Funciones\Centros::JUVENTUD;
            }
            if(strpos($actividad->aula->nombre, 'Matas') !== false){
                $lugar = \App\Funciones\Centros::MATAS;
            }
        }
        // Creamos fechas según son el mismo día o en un periodo
        $inicio_fin = 'inicio - fin';
    
        if($actividad->fecha_inicio == $actividad->fecha_fin){
            setlocale(LC_TIME, 'es_ES.UTF-8');
            $inicio_fin =  ucfirst( strftime('%A %e de %B',strtotime($actividad->fecha_inicio) ) );
        }
        else{
            setlocale(LC_TIME, 'es_ES.UTF-8');
            if( strftime( '%B', strtotime($actividad->fecha_inicio) ) == strftime('%B', strtotime($actividad->fecha_fin))){
                $inicio_fin =  strftime( 'Del %e', strtotime($actividad->fecha_inicio) ).' al '.strftime('%e de %B', strtotime($actividad->fecha_fin)).'.';
                $inicio_fin = preg_replace('/( ){2,}/u',' ',$inicio_fin);
                $inico_fin = str_replace('bado', 'bados', $inicio_fin);
            }            
            else
                $inicio_fin =  ucfirst(Carbon::parse($actividad->fecha_inicio)->formatLocalized('%A') ) . strftime( ' del %e de %B', strtotime($actividad->fecha_inicio) ).' al '.strftime('%e de %B', strtotime($actividad->fecha_fin)).'.';
        }
        // Creamos la fecha de horario
        $horario =  'De ' . Carbon::parse( $actividad->hora_inicio)->format('G:i') . ' a ' . Carbon::parse( $actividad->hora_fin)->format('G:i') . ' h.';
        // Horas de la actividad
        $horas = $actividad->horas . ' horas.';
        $categoria = $actividad->categoria->nombre;
        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
        $html = view('pdfs/dosier')->with(compact('actividad', 'fechas', 'edad', 'lugar')) 
                                    ->with(compact('descripcion', 'contenido', 'inicio_fin', 'categoria'))->render(); 
        $mpdf->WriteHTML($html, 0);
        $nombre = $actividad->nombre.' - '.$actividad->grupo->nombre;
        $mpdf->SetTitle($nombre);
        $mpdf->SetAuthor("Francisco Piedras Pérez");
        $mpdf->SetSubject('Actividade RozasJoven');
        $mpdf->SetKeywords('Actividad, RozasJoven, Las Rozas'); 
        $mpdf->Output(public_path('pdfs/').$nombre.'.pdf', \Mpdf\Output\Destination::FILE);  
        $mpdf->Output($nombre.'.pdf',\Mpdf\Output\Destination::INLINE);     
        //return $mpdf;
     }*/

     public function crearDosier1($id){
        $actividad = Actividad::whereId($id)->firstOrFail();
        $nombre = $actividad->nombre;
        $fechas = '';
        
        if( $actividad->edad_maxima < 1 )
            $edad = 'A partir de ' . $actividad->edad_minima . ' años';
        else
            $edad = 'De ' .  $actividad->edad_minima . ' a ' .  $actividad->edad_maxima . ' años';
        $precio = $actividad->precio . ' €';
        $incluye = $actividad->incluye;
        $descripcion = $actividad->dosier->descripcion_dosier;
        $contenido = $actividad->dosier->contenido_dosier;

        $lugar = '';
        if (strpos($actividad->aula->nombre, 'Exterior') !== false) {
            $lugar = 'En ' . $actividad->lugar_celebracion;
        }
        else{
            if(strpos($actividad->aula->nombre, 'Casa') !== false){
                $lugar = \App\Funciones\Centros::CASA;
            }
            if(strpos($actividad->aula->nombre, 'Centro') !== false){
                $lugar = \App\Funciones\Centros::JUVENTUD;
            }
            if(strpos($actividad->aula->nombre, 'Matas') !== false){
                $lugar = \App\Funciones\Centros::MATAS;
            }
        }
        // Creamos fechas según son el mismo día o en un periodo
        $inicio_fin = 'inicio - fin';
    
        if($actividad->fecha_inicio == $actividad->fecha_fin){
            setlocale(LC_TIME, 'es_ES.UTF-8');
            $inicio_fin =  ucfirst( strftime('%A %e de %B',strtotime($actividad->fecha_inicio) ) );
        }
        else{
            setlocale(LC_TIME, 'es_ES.UTF-8');
            if( strftime( '%B', strtotime($actividad->fecha_inicio) ) == strftime('%B', strtotime($actividad->fecha_fin))){
                $inicio_fin =  strftime( 'Del %e', strtotime($actividad->fecha_inicio) ).' al '.strftime('%e de %B', strtotime($actividad->fecha_fin)).'.';
                $inicio_fin = preg_replace('/( ){2,}/u',' ',$inicio_fin);
                $inico_fin = str_replace('bado', 'bados', $inicio_fin);
            }            
            else
                $inicio_fin =  ucfirst(Carbon::parse($actividad->fecha_inicio)->formatLocalized('%A') ) . strftime( ' del %e de %B', strtotime($actividad->fecha_inicio) ).' al '.strftime('%e de %B', strtotime($actividad->fecha_fin)).'.';
        }
        // Creamos la fecha de horario
        $horario =  'De ' . Carbon::parse( $actividad->hora_inicio)->format('G:i') . ' a ' . Carbon::parse( $actividad->hora_fin)->format('G:i') . ' h.';
        // Horas de la actividad
        $horas = $actividad->horas . ' horas.';
        $categoria = $actividad->categoria->nombre;
        return view('pdfs/dosier')->with(compact('actividad', 'fechas', 'edad', 'lugar')) 
                                    ->with(compact('descripcion', 'contenido', 'inicio_fin', 'categoria'))->render(); 

     }

     public function crearDosier($id){
        $actividad = Actividad::whereId($id)->firstOrFail();
        $nombre = $actividad->nombre;
        $fechas = '';
        
        if( $actividad->edad_maxima < 1 )
            $edad = 'A partir de ' . $actividad->edad_minima . ' años';
        else
            $edad = 'De ' .  $actividad->edad_minima . ' a ' .  $actividad->edad_maxima . ' años';
        $precio = $actividad->precio . ' €';
        $incluye = $actividad->incluye;
        $descripcion = $actividad->dosier->descripcion_dosier;
        $contenido = $actividad->dosier->contenido_dosier;

        $lugar = '';
        if (strpos($actividad->aula->nombre, 'Exterior') !== false) {
            $lugar = 'En ' . $actividad->lugar_celebracion;
        }
        else{
            if(strpos($actividad->aula->nombre, 'Casa') !== false){
                $lugar = \App\Funciones\Centros::CASA;
            }
            if(strpos($actividad->aula->nombre, 'Centro') !== false){
                $lugar = \App\Funciones\Centros::JUVENTUD;
            }
            if(strpos($actividad->aula->nombre, 'Matas') !== false){
                $lugar = \App\Funciones\Centros::MATAS;
            }
        }
        // Creamos fechas según son el mismo día o en un periodo
        $inicio_fin = 'inicio - fin';
    
        if($actividad->fecha_inicio == $actividad->fecha_fin){
            setlocale(LC_TIME, 'es_ES.UTF-8');
            $inicio_fin =  ucfirst( strftime('%A %e de %B',strtotime($actividad->fecha_inicio) ) );
        }
        else{
            setlocale(LC_TIME, 'es_ES.UTF-8');
            if( strftime( '%B', strtotime($actividad->fecha_inicio) ) == strftime('%B', strtotime($actividad->fecha_fin))){
                $inicio_fin =  strftime( 'Del %e', strtotime($actividad->fecha_inicio) ).' al '.strftime('%e de %B', strtotime($actividad->fecha_fin)).'.';
                $inicio_fin = preg_replace('/( ){2,}/u',' ',$inicio_fin);
                $inico_fin = str_replace('bado', 'bados', $inicio_fin);
            }            
            else
                $inicio_fin =  ucfirst(Carbon::parse($actividad->fecha_inicio)->formatLocalized('%A') ) . strftime( ' del %e de %B', strtotime($actividad->fecha_inicio) ).' al '.strftime('%e de %B', strtotime($actividad->fecha_fin)).'.';
        }
        // Creamos la fecha de horario
        $horario =  'De ' . Carbon::parse( $actividad->hora_inicio)->format('G:i') . ' a ' . Carbon::parse( $actividad->hora_fin)->format('G:i') . ' h.';
        // Horas de la actividad
        $horas = $actividad->horas . ' horas.';
        $categoria = $actividad->categoria->nombre;
        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
        $mpdf->SetDisplayMode('fullpage');

        //$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list
        //$mpdf->max_colH_correction = 1.1;

        $html1 = view('pdfs/dosier1')->with(compact('actividad', 'fechas', 'edad', 'lugar')) 
                                    ->with(compact('descripcion', 'contenido', 'inicio_fin', 'categoria'))->render(); 
        $html2 = view('pdfs/dosier2')->with(compact('actividad', 'fechas', 'edad', 'lugar')) 
                                    ->with(compact('descripcion', 'contenido', 'inicio_fin', 'categoria'))->render(); 
        $mpdf->SetColumns(2,'J',5);
        $mpdf->WriteHTML($html1, 0);
        $mpdf->AddColumn();
        $mpdf->WriteHTML($html2, 0);
        $nombre = $actividad->nombre.' - '.$actividad->grupo->nombre;
        $mpdf->SetTitle($nombre);
        $mpdf->SetAuthor("Francisco Piedras Pérez");
        $mpdf->SetSubject('Actividade RozasJoven');
        $mpdf->SetKeywords('Actividad, RozasJoven, Las Rozas'); 
        $mpdf->Output(public_path('pdfs/').$nombre.'.pdf', \Mpdf\Output\Destination::FILE);  
        $mpdf->Output($nombre.'.pdf',\Mpdf\Output\Destination::INLINE);     
        //return $mpdf;
     }
}

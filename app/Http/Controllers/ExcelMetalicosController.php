<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use PhpOffice\PhpSpreadsheet\Shared\Font;

use App\Categoria;
use App\Actividad;

use Carbon\Carbon;
class ExcelMetalicosController extends Controller
{
    /**
     * Nos crea un excel con los datos de las metálicos de la campaña en sesión,
     * 
     *  @return .xlsx
     */   
    public function crearExcel(){

        $importeTotal = 0;
        $gastadoTotal = 0;

        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        date_default_timezone_set('Europe/Madrid');
        $phpExcel = new  Spreadsheet();
        // Encabezados
        $titulosColumnas = array('Actividad','Importe','fecha necesidad','fecha inicio','fecha justificacion',
                                'Dinero gastado','Dinero sobrante','Notas');
        
        // Propiedades del documento
        $phpExcel->getProperties()  ->setCreator("Francisco Piedras Pérez")
                                    ->setLastModifiedBy("Francisco Piedras Pérez")
                                    ->setTitle(session('campaniaActiva')[0]." - Actividades Trimestrales Metálicos")
                                    ->setSubject("Rozas Joven - Concejalía de Juventud")
                                    ->setDescription("Resumen de los metálicos de las actividadedes para la concejalía de juventud en Las Rozas.")
                                    ->setKeywords("Actividades metálicos")
                                    ->setCategory("Actividades");
        $i = 1; //Comienzo de las filas quitando los títulos

        $phpExcel   ->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Metálicos ('.session('campaniaActiva')[0].')');
        $phpExcel   ->getActiveSheet()->getStyle('A1:H1')->applyFromArray(\App\Funciones\Estilos::ESTILO_CABECERA);
        $phpExcel   ->getActiveSheet()->mergeCells('A1:H1');

        $campania_id = session()->get('campaniaActiva')[1];
        // Buscamos todas las actividades de esa campaña
        
        $actividades = Actividad::where([
            ['campania_id', $campania_id]
            ])->get();
        if(count($actividades) >0)
        {
            // Importes y dinero_gastado guardan la posción de los subtotales, para luego
            // añadirlo al total como suma

            $i++;
            $salto; // Variable para saber donde empieza la activdad
            $phpExcel->getActiveSheet()->getStyle('A'.$i.':H'.$i)->applyFromArray(\App\Funciones\Estilos::ESTILO_TITULOS);
            // CABECERA PRINCIPAL
            $phpExcel   ->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i,  $titulosColumnas[0])
                        ->setCellValue('B'.$i,  $titulosColumnas[1])
                        ->setCellValue('C'.$i,  $titulosColumnas[2])
                        ->setCellValue('D'.$i,  $titulosColumnas[3])
                        ->setCellValue('E'.$i,  $titulosColumnas[4])
                        ->setCellValue('F'.$i,  $titulosColumnas[5])
                        ->setCellValue('G'.$i,  $titulosColumnas[6])
                        ->setCellValue('H'.$i,  $titulosColumnas[7])
                    ;
            $i++;

            // Recorremos las actividaddes
            foreach ($actividades as $actividad) {
                //Miramos si tiene metálicos, si no tiene pasamos
                if(count($actividad->metalicos)>0){
                    //Nombre de la actividad
                    $phpExcel   ->setActiveSheetIndex(0)
                    ->setCellValue('A'.$i, $actividad->nombre);
                    $phpExcel   ->getActiveSheet()->getStyle('A'.$i.':H'.$i)->applyFromArray(\App\Funciones\Estilos::ESTILO_ACTIVIDAD);
                    $i++;
                    $salto = $i;
                    //Ahora debemos recorrer los metálicos de la actividad
                    foreach ($actividad->metalicos as $metalico){
                    $phpExcel   ->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i, $metalico->concepto)
                        ->setCellValue('B'.$i, $metalico->importe)
                        ->setCellValue('C'.$i, Carbon::parse($metalico->fecha_necesidad)->format('Y-m-d'))
                        ->setCellValue('D'.$i, Carbon::parse($actividad->fecha_inicio)->format('Y-m-d') )
                        ->setCellValue('E'.$i, Carbon::parse($metalico->fecha_justificacion)->format('Y-m-d') )
                        ->setCellValue('F'.$i, $metalico->dinero_gastado)
                        ->setCellValue('G'.$i, '=(B'.$i.'-F'.$i.')')
                        ->setCellValue('H'.$i, $metalico->notas);
                    $phpExcel   ->getActiveSheet()->getStyle('A'.$i.':H'.$i)->applyFromArray(\App\Funciones\Estilos::ESTILO_METALICO);
                    $phpExcel   ->getActiveSheet()->getStyle('B'.$i)->getNumberFormat()->setFormatCode('#,##0.00_-"€"');
                    $phpExcel   ->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode('#,##0.00_-"€"');
                    $phpExcel   ->getActiveSheet()->getStyle('G'.$i)->getNumberFormat()->setFormatCode('#,##0.00_-"€"');
                    $i++;
                    $importeTotal += (int) $metalico->importe;
                    $gastadoTotal += (int) $metalico->dinero_gastado;        
                    }
                    $phpExcel   ->setActiveSheetIndex(0)->setCellValue('A'.$i, 'Subtotal');

                    $phpExcel   ->setActiveSheetIndex(0)->setCellValue('B'.$i, '=SUM(B'.$salto.':B'.($i-1).')');
                    $phpExcel   ->getActiveSheet()->getStyle('B'.$i)->getNumberFormat()->setFormatCode('#,##0.00_-"€"');

                    $phpExcel   ->setActiveSheetIndex(0)->setCellValue('F'.$i, '=SUM(F'.$salto.':F'.($i-1).')');
                    $phpExcel   ->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode('#,##0.00_-"€"');

                    $phpExcel   ->setActiveSheetIndex(0)->setCellValue('G'.$i, '=(B'.$i.'-F'.($i).')');
                    $phpExcel   ->getActiveSheet()->getStyle('G'.$i)->getNumberFormat()->setFormatCode('#,##0.00_-"€"');

                    $phpExcel   ->getActiveSheet()->getStyle('A'.$i.':H'.$i)->applyFromArray(\App\Funciones\Estilos::ESTILO_SUBTOTAL);

                    $i++; 
                }           
            }
        }        
 
        $phpExcel   ->setActiveSheetIndex(0)->setCellValue('A'.$i, 'Total');
        
        $phpExcel   ->setActiveSheetIndex(0)->setCellValue('B'.$i, $importeTotal );
        $phpExcel   ->getActiveSheet()->getStyle('B'.$i)->getNumberFormat()->setFormatCode('#,##0.00_-"€"');

        $phpExcel   ->setActiveSheetIndex(0)->setCellValue('F'.$i, $gastadoTotal);
        $phpExcel   ->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode('#,##0.00_-"€"');

        $phpExcel   ->setActiveSheetIndex(0)->setCellValue('G'.$i, '=(B'.$i.'-F'.$i.')');
        $phpExcel   ->getActiveSheet()->getStyle('G'.$i)->getNumberFormat()->setFormatCode('#,##0.00_-"€"');

        $phpExcel   ->getActiveSheet()->getStyle('A'.$i.':H'.$i)->applyFromArray(\App\Funciones\Estilos::ESTILO_TOTAL);

        foreach(range('A','H') as $columnID)
        {
            $phpExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }

        // Renombramos la hoja
        $phpExcel->getActiveSheet()->setTitle('Metálicos');

        // Activamos la primera hoja como activa, así Excel abre esta como primera
        $phpExcel->setActiveSheetIndex(0);

        // Cabeceras para el archivo tipo excel (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.session()->get('campaniaActiva')[0].' - Metálicos.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0 

        //$objWriter = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($phpExcel, 'Xlsx');
        //$objWriter->save('php://output');
        $writer = new Xlsx($phpExcel);
        $writer->setPreCalculateFormulas(false);
        $writer->save('php://output');
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Empresa;
use App\Profesor;
use App\Http\Requests\ProfesorFormRequest;
class ProfesorsController extends Controller
{
    /**
     * Mostramos todos los prefesores.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profesores = Profesor::orderBy('nombre_completo', 'asc')->paginate(15);
        return view('profesores.index', compact('profesores'));
    }

    /**
     * Mostramos el formulario de creación de profesor.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
        $empresas = Empresa::orderBy('nombre_empresa', 'asc')->pluck('nombre_empresa', 'id'); 
        return view('profesores.crear', compact('empresas'));
    }

    /**
     * Guardamos un nuevo profesor.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProfesorFormRequest $request)
    {
        $profesor = new Profesor($request->all());
        $profesor->save();
        return redirect('/profesores')->with('mensaje', 'Profesor guardado');  
    }

    /**
     * Editamos un profesor específico.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empresas = Empresa::orderBy('nombre_empresa', 'asc')->pluck('nombre_empresa', 'id'); 
        $profesor = Profesor::whereId($id)->firstOrFail();
        return view('profesores.edit', compact('profesor', 'empresas'));
    }

    /**
     * Guardamos y actualizamos un profesor.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profesor = Profesor::whereId($id)->firstOrFail();
        $profesor->update($request->all());
        return redirect('profesores')->with('mensaje', 'Profesor modificado');
    }

    /**
     * Eliminamos un profesor.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profesor = Profesor::whereId($id)->firstOrFail();
        $profesor->delete();

        return redirect('/profesores')->with('mensaje', 'Profesor eliminado');
    }

    /**
     * Mostramos el aviso de eliminación de profesor.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function borrar($id)
    {
        $profesor = Profesor::whereId($id)->firstOrFail();
        return view('profesores.borrar', compact('profesor'));
    }
}

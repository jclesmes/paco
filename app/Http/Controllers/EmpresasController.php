<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

Use App\Empresa;
use App\Contacto;
use App\Profesor;
class EmpresasController extends Controller
{
    /**
     * Muestra un listado de empresas.
     * Ordenado por el nombre de la empresa
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empresas = Empresa::orderBy('nombre_empresa', 'asc')->paginate(15);
        return view('empresas.index', compact('empresas')); 
    }

    /**
     * Muestra el formulario de creación de empresa.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('empresas.crear');
    }

    /**
     * Guarda una nueva empresa.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $empresa = new Empresa( $request->all());
        $empresa->save();
        return redirect('/empresas')->with('mensaje', 'Empresa guardada');  
    }

    /**
     * Muestra una empresa específica, junto con los contactos de ella
     * y los profesores si los tuviera.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empresa = Empresa::whereId($id)->firstOrFail();
        $contactos = Contacto::where('empresa_id', $id)->get();
        $profesores = Profesor::where('empresa_id', $id)->get();
        return view('empresas.show', compact('empresa', 'contactos', 'profesores'));
    }

    /**
     * Muestra el formulario para editar una empresa.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empresa = Empresa::whereId($id)->firstOrFail();
        return view('empresas.edit', compact('empresa'));
    }

    /**
     * Actualiza y guarda una empresa.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $empresa = Empresa::whereId($id)->firstOrFail();
        $empresa->update($request->all());
        $empresa->save();
        return redirect('/empresas')->with('mensaje', 'Empresa modificada');
    }

    /**
     * Elimina una empresa específica.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $empresa = Empresa::whereId($id)->firstOrFail();
        $empresa->delete();

        return redirect('/empresas')->with('mensaje', 'Empresa eliminada');
    }

    /**
     * Muestra un aviso antes de eliminar una empresa.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function borrar($id)
    {
        $empresa = Empresa::whereId($id)->firstOrFail();
        $profesores = Profesor::where('empresa_id', $empresa->id)->get();
        $contactos = Contacto::where('empresa_id', $empresa->id)->get();
        return view('empresas.borrar', compact('empresa', 'contactos', 'profesores'));
    }
}

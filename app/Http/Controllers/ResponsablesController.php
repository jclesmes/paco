<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Responsable;
use App\Actividad;
class ResponsablesController extends Controller
{
    /**
     * Mostramos todos los responsables.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $responsables = Responsable::all();
        return view('responsables.index', compact('responsables'));
    }

    /**
     * Mostramos el formulario de creación de responsables.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('responsables.crear');
    }

    /**
     * Guardamos un nuevo responsable.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $responsable = new Responsable;
        $responsable->nombre = $request->nombre;
        $responsable->save();
        return redirect('/responsables')->with('mensaje', 'Responsable guardado'); 
    }

    /**
     * Mostramos un responsable, ademas de sus actividades de campaña.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $responsable = Responsable::whereId($id)->firstOrFail();
        $campania = session('campaniaActiva');
        $campania_id = $campania[1];
        $actividades = Actividad::where([
            ['responsable_id',$id],
            ['campania_id', $campania_id] 
        ])->orderBy('fecha_inicio')->get();
        return view('responsables.show', compact('responsable', 'actividades'));
    }

    /**
     * Mostramos el formulario de editar responsable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $responsable = Responsable::whereId($id)->firstOrFail();
        return view('responsables.edit', compact('responsable'));
    }

    /**
     * Actualizamos y guardamos un responsable.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $responsable = Responsable::whereId($id)->firstOrFail();
        $responsable->nombre = $request->get('nombre');
        $responsable->save();

        return redirect('/responsables')->with('mensaje', 'Responsable modificad@');
    }

    /**
     * Eliminamos un responsable especificado.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $responsable = Responsable::whereId($id)->firstOrFail();
        $responsable->delete();

        return redirect('/responsables')->with('mensaje', 'Responsable eliminada');
    }

    /**
     * Mostramos el aviso de eliminar responsable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function borrar($id)
    {
        $responsable = Responsable::whereId($id)->firstOrFail();
        return view('responsables.borrar', compact('responsable'));
    }
}

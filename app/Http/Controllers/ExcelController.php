<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use PhpOffice\PhpSpreadsheet\Shared\Font;

use App\Categoria;
use App\Actividad;

use Carbon\Carbon;
class ExcelController extends Controller
{
    /**
     * Nos crea un excel con los datos de las actividades de la campaña en sesión,
     * además si se ha filtrado por categoría solo nos pondrá las actividades de
     * esa categoría
     * 
     *  @return .xlsx
     */   
    public function index()
    {
        $id = session()->get('categoria');
        $cam = session()->get('campaniaActiva');

         if($id[0]==0 || $id[0]==null)
         {
            $categorias = Categoria::All();
            $nombre_archivo = 'Actividades';
         }
        else {
            $categorias = Categoria::where('id',$id[0])->get();
            $nombre_archivo = $categorias->get(0)->nombre;
        }
        
        $this->crearExcel($categorias, $nombre_archivo);
    }
        
    /**
     * Función para generar un archivo excel con las actividades.
     *
     * @param Categoria array() $categoria
     * @param  string  $nombre_archivo
     * @return .xlsx Genera un archivo excel con los datos
     */
    public function crearExcel( $categorias=array(Categoria), $nombre_archivo ){
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        date_default_timezone_set('Europe/Madrid');
        $phpExcel = new  Spreadsheet();
        // Encabezados
        $titulosColumnas = array('ACTIVIDAD','GRUPO','RESPONSABLE','FECHA INICIO','FECHA FIN',
                            'DIAS','AULA','HORARIO INICIO','HORARIO FIN', 'HORAS', 'PRECIO', 'COSTE_EXT_MÁX', 'PAR. MIN', 'PAR. MAX',	
                            'EDAD MIN',	'EDAD MAX',	'FECHA INSCRIPCIONES', 'INCLUYE', 'TEXTO PREVIO FOLLETO', 
                            'LUGAR DE CELEBRACIÓN', 'OBSERVACIONES');
       
        // Propiedades del documento
        $phpExcel->getProperties()  ->setCreator("Francisco Piedras Pérez")
                                    ->setLastModifiedBy("Francisco Piedras Pérez")
                                    ->setTitle(session('campaniaActiva')[0]." - Actividades Trimestrales")
                                    ->setSubject("Rozas Joven - Concejalía de Juventud")
                                    ->setDescription("Resumen de actividadedes para la concejalía de juventud en Las Rozas.")
                                    ->setKeywords("Actividades")
                                    ->setCategory("Actividades");
        //Comienzo de las filas quitando los títulos
        $i = 2; 
        $i_inicio =4;

        $guardaTotales = array();

        $phpExcel   ->setActiveSheetIndex(0)
                    ->setCellValue('A1',  session('campaniaActiva')[0]);
        $phpExcel   ->getActiveSheet()->getStyle('A1:U1')->applyFromArray(\App\Funciones\Estilos::ESTILO_CAMPANIA);

        foreach ($categorias as $categoria)
        {
            $campania_id = session()->get('campaniaActiva')[1];
            // Buscamos todas las actividades de esa catgoría
            $actividades = Actividad::where([
                ['categoria_id', $categoria->id], 
                ['campania_id', $campania_id]
                ])->orderBy('fecha_inicio')->get();
            if(count($actividades) >0)
            {
            // Añadimos el nombre de la categoria
            $phpExcel   ->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i,  $categoria->nombre);
            $phpExcel   ->getActiveSheet()->getStyle('A'.$i.':U'.$i)->applyFromArray(\App\Funciones\Estilos::ESTILO_CATEGORIAS[$categoria->id]);
            $i++;
            $phpExcel->getActiveSheet()->getStyle('A'.$i.':U'.$i)->applyFromArray(\App\Funciones\Estilos::ESTILO_TITULO_COLUMNAS);
            // CABECERA DE CADA CATEGORIA
            $phpExcel   ->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i,  $titulosColumnas[0]) 
                        ->setCellValue('B'.$i,  $titulosColumnas[1])
                        ->setCellValue('C'.$i,  $titulosColumnas[2])
                        ->setCellValue('D'.$i,  $titulosColumnas[3])
                        ->setCellValue('E'.$i,  $titulosColumnas[4])
                        ->setCellValue('F'.$i,  $titulosColumnas[5])
                        ->setCellValue('G'.$i,  $titulosColumnas[6])
                        ->setCellValue('H'.$i,  $titulosColumnas[7])
                        ->setCellValue('I'.$i,  $titulosColumnas[8])
                        ->setCellValue('J'.$i,  $titulosColumnas[9])
                        ->setCellValue('K'.$i,  $titulosColumnas[10])
                        ->setCellValue('L'.$i,  $titulosColumnas[11])
                        ->setCellValue('M'.$i,  $titulosColumnas[12])
                        ->setCellValue('N'.$i,  $titulosColumnas[13])
                        ->setCellValue('O'.$i,  $titulosColumnas[14])
                        ->setCellValue('P'.$i,  $titulosColumnas[15])
                        ->setCellValue('Q'.$i,  $titulosColumnas[16])
                        ->setCellValue('R'.$i,  $titulosColumnas[17])
                        ->setCellValue('S'.$i,  $titulosColumnas[18])
                        ->setCellValue('T'.$i,  $titulosColumnas[19])
                        ->setCellValue('U'.$i,  $titulosColumnas[19]);
            $i++;

            // Actividades de esa categoría
            foreach ($actividades as $actividad) {
                $horas = str_replace(',','.',$actividad->horas);
                $phpExcel   ->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i, $actividad->nombre)
                            ->setCellValue('B'.$i, $actividad->grupo->nombre)
                            ->setCellValue('C'.$i, $actividad->responsable->nombre)
                            ->setCellValue('D'.$i, Carbon::parse($actividad->fecha_inicio)->format('Y-m-d') )
                            ->setCellValue('E'.$i, Carbon::parse($actividad->fecha_fin)->format('Y-m-d') )
                            ->setCellValue('F'.$i, $actividad->dias)
                            ->setCellValue('G'.$i, $actividad->aula->nombre)
                            ->setCellValue('H'.$i, Carbon::parse( $actividad->hora_inicio)->format('G:i') )
                            ->setCellValue('I'.$i, Carbon::parse( $actividad->hora_fin)->format('G:i') )
                            ->setCellValue('J'.$i, floatval($horas) )
                            ->setCellValue('K'.$i, $actividad->precio)
                            ->setCellValue('L'.$i, $actividad->coste_estimado_maximo)
                            ->setCellValue('M'.$i, $actividad->par_min)
                            ->setCellValue('N'.$i, $actividad->par_max)
                            ->setCellValue('O'.$i, $actividad->edad_minima)
                            ->setCellValue('P'.$i, $actividad->edad_maxima)
                            ->setCellValue('Q'.$i, Carbon::parse($actividad->fecha_inscripciones)->format('Y-m-d'))
                            ->setCellValue('R'.$i, $actividad->incluye)
                            ->setCellValue('S'.$i, $actividad->texto_previo_folleto)
                            ->setCellValue('T'.$i, $actividad->lugar_celebracion)
                            ->setCellValue('U'.$i, $actividad->observaciones);
                
                $phpExcel   ->getActiveSheet()->getStyle('H'.$i)
                            ->getNumberFormat()
                            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_TIME3);
                $phpExcel   ->getActiveSheet()->getStyle('I'.$i)
                            ->getNumberFormat()
                            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_TIME3);
                            
                $phpExcel   ->getActiveSheet()->getStyle('J'.$i)
                            ->getNumberFormat()
                            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);
                $phpExcel   ->getActiveSheet()->getStyle('K'.$i)->getNumberFormat()->setFormatCode('#,##0.00_-"€"');
                $phpExcel   ->getActiveSheet()->getStyle('L'.$i)->getNumberFormat()->setFormatCode('#,##0.00_-"€"');
                $i++;
            }
            $phpExcel   ->getActiveSheet()->getStyle('A'.$i.':U'.$i)->applyFromArray(\App\Funciones\Estilos::ESTILO_TOTALES);
            $phpExcel   ->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i, 'TOTALES',
                            \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_FORMULA)
                        ->setCellValueExplicit('B'.$i, '=COUNTIF(B'.$i_inicio.':B'. ($i-1) .',"*")',
                            \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_FORMULA)
                        ->setCellValueExplicit('J'.$i, '=SUM(J'.$i_inicio.':J'.($i-1).')',
                            \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_FORMULA)
                        ->setCellValueExplicit('L'.$i, '=SUM(L'.$i_inicio.':L'.($i-1).')',
                            \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_FORMULA);

            $phpExcel   ->getActiveSheet()
                        ->getStyle('H'.$i)->getNumberFormat()
                        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_TIME3);

            $phpExcel   ->getActiveSheet()->getStyle('I'.$i)
                        ->getNumberFormat()
                        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_TIME3);
                    
            $phpExcel   ->getActiveSheet()->getStyle('J'.$i)
                        ->getNumberFormat()
                        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);

            $phpExcel   ->getActiveSheet()->getStyle('K'.$i)->getNumberFormat()->setFormatCode('#,##0.00_-"€"');
            $phpExcel   ->getActiveSheet()->getStyle('L'.$i)->getNumberFormat()->setFormatCode('#,##0.00_-"€"');            

            $i_inicio = $i+3;
            array_push($guardaTotales, $i);
            $i++;
        }        
    }   

    // Ahora escribimos los totales
    $b = $j =$k =$l = '=SUM(';

    foreach ($guardaTotales as $posicion) {
        $b = $b . 'B'.$posicion.'+';
    };
    $b = substr( $b , 0 , -1);
    $b = $b.')';

    foreach ($guardaTotales as $posicion) {
        $j = $j . 'J'.$posicion.'+';
    };
    $j = substr( $j , 0 , -1);
    $j = $j.')';

    $k = substr( $k , 0 , -1);
    $k = $k.')';

    foreach ($guardaTotales as $posicion) {
        $l = $l . 'L'.$posicion.'+';
    };
    $l = substr( $l , 0 , -1);
    $l = $l.')';

    $phpExcel   ->getActiveSheet()->getStyle('A'.$i.':U'.$i)->applyFromArray(\App\Funciones\Estilos::ESTILO_TOTALES_FIN);

    $phpExcel   ->setActiveSheetIndex(0)
                ->setCellValue('A'.$i, 'TOTALES',
                    \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_FORMULA)
                ->setCellValueExplicit('B'.$i, $b,
                    \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_FORMULA)
                ->setCellValueExplicit('J'.$i, $j,
                    \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_FORMULA)
                ->setCellValueExplicit('L'.$i, $l,
                    \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_FORMULA);

    $phpExcel   ->getActiveSheet()
                ->getStyle('H'.$i)->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_TIME3);

    $phpExcel   ->getActiveSheet()->getStyle('I'.$i)
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_TIME3);

    $phpExcel   ->getActiveSheet()->getStyle('J'.$i)
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);

    $phpExcel   ->getActiveSheet()->getStyle('L'.$i)->getNumberFormat()->setFormatCode('#,##0.00_-"€"');

    // Bloqueamos primera columna
    $phpExcel->getActiveSheet()->freezePane('B1');

    // Aplicamos ancho automático a las columnas
    foreach(range('A','U') as $columnID)
    {
        $phpExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
    }

    // Renombramos la hoja de cálculo
    $phpExcel->getActiveSheet()->setTitle('General');

    // Activamos la primera hoja como activa, así Excel abre esta como primera
    $phpExcel->setActiveSheetIndex(0);

    // Cabeceras para el archivo tipo excel (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.session()->get('campaniaActiva')[0].' - '. $nombre_archivo .'- '.Carbon::now().'.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0 

    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($phpExcel, 'Xlsx');
    $writer = new Xlsx($phpExcel);
    $writer->setPreCalculateFormulas(false);
    $writer->save('php://output');
    }
}
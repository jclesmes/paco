<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use PhpOffice\PhpSpreadsheet\Shared\Font;

use App\Categoria;
use App\Actividad;

use Carbon\Carbon;
class ExcelPrevencionController extends Controller
{
    /**
     * Nos crea un excel con los datos de prevención de la campaña en sesión
     * 
     *  @return .xlsx
     */   
    public function prevencion()
    {
        $campania_id = session()->get('campaniaActiva')[1];
        $i = 2; // Inicio filas
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        date_default_timezone_set('Europe/Madrid');
        $phpExcel = new  Spreadsheet();
        // Encabezados
        $titulosColumnasEmpresas = array('NOMBRE EMPRESA','CIF','RAZÓN SOCIAL','DIRECCIÓN','PROVINCIA',
                            'CIUDAD','PAÍS','C.P.', 'WEB', 'ACTIVIDAD', 'TELÉFONO', 'FAX');

        $titulosColumnasContactos = array('NOMBRE Y APELLIDOS', 'CORREO', 'PUESTO', 'TELÉFONO', 'MÓVIL');   
                     
        $titulosColumnasContratos = array('NOMBRE DE LOS SERVICIOS PRESTADOS PARA EL AYTO', 'FECHA INICIO CONTRATO', 
                                          'DURACIÓN ESTIMADA CONTRATO', 'CENTROS DEL AYTO DONDE SE DESARROLLA LA ACTIVIDAD' );        

        // Propiedades del archivo
        $phpExcel->getProperties()->setCreator("Francisco Piedras Pérez")
                                    ->setLastModifiedBy("Francisco Piedras Pérez")
                                    ->setTitle(session('campaniaActiva')[0]." - Prevención Risgos")
                                    ->setSubject("Rozas Joven - Concejalía de Juventud")
                                    ->setDescription("Prevención de riesgos para la Concejalía de Juventud en Las Rozas.")
                                    ->setKeywords("Prevención riesgos actividades")
                                    ->setCategory("Actividades");

        // Añadimos la cabecera con el título de campaña
        $phpExcel   ->setActiveSheetIndex(0)
                    ->setCellValue('B'.$i,  session('campaniaActiva')[0]);
        $phpExcel   ->getActiveSheet()->getStyle('B'.$i.':M'.$i)->applyFromArray(\App\Funciones\Estilos::ESTILO_PREVENCION_CAMPANIA);
        $phpExcel   ->getActiveSheet()->getStyle('B'.$i.':M'.$i)->applyFromArray(\App\Funciones\Estilos::BORDE);
        $phpExcel   ->getActiveSheet()->mergeCells('B'.$i.':M'.$i);
        $i++;
        // Añado fila en rosa
        $phpExcel   ->setActiveSheetIndex(0)
                    ->setCellValue('B'.$i, 'EMPRESA');
        $phpExcel   ->getActiveSheet()->mergeCells('B'.$i.':M'.$i);
        $phpExcel   ->getActiveSheet()->getStyle('B'.$i.':M'.$i)->applyFromArray(\App\Funciones\Estilos::ESTILO_PREVENCION_ROSA);
        $phpExcel   ->getActiveSheet()->getStyle('B'.$i.':M'.$i)->applyFromArray(\App\Funciones\Estilos::BORDE);
        //
        $i = 4; //Los títulos de la empresa empizan en la línea 4 
        $phpExcel   ->getActiveSheet()->getStyle('B'.$i.':M'.$i)->applyFromArray(\App\Funciones\Estilos::ESTILO_PREVENCION_EMPRESA_CABECERA);
        $phpExcel   ->getActiveSheet()->getStyle('B'.$i.':M'.$i)->applyFromArray(\App\Funciones\Estilos::BORDE_TODO);
        $phpExcel   ->setActiveSheetIndex(0)
                    ->setCellValue('B'.$i,  $titulosColumnasEmpresas[0])  //Titulo de las columnas
                    ->setCellValue('C'.$i,  $titulosColumnasEmpresas[1])
                    ->setCellValue('D'.$i,  $titulosColumnasEmpresas[2])
                    ->setCellValue('E'.$i,  $titulosColumnasEmpresas[3])
                    ->setCellValue('F'.$i,  $titulosColumnasEmpresas[4])
                    ->setCellValue('G'.$i,  $titulosColumnasEmpresas[5])
                    ->setCellValue('H'.$i,  $titulosColumnasEmpresas[6])
                    ->setCellValue('I'.$i,  $titulosColumnasEmpresas[7])
                    ->setCellValue('J'.$i,  $titulosColumnasEmpresas[8])
                    ->setCellValue('K'.$i,  $titulosColumnasEmpresas[9])
                    ->setCellValue('L'.$i,  $titulosColumnasEmpresas[10])
                    ->setCellValue('M'.$i,  $titulosColumnasEmpresas[11])
        ;
        $phpExcel   ->getActiveSheet()->getStyle('B'.$i.':M'.$i)->applyFromArray(\App\Funciones\Estilos::ESTILO_PREVENCION_TITULOS);
        $phpExcel   ->getActiveSheet()->getStyle('B'.$i.':M'.$i)->applyFromArray(\App\Funciones\Estilos::BORDE);
        $i++;
        
        $actividades_sin_filtro = Actividad::where('campania_id', $campania_id)->orderBy('fecha_inicio', 'ASC')->get();
        $actividades = array();
        $j = 0;
        $repetido = false;
        // Voy a coger solo las empresas una vez, para ello lo hago a traves de los profesores
        foreach($actividades_sin_filtro as $actividad){
            $repetido = false;
            foreach($actividades as $actividad_antigua  ){
                try{
                    if($actividad_antigua->profesor != null && $actividad->profesor != null )
                    { 
                        if($actividad_antigua->profesor->empresa_id == $actividad->profesor->empresa_id){
                            $repetido = true;
                            //echo 'IGUALES';
                        }
                        //echo 'Entrado con: actividad_antigua-> '. $actividad_antigua->profesor->empresa_id. ' y '.$actividad->profesor->empresa_id.'<br>';
                     } 
                }catch(\Exception $e){
                    //Nada que hacer
                }
            }
            if (!$repetido) {
                array_push($actividades,$actividades_sin_filtro->get($j));
            }
            $j++;
        }
        //dd($actividades, $j);
        foreach ($actividades as $actividad) {
            $phpExcel   ->getActiveSheet()->getStyle('B'.$i.':M'.$i)->applyFromArray(\App\Funciones\Estilos::ESTILO_PREVENCION_EMPRESA);
            $phpExcel   ->getActiveSheet()->getStyle('B'.$i.':M'.$i)->applyFromArray(\App\Funciones\Estilos::BORDE_TODO_FINO);
            //Compruebo si hay profesor
            if( $actividad->profesor != null ){
                // Compruebo si tiene empresa asociada
                if($actividad->profesor->empresa != null){
                    $phpExcel   ->setActiveSheetIndex(0)
                                ->setCellValue('B'.$i, $actividad->profesor->empresa->nombre_empresa)
                                ->setCellValue('C'.$i, $actividad->profesor->empresa->cif)
                                ->setCellValue('D'.$i, $actividad->profesor->empresa->razon_social)
                                ->setCellValue('E'.$i, $actividad->profesor->empresa->direccion)
                                ->setCellValue('F'.$i, $actividad->profesor->empresa->provincia)
                                ->setCellValue('G'.$i, $actividad->profesor->empresa->ciudad)
                                ->setCellValue('H'.$i, $actividad->profesor->empresa->pais)
                                ->setCellValue('I'.$i, $actividad->profesor->empresa->cp)
                                ->setCellValue('J'.$i, $actividad->profesor->empresa->web)
                                ->setCellValue('K'.$i, $actividad->profesor->empresa->actividad)
                                ->setCellValue('L'.$i, $actividad->profesor->empresa->telefono)
                                ->setCellValue('M'.$i, $actividad->profesor->empresa->fax);
                                $i++;
                }
            }
        }
        // Añado fila en rosa
        $phpExcel   ->setActiveSheetIndex(0)
                     ->setCellValue('B'.$i, 'PERSONA DE CONTACTO');
        $phpExcel   ->getActiveSheet()->getStyle('B'.$i.':M'.$i)->applyFromArray(\App\Funciones\Estilos::ESTILO_PREVENCION_ROSA);
        $phpExcel   ->getActiveSheet()->getStyle('B'.$i.':M'.$i)->applyFromArray(\App\Funciones\Estilos::BORDE_TODO);
        $phpExcel   ->getActiveSheet()->mergeCells('B'.$i.':M'.$i);
        $i++;
        $phpExcel   ->getActiveSheet()->mergeCells('B'.$i.':D'.$i);
        $phpExcel   ->getActiveSheet()->mergeCells('E'.$i.':G'.$i);
        $phpExcel   ->getActiveSheet()->mergeCells('H'.$i.':I'.$i);
        $phpExcel   ->getActiveSheet()->mergeCells('J'.$i.':K'.$i);
        $phpExcel   ->getActiveSheet()->mergeCells('L'.$i.':M'.$i);

        $phpExcel   ->setActiveSheetIndex(0)
                    ->setCellValue('B'.$i,  $titulosColumnasContactos[0])  
                    ->setCellValue('E'.$i,  $titulosColumnasContactos[1])
                    ->setCellValue('H'.$i,  $titulosColumnasContactos[2])
                    ->setCellValue('J'.$i,  $titulosColumnasContactos[3])
                    ->setCellValue('L'.$i,  $titulosColumnasContactos[4])
        ;
        $phpExcel   ->getActiveSheet()->getStyle('B'.$i.':M'.$i)->applyFromArray(\App\Funciones\Estilos::ESTILO_PREVENCION_TITULOS);
        $phpExcel   ->getActiveSheet()->getStyle('B'.$i.':M'.$i)->applyFromArray(\App\Funciones\Estilos::BORDE_TODO);
        $i++;
       //
       foreach ($actividades as $actividad) {
            //Compruebo si hay profesor
            if( $actividad->profesor != null ){
                // Compruebo si tiene contacto
                if($actividad->profesor->empresa->contacto != null){
                    $phpExcel   ->getActiveSheet()->getStyle('B'.$i.':M'.$i)->applyFromArray(\App\Funciones\Estilos::BORDE_TODO_FINO);
                    $phpExcel   ->getActiveSheet()->mergeCells('B'.$i.':D'.$i);
                    $phpExcel   ->getActiveSheet()->mergeCells('E'.$i.':G'.$i);
                    $phpExcel   ->getActiveSheet()->mergeCells('H'.$i.':I'.$i);
                    $phpExcel   ->getActiveSheet()->mergeCells('J'.$i.':K'.$i);
                    $phpExcel   ->getActiveSheet()->mergeCells('L'.$i.':M'.$i);

                    $phpExcel   ->setActiveSheetIndex(0)
                                ->setCellValue('B'.$i, $actividad->profesor->empresa->nombre_empresa)
                                ->setCellValue('E'.$i, $actividad->profesor->empresa->cif)
                                ->setCellValue('H'.$i, $actividad->profesor->empresa->razon_social)
                                ->setCellValue('J'.$i, $actividad->profesor->empresa->direccion);
                    $i++;
                }
            }
        }
        // Añado fila en rosa
        $phpExcel   ->setActiveSheetIndex(0)
                    ->setCellValue('B'.$i, 'CONTRATO DE SERVICIOS CON EL AYUNTAMIENTO');
        $phpExcel   ->getActiveSheet()->getStyle('B'.$i.':M'.$i)->applyFromArray(\App\Funciones\Estilos::ESTILO_PREVENCION_ROSA);
        $phpExcel   ->getActiveSheet()->getStyle('B'.$i.':M'.$i)->applyFromArray(\App\Funciones\Estilos::BORDE_TODO);
        $phpExcel   ->getActiveSheet()->mergeCells('B'.$i.':M'.$i);
        $i++;
        $phpExcel   ->getActiveSheet()->mergeCells('B'.$i.':D'.$i);
        $phpExcel   ->getActiveSheet()->mergeCells('E'.$i.':G'.$i);
        $phpExcel   ->getActiveSheet()->mergeCells('H'.$i.':J'.$i);
        $phpExcel   ->getActiveSheet()->mergeCells('K'.$i.':M'.$i);

        $phpExcel   ->setActiveSheetIndex(0)
                    ->setCellValue('B'.$i,  $titulosColumnasContratos[0])  
                    ->setCellValue('E'.$i,  $titulosColumnasContratos[1])
                    ->setCellValue('H'.$i,  $titulosColumnasContratos[2])
                    ->setCellValue('K'.$i,  $titulosColumnasContratos[3])
        ;
        $phpExcel   ->getActiveSheet()->getStyle('B'.$i.':M'.$i)->applyFromArray(\App\Funciones\Estilos::ESTILO_PREVENCION_TITULOS);
        $phpExcel   ->getActiveSheet()->getStyle('B'.$i.':M'.$i)->applyFromArray(\App\Funciones\Estilos::BORDE_TODO);
        $i++;
        foreach ($actividades as $actividad) {
            $phpExcel   ->getActiveSheet()->mergeCells('B'.$i.':D'.$i);
            $phpExcel   ->getActiveSheet()->mergeCells('E'.$i.':G'.$i);
            $phpExcel   ->getActiveSheet()->mergeCells('H'.$i.':J'.$i);
            $phpExcel   ->getActiveSheet()->mergeCells('K'.$i.':M'.$i);
            
            $lugar = '';
            if (strpos($actividad->aula->nombre, 'Exterior') !== false) {
                $lugar = 'Exterior';
            }
            else{
                if(strpos($actividad->aula->nombre, 'Casa') !== false){
                    $lugar = \App\Funciones\Centros::CASA;
                }
                if(strpos($actividad->aula->nombre, 'Centro') !== false){
                    $lugar = \App\Funciones\Centros::JUVENTUD;
                }
                if(strpos($actividad->aula->nombre, 'Matas') !== false){
                    $lugar = \App\Funciones\Centros::MATAS;
                }
            }
            //dd($lugar);
            $phpExcel   ->getActiveSheet()->getStyle('B'.$i.':M'.$i)->applyFromArray(\App\Funciones\Estilos::BORDE_TODO_FINO);
            $phpExcel   ->setActiveSheetIndex(0)
                        ->setCellValue('B'.$i, $actividad->nombre)
                        ->setCellValue('E'.$i, $actividad->fecha_inicio)
                        ->setCellValue('H'.$i, $actividad->horas . ' h' )
                        ->setCellValue('K'.$i, $lugar);
            $i++;
        }
    
        
        foreach(range('B','M') as $columnID)
        {
            $phpExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }

        // Renombro la hoja de cálculo
        $phpExcel->getActiveSheet()->setTitle('Prevención Riesgos');

        // Activamos la primera hoja como activa, así Excel abre esta como primera
        $phpExcel->setActiveSheetIndex(0);

        // Cabeceras para el archivo tipo excel (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'. session()->get('campaniaActiva')[0] .' - Prevención Riesgos.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0 

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($phpExcel, 'Xlsx');
        $writer = new Xlsx($phpExcel);
        $writer->setPreCalculateFormulas(false);
        $writer->save('php://output');

        exit;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Actividad;
use App\Aula;
class AulasController extends Controller
{
    /**
     * Muestra todas las aulas.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aulas = Aula::orderBy('nombre', 'asc')->get();
        return view('aulas.index', compact('aulas'));
    }

    /**
     * Muestra el formulario para crear una nueva aula.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('aulas.crear');
    }

    /**
     * Guarda una nueva aula.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $aula = new Aula;
        $aula->nombre = $request->nombre;
        $aula->save();
        return redirect('/aulas')->with('mensaje', 'Aula guardada');  
    }

    /**
     * Muestra los datos de un aula.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $aula = Aula::whereId($id)->firstOrFail();
        $campania_id = session()->get('campaniaActiva')[1];
        $actividades = Actividad::where([['aula_id', $aula->id],['campania_id', $campania_id]])->orderBy('fecha_inicio')->orderBy('hora_inicio')->get();

        return view('aulas.show', compact('aula', 'actividades'));
    }

    /**
     * Muestra el formulario para editar un aula.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $aula = Aula::whereId($id)->firstOrFail();
        return view('aulas.edit', compact('aula'));
    }

    /**
     * Actualiza y guarda un aula.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $aula = Aula::whereId($id)->firstOrFail();
        $aula->nombre = $request->get('nombre');
        $aula->save();

        return redirect('/aulas')->with('mensaje', 'Aula '. $aula->nombre .  ' modificada');
    }

    /**
     * Elimina un aula especificada.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $aula = Aula::whereId($request->id)->firstOrFail();
        $aula->delete();

        return redirect('/aulas')->with('mensaje', 'Aula eliminada');
    }

    /**
     * Muestra el aviso para eliminar un aula.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function borrar($id)
    {
        $aula = Aula::whereId($id)->firstOrFail();
        return view('aulas.borrar', compact('aula'));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactoFormRequest;

use App\Contacto;
use App\Empresa;
class ContactosController extends Controller
{
    /**
     * Muestra todos los contactos.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contactos = Contacto::orderBy('nombre_completo', 'asc')->paginate(15);
        return view('contactos.index', compact('contactos')); 
    }

    /**
     * Muestra el formulario para crear un contacto.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empresas = Empresa::orderBy('nombre_empresa', 'asc')->pluck('nombre_empresa', 'id'); 
        return View('contactos.crear', compact('empresas'));
    }

    /**
     * Guarda un nuevo contacto.
     *
     * @param  \App\Http\Requests\ContactoFormRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactoFormRequest $request)
    {
        $contacto = new Contacto($request->all());
        $contacto->save();
        return  redirect('/contactos')->with('mensaje', 'Contacto creado'); 
    }

    /**
     * Muestra un contacto específico.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Muestra el formulario para editar un contacto.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empresas = Empresa::orderBy('nombre_empresa', 'asc')->pluck('nombre_empresa', 'id');
        $contacto = Contacto::whereId($id)->firstOrFail();
        return view('contactos.edit', compact('contacto', 'empresas'));
    }

    /**
     * Actualiza y guarda un contacto.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ContactoFormRequest $request, $id)
    {
        $contacto = Contacto::whereId($id)->firstOrFail();
        $contacto->update($request->all());
        return redirect('/contactos')->with('mensaje', 'Contacto modificado');
    }

    /**
     * Elimina un contacto específico.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contacto = Contacto::whereId($id)->firstOrFail();
        $contacto->delete();

        return redirect('/contactos')->with('mensaje', 'Contacto eliminado');
    }

    /**
     * Muestra el aviso de eliminar un contacto.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function borrar($id)
    {
        $contacto = Contacto::whereId($id)->firstOrFail();
        return view('contactos.borrar', compact('contacto'));
    }
}

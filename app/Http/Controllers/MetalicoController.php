<?php

namespace App\Http\Controllers;

use App\Metalico;
use App\Actividad;
use Illuminate\Http\Request;
use App\Http\Requests\MetalicoFormRequest;

class MetalicoController extends Controller
{
    /**
     * Muestra todos los metálicos.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $campania_id = session()->get('campaniaActiva')[1];
        $actividades_todas = Actividad::where('campania_id',  $campania_id)->orderBy('categoria_id', 'asc')->get();
        $actividades = array();
        foreach ($actividades_todas as $actividad) {
          if( count($actividad->metalicos) > 0 ){
              array_push($actividades, $actividad);
          } 
        }
        return view('metalicos.index', compact('actividades'));
    }

    /**
     * Muestra el formulario para crear metálicos.
     *
     * @return \Illuminate\Http\Response
     * @param int $id Id de la actividad a la cual se agrega el metálico
     */
    public function create($id)
    {
        return view('metalicos.crear', compact('id'));
    }

    /**
     * Guarda un metálico nuevo.
     *
     * @param  \App\Http\Requests\MetalicoFormRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MetalicoFormRequest $request)
    {
        if($request->ajax()){
            $metalico = new Metalico($request->all());
            // La petición la hago por AJAX
            if ($metalico) {
                $metalico->save();
                return response()->json([
                    'status'     => 'success',
                    'id'     => $metalico->id,
                    'fecha'  => $metalico->fecha_necesidad,
                    'concepto'  => $metalico->concepto,
                    'importe'  => $metalico->importe
                    ]);
            } else {
                return response()->json([
                    'status' => 'error',
                    'datos' => $metalico]);
            }
        }
        else{
            return 'Lo siento esto no está permitido así.';
        }
       
    }

    /**
     * Muestra un metálico específico.
     *
     * @param  \App\Metalico  $metalico
     * @return \Illuminate\Http\Response
     */
    public function show($idActividad)
    {
        $metalicos = Metalico::where('actividad_id', $idActividad)->get();
        $actividad = Actividad::whereId($idActividad)->firstOrFail();
        return view('metalicos.show', compact('metalicos', 'actividad'));
    }

    /**
     * Muestra un formulario para editar un metálico.
     *
     * @param  \App\Metalico  $metalico
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $metalico = Metalico::whereId($id)->firstOrFail();
        return view('metalicos.edit', compact('metalico'));
    }

    /**
     * Actualiza y guarda un metálico.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Metalico  $metalico
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Metalico $metalico)
    {
        $metalico_anterior = Metalico::whereId($request->request->get('id'))->firstOrFail();
        $metalico_anterior->update($request->all());
        return redirect('actividades/metalicos')->with('mensaje', 'Metálico modificado');
    }

    /**
     * Elimina un metálico.
     *
     * @param  \App\Metalico  $metalico
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $metalico = Metalico::whereId($id)->firstOrFail();
        $metalico->delete();

        return redirect('/')->with('mensaje', 'Metálico eliminado');
    
    }

    /**
     * Muestra el aviso de eliminación de un metálico
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function borrar($id)
    {
        $metalico = Metalico::whereId($id)->firstOrFail();
        return view('metalicos.borrar', compact('metalico'));
    }
}

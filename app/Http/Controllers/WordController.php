<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Actividad;

use Carbon\Carbon;
class WordController extends Controller
{
    /**
     * Mostramos y creamos todos los words de las actividades de la campaña.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Borramos lo antiguo
        $path = public_path('words');
        $files = scandir($path);
        Array_map('unlink', glob($path.'/*.docx'));
        $count = 0;

        $campania_id = session()->get('campaniaActiva')[1];
  
        $actividades = Actividad::where('campania_id', $campania_id)->get();
        
        // Creamos todos los Words de las actividades de la campaña
        foreach ($actividades as $actividad) 
        {
            $word = $this->crearWord($actividad);
            $count = 1;
        }

        $path = public_path('words');

        $files = scandir($path);

        return view('words.index', compact('path', 'files', 'count') );
    }


    /**
     * Creamos el Word de una actividad y la descargamos.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $actividad = Actividad::findOrFail($id);
        $this->crearWord($actividad);
        $nombre = $actividad->nombre.' - '.$actividad->grupo->nombre;
        $nombre_url = rawurlencode($nombre);
        //dd($nombre);
        // Cabeceras para el Word
        header('Content-Type: application/octet-stream');
        header("Content-Disposition: attachment; filename=\"{$nombre}.docx\"");
        
        echo file_get_contents('words/'.$nombre.'.docx');
    }

    /**
     * Creamos el Word de una actividad.
     *
     * @param  \App\Actividad  $actividad
     */    
    public function crearWord(Actividad $actividad)
    {
        $writers = array('Word2007' => 'docx', 'HTML' => 'html', 'PDF' => 'pdf');
        
        // Plantilla de Word a usar
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('plantilla/plantilla.docx');

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $lugar = '';
        if (strpos($actividad->aula->nombre, 'Exterior') !== false) {
            $lugar = 'En ' . $actividad->lugar_celebracion;
        }
        else{
            if(strpos($actividad->aula->nombre, 'Casa') !== false){
                $lugar = \App\Funciones\Centros::CASA;
            }
            if(strpos($actividad->aula->nombre, 'Centro') !== false){
                $lugar = \App\Funciones\Centros::JUVENTUD;
            }
            if(strpos($actividad->aula->nombre, 'Matas') !== false){
                $lugar = \App\Funciones\Centros::MATAS;
            }
        }

        // Rellenamos la plantilla con los datos
        $templateProcessor->setValue('nombre', $actividad->nombre);
        $templateProcessor->setValue('grupo', $actividad->grupo->nombre);
        $templateProcessor->setValue('categoria', $actividad->categoria->nombre);
        $templateProcessor->setValue('dias',  $actividad->dias);
        $templateProcessor->setValue('parMin',  $actividad->par_min);
        $templateProcessor->setValue('parMax',$actividad->par_max);
        $templateProcessor->setValue('edadMinima', $actividad->edad_minima);
        $templateProcessor->setValue('edadMaxima', $actividad->edad_maxima);
        $templateProcessor->setValue('fechaInscripcion', $actividad->fecha_inscripcion);
        $templateProcessor->setValue('fechaInicio', $actividad->fecha_inicio);
        $templateProcessor->setValue('fechaFin', $actividad->fecha_fin);
        $templateProcessor->setValue('excepto', $actividad->fecha_excluidos);
        $templateProcessor->setValue('horaInicio', Carbon::parse( $actividad->hora_inicio)->format('G:i') . ' h');
        $templateProcessor->setValue('horaFin', Carbon::parse( $actividad->hora_fin)->format('G:i') . ' h');
        $templateProcessor->setValue('precio', $actividad->precio .' €');
        $templateProcessor->setValue('responsables', $actividad->responsable->nombre);
        $templateProcessor->setValue('ubicacion', $lugar);
        $templateProcessor->setValue('incluye', $actividad->incluye);
        $templateProcessor->setValue('textoPrevio', $actividad->texto_previo_folleto);
        $templateProcessor->setValue('observaciones', $actividad->observaciones);
        
        /* Cálculo del tiempo DEPRECATED
        if(intval($actividad->categoria_id) == 1)
            $horas = '';
        else{
            // Compruebo si son del mismo día
            if($actividad->fecha_inicio == $actividad->fecha_fin){
                $horas = Carbon::parse($actividad->hora_fin)->diffInHours(Carbon::parse($actividad->hora_inicio));
            }
            else{
                $dias = Carbon::parse($actividad->fecha_fin)->diffInDays(Carbon::parse($actividad->fecha_inicio));
                //echo 'Dias: ' . $dias . '<br>';
                $horas_dias = Carbon::parse($actividad->hora_fin)->diffInHours(Carbon::parse($actividad->hora_inicio));
                //echo 'Horas Dias: ' . $horas_dias . '<br>';
                //$calculo_horas = ceil($dias/7 * $horas_dias) + 1;
                $calculo_horas = ($dias * $horas_dias)/7 + 1;
                $horas = $calculo_horas;
            }
        }*/
        
        $horas = $actividad->horas;
        
        $templateProcessor->setValue('tiempo', $horas . ' h.');
        $templateProcessor->setValue('coste', $actividad->coste_estimado_maximo.' €');

        if($actividad->profesor != null){
            $templateProcessor->setValue('profesor', $actividad->profesor->nombre_completo);
            $templateProcessor->setValue('profesorTelefono', $actividad->profesor->telefono);
            if($actividad->profesor->empresa != null){
                $templateProcessor->setValue('empresaNombre', $actividad->profesor->empresa->nombre_empresa);
                $templateProcessor->setValue('empresaTelefono', $actividad->profesor->empresa->telefono);
            }
            else{
                $templateProcessor->setValue('empresaNombre', '');
                $templateProcessor->setValue('empresaTelefono', '');
            }
        }
        else{
            $templateProcessor->setValue('profesor', '');
            $templateProcessor->setValue('profesorTelefono', '');
            $templateProcessor->setValue('empresaNombre', '');
            $templateProcessor->setValue('empresaTelefono', '');
        }
        
        $templateProcessor->setValue('aula', $actividad->aula->nombre);

        $templateProcessor->saveAs('words/'.$actividad->nombre.' - '.$actividad->grupo->nombre.'.docx');
    }
}

<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Actividad;
use Illuminate\Http\Request;

class CategoriasController extends Controller
{
    /**
     * Muestra todas las categorías.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = Categoria::all();
        return view('categorias.index', compact('categorias'));
    }

    /**
     * Muestra el formulario para crear una categoría.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categorias.crear');
    }

    /**
     * Guarda una categoría.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $categoria = new Categoria;
        $categoria->nombre = $request->nombre;
        $categoria->save();
        return redirect('/categorias')->with('mensaje', 'Categoria guardada'); 
    }


    /**
     * Muestra el formulario para editar una categoría.
     *
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoria = Categoria::whereId($id)->firstOrFail();
        return view('categorias.edit', compact('categoria'));
    }

    /**
     * Actualiza y guarda una categoría.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categoria = Categoria::whereId($id)->firstOrFail();
        $categoria->nombre = $request->get('nombre');
        $categoria->save();

        return redirect('/categorias')->with('mensaje', 'Categoria modificada');
    }

    /**
     * Elimina una categoría.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categoria = Categoria::whereId($id)->firstOrFail();
        $categoria->delete();

        return redirect('/categorias')->with('mensaje', 'Categoria eliminada');
    }

    /**
     * Muestra el aviso para eliminar una categoría.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function borrar($id)
    {
        $actividades = Actividad::where('categoria_id', $id)->get();
        $categoria = Categoria::whereId($id)->firstOrFail();
        return view('categorias.borrar', compact('categoria', 'actividades'));
    }
}

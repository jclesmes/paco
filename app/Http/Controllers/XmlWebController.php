<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Actividad;
use Iluminate\Http\Response;

use Carbon\Carbon;

// definiciones aire libre y familia
define ("AIRE_CLASS", "aire");
define ("AIRE_CLASS_FONDO", "fondoaire");
define ("AIRE_CATID", "participa/actividades-trimestrales-2/aire-libre-y-familia");
// definiciones música y artes escénicas
define ("MUSICA_CLASS", "musica");
define ("MUSICA_CLASS_FONDO", "fondomusica");
define ("MUSICA_CATID", "participa/actividades-trimestrales-2/musica-y-artes-escenicas");
// definiciones imágen y nuevas tecnologías
define ("IMAGEN_CLASS", "imagen");
define ("IMAGEN_CLASS_FONDO", "fondoimagen");
define ("IMAGEN_CATID", "participa/actividades-trimestrales-2/imagen-y-nuevas-tecnologias");
// definiciones para habilidades
define ("HABILIDADES_CLASS", "habilidades");
define ("HABILIDADES_CLASS_FONDO", "fondohabilidades");
define ("HABILIDADES_CATID", "participa/actividades-trimestrales-2/habilidades-domesticas");
// definiciones formación y empleo
define ("FORMACION_CLASS", "formacion");
define ("FORMACION_CLASS_FONDO", "fondoformacion");
define ("FORMACION_CATID", "participa/actividades-trimestrales-2/formacion-y-empleo");
// definiciones más ocio
define ("OCIO_CLASS", "ocio");
define ("OCIO_CLASS_FONDO", "fondoocio");
define ("OCIO_CATID", "participa/actividades-trimestrales-2/mas-ocio");
// definiciones certámenes y exposiciones
// definiciones servicios

class XmlWebController extends Controller
{
    /**
     * Mostramos el formulario para a partír del cual se genera el xml para importar las 
     * actividades en Joomla a través de J2XML.
     *
     * @return \Illuminate\Http\Response
     */
    public function xml(){
        return view('xml');
    }

    /**
     * Creamos el Word de una actividad y la descargamos.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response Descar del archivo XML para Joomla
     */
    public function crearXml(Request $request){
        $id =$request->id;
        $campania_id = session()->get('campaniaActiva')[1];
        $actividades = Actividad::orderBy('categoria_id', 'asc')->where('campania_id', $campania_id)->get();
        //echo $actividad->fecha_inicio;
        $xml = '<?xml version="1.0" encoding="UTF-8"?>
<j2xml version="15.9.0">';
        //dd($xml);
        foreach ($actividades as $actividad) {
            $id++;
            $xml = $xml. $this->articulo($actividad, $id); 
        }
        $xml = $xml.'</j2xml>';

        return response($xml)
        ->withHeaders([
            'Content-Type' => 'text/xml',
            'Content-Disposition'=> 'attachment; filename=Subida-web.xml'
        ]); 
    }

    /**
     * Creamos un artículo completo a partir de una actividad.
     *
     * @param  \App\Actividad  $actividad
     * @param  int  $id
     * @return String $data Datos de la actividad en el formato necesario para el XML
     */
    public function articulo(Actividad $actividad, $id){
        $AIRE_URL = $this->url($id, 40);
        $MUSICA_URL = $this->url($id, 41);
        $IMAGEN_URL = $this->url($id, 44);
        $HABILIDADES_URL = $this->url($id, 42);
        $FORMACION_URL = $this->url($id, 43);
        $OCIO_URL = $this->url($id, 45);
        switch ($actividad->categoria_id) {
            case 1:
                $url = $AIRE_URL;
                $class_fondo = AIRE_CLASS_FONDO;
                $class = AIRE_CLASS;
                $cat_id = AIRE_CATID;
                break;
            case 2:
                $url = $MUSICA_URL;
                $class_fondo = MUSICA_CLASS_FONDO;
                $class = MUSICA_CLASS;
                $cat_id = MUSICA_CATID;
                break;
            case 3:
                $url = $IMAGEN_URL;
                $class_fondo = IMAGEN_CLASS_FONDO;
                $class = IMAGEN_CLASS;
                $cat_id = IMAGEN_CATID;
                break;
            case 4:
                $url = $HABILIDADES_URL;
                $class_fondo = HABILIDADES_CLASS_FONDO;
                $class = HABILIDADES_CLASS;
                $cat_id = HABILIDADES_CATID;
                break;
            case 5:
                $url = $FORMACION_URL;
                $class_fondo = FORMACION_CLASS_FONDO;
                $class = FORMACION_CLASS;
                $cat_id = FORMACION_CATID;
                break;
            case 6:
                $url = $OCIO_URL;
                $class_fondo = OCIO_CLASS_FONDO;
                $class = OCIO_CLASS;
                $cat_id = OCIO_CATID;
                break;
            default:
                $url = $AIRE_URL;
                $class_fondo = AIRE_CLASS_FONDO;
                $class = AIRE_CLASS;
                $cat_id = AIRE_CATID;
                break;
        }
        $edad = $this->edad($actividad->edad_maxima, $actividad->edad_minima);
        $fecha = $this->fecha($actividad);
        $lugar = $this->lugar($actividad);
        // Añadido htmlentities 
        $introtext ='<div class="g-grid"><a href="'.$url.'" class="transparencia"> <img src="images/participa/" alt="'.$actividad->nombre.'" width="100%" title="'.$actividad->nombre.'" border="0" /> </a></div>
                    <div class="g-grid '.$class_fondo.'">
                        <h2><span class="'.$class.'"><i class="punto fa fa-stop fa-1x"></i> '.$edad.'</span></h2>
                        <span class="subtitulointro">'.$actividad->texto_previo_folleto.'</span>
                    </div>';
        // Convertimos
        $introtext = htmlentities($introtext);
        // Igual con fulltext
        if($actividad->categoria_id == 1){
            $incluye = '<p><i class="punto fa fa-circle">&nbsp;</i> ' .htmlentities($actividad->incluye).'</p>';
            $horas = '';
            $horario = '';
        }
        else{
            $incluye = '';
            $horas = '<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.$this->horas($actividad).'</p>';
            $horario = '<p><i class="punto fa fa-circle" aria-hidden="true"></i> '.$actividad->horas.' horas.</p>';
        }
        $fulltext ='<div class="g-grid">
                        <div class="g-block size-40">
                            <img src="images/participa/" alt="'.htmlentities($actividad->nombre).'" title="'.htmlentities($actividad->nombre).'" border="0" />
                        </div>
                        <div class="g-block size-5">&nbsp;</div>
                        <div class="g-block">
                            <h2><span class="'.$class.'"><i class="punto fa fa-stop fa-1x"></i> '.$edad.'</span></h2>
                            <span class="subtitulo">'.htmlentities($actividad->texto_previo_folleto).'</span></div>
                        </div>
                        <p>&nbsp;</p>
                        <div class="g-grid">
                            <div class="g-block folleto '.$class_fondo.' size-70">
                            '.$fecha.'
                            '.$horas.'
                            '.$horario.'
                            <p><i class="punto fa fa-circle"></i> '.htmlentities($actividad->precio).' &euro;</p>
                            <p class="'.$class.'">'.$lugar.'.</p>
                            '.$incluye.'
                        </div>
                        <div class="g-block size-5">&nbsp;</div>
                        <div class="g-block dossier">
                            <div class="dossiercontenido"><a href="images/participa/.pdf" target="_blank" rel="noopener noreferrer" title="descargar dossier"> <i class="fa fa-file-pdf-o fa-4x"></i><br /><b>Desc&aacute;rgate m&aacute;s info</b> </a></div>
                        </div>
                    </div>';
        $fulltext = htmlentities($fulltext);
        $data = '
        <content>
            <id>'.$id.'</id>
            <title><![CDATA[ '.$actividad->nombre.']]></title>
            <alias><![CDATA[ '.str_replace(" ", "-", $actividad->nombre).']]></alias>
            <introtext><![CDATA['.$introtext.']]></introtext>
            <fulltext><![CDATA['.$fulltext.']]></fulltext>
        <state>0</state>
        <created><![CDATA[]]></created>
        <created_by_alias/>
        <modified><![CDATA[]]></modified>
        <publish_up><![CDATA[]]></publish_up>
        <publish_down><![CDATA['.$actividad->fecha_inicio.']]></publish_down>
        <images><![CDATA[{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}]]></images>
        <urls><![CDATA[{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}]]></urls>
        <attribs><![CDATA[{"article_layout":"","show_title":"","link_titles":"","show_tags":"","show_intro":0,"info_block_position":"","info_block_show_title":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_associations":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_page_title":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}]]></attribs>
        <version>1</version>
        <ordering>1</ordering>
        <metakey><![CDATA[Rozas Joven, Las Rozas, juventud, actividades, taller, '.$actividad->nombre.']]></metakey>
        <metadesc><![CDATA['.$actividad->texto_previo_folleto.']]></metadesc>
        <hits>0</hits>
        <metadata><![CDATA[{"robots":"","author":"","rights":"","xreference":""}]]></metadata>
        <language><![CDATA[*]]></language>
        <xreference/>
        <created_by><![CDATA[francisco]]></created_by>
        <modified_by><![CDATA[francisco]]></modified_by>
        <catid><![CDATA['.$cat_id.']]></catid>
        <access>1</access>
        <featured>0</featured>
        <rating_sum>0</rating_sum>
        <rating_count>0</rating_count>
      </content>
     ';
      return $data;
    }

    /**
     * Función para poner el lugar.
     *
     * @param  \App\Actividad  $actividad
     * @return String $lugar;
     */
    public function lugar($actividad){
        if (strpos($actividad->aula->nombre, 'Exterior') !== false) {
            $lugar = 'En ' . $actividad->lugar_celebracion;
        }
        else{
            if(strpos($actividad->aula->nombre, 'Casa') !== false){
                $lugar = \App\Funciones\Centros::CASA_URL;
            }
            if(strpos($actividad->aula->nombre, 'Centro') !== false){
                $lugar = \App\Funciones\Centros::JUVENTUD_URL;
            }
            if(strpos($actividad->aula->nombre, 'Matas') !== false){
                $lugar = \App\Funciones\Centros::MATAS_URL;
            }
        }
        return $lugar;
    }

    /**
     * Función para poner la edad.
     *
     * @param int  $edad_maxima
     * @param int $edad_minima
     * @return String $edad;
     */
    public function edad($edad_maxima, $edad_minima){
        if( $edad_maxima < 1 )
            $edad = 'A partir de ' . $edad_minima . ' años.';
        else
            $edad = 'De ' .  $edad_minima . ' a ' .  $edad_maxima . ' años.';
        return $edad;
    }

    /**
     * Función calcular las fechas.
     *
     * @param  \App\Actividad  $actividad
     * @return String $inicio_fin;
     */
    public function fecha(Actividad $actividad){
        Carbon::setLocale('es');
        setlocale(LC_TIME, 'es_ES.UTF-8', 'spanish');
        $inicio = $actividad->fecha_inicio;
        $fin = $actividad->fecha_fin;
        $inicio_fin = 'nada';
        if(strcmp($inicio, $fin) == 0){
            setlocale(LC_TIME, 'es_ES.UTF-8', 'spanish');
            $inicio_fin = '<p><i class="fa fa-calendar-o"></i> '.htmlentities( ucfirst( strftime( '%A %e de %B', strtotime($actividad->fecha_inicio) ) ) ).'.</p>';
        }
        else{
            setlocale(LC_TIME, 'es_ES.UTF-8', 'spanish');
            if( strftime( '%B', strtotime($actividad->fecha_inicio) ) == strftime('%B', strtotime($actividad->fecha_fin))){
                $inicio_fin = '<p><i class="fa fa-calendar-o"></i> '.htmlentities( strftime( '%A del %e', strtotime($actividad->fecha_inicio) ).' al '.strftime('%e de %B', strtotime($actividad->fecha_fin)) ).'.</p>'; 
            }
            else
                $inicio_fin = '<p><i class="fa fa-calendar-o"></i> '.htmlentities(ucfirst(Carbon::parse($actividad->fecha_inicio)->formatLocalized('%A') ) . strftime( ' del %e de %B', strtotime($actividad->fecha_inicio) ) ).' al '.htmlentities( strftime('%e de %B', strtotime($actividad->fecha_fin) ) ).'.</p>';  
        }
        //dd( htmlentities($inicio_fin, ENT_QUOTES, "UTF-8") ); //htmlentities($inicio_fin, ENT_QUOTES, "UTF-8");
        $inicio_fin = str_replace('á', '&aacute;', $inicio_fin); //str_replace ( $valor_a_buscar , $valor_de_reemplazo , $string , [$contador ] )
        $inicio_fin = str_replace('é', '&eacute;', $inicio_fin);
        return $inicio_fin;
    }

    /**
     * Función para calcular las horas de la actividad.
     *
     * @param  \App\Actividad  $actividad
     * @return String $horas;
     */
    public function horas(Actividad $actividad){
        Carbon::setLocale('es');
        setlocale(LC_TIME, 'es_ES.UTF-8', 'spanish');
        $horas = 'De '.Carbon::parse( $actividad->hora_inicio)->format('G:i').' a '. Carbon::parse( $actividad->hora_fin)->format('G:i') . ' h.';
        return $horas;
    }

    /**
     * Función para sacr la URL de Joomla.
     *
     * @param int $id
     * @param int $cat_id
     * @return String;
     */
    public function url($id, $cat_id){
        return "index.php?option=com_content&view=article&id=$id&catid=$cat_id";
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use App\Categoria;
use App\Actividad;
use App\Profesor;

use Carbon\Carbon;
class ExcelResponsableFolletoController extends Controller
{
    public function responsables()
    {
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        date_default_timezone_set('Europe/Madrid');
        $phpExcel = new  Spreadsheet();
        // Encabezados
        $titulosColumnas = array('TALLER','TÉCNICO DEL TALLER','PROFESOR','TEL. PROFESOR','CORREO PROFESOR',
                            'EMPRESA','CONTACTO EMPRESA','TELEFONO EMPRESA');
                            // Definimos estilos
                

        // Set document properties
        $phpExcel->getProperties()->setCreator("Francisco Piedras Pérez")
                                    ->setLastModifiedBy("Francisco Piedras Pérez")
                                    ->setTitle("Prevención riesgos")
                                    ->setSubject("Rozas Joven")
                                    ->setDescription("Responsables para la Concejalía de Juventud en Las Rozas.")
                                    ->setKeywords("Office 2017 prevención responsables actividades openxml php")
                                    ->setCategory("Responsables");
        $i = 2; //Comienzo de las filas quitando los títulos
        $i_inicio =4;

        $categorias = Categoria::All();
        $phpExcel   ->setActiveSheetIndex(0)
                    ->setCellValue('B2',  session('campaniaActiva')[0]);
        $phpExcel   ->getActiveSheet()->getStyle('B2:U2')->applyFromArray(\App\Funciones\Estilos::ESTILO_CAMPANIA);

        foreach ($categorias as $categoria)
        {
            $campania_id = session()->get('campaniaActiva')[1];
            $actividades = Actividad::where([
                ['categoria_id', $categoria->id], 
                ['campania_id', $campania_id] ])->get();
                
            if(count($actividades)>0)
            {
                // Añadimos el nombre de la categoria
                $phpExcel   ->setActiveSheetIndex(0)
                            ->setCellValue('B'.$i,  $categoria->nombre);
                $phpExcel   ->getActiveSheet()->getStyle('B'.$i.':I'.$i)->applyFromArray(\App\Funciones\Estilos::ESTILO_CATEGORIAS[$categoria->id]);
                $i++;
                $phpExcel->getActiveSheet()->getStyle('B'.$i.':I'.$i)->applyFromArray(\App\Funciones\Estilos::ESTILO_TITULO_COLUMNAS);
                // CABECERA DE CADA CATEGORIA
                $phpExcel   ->setActiveSheetIndex(0)
                            ->setCellValue('B'.$i,  $titulosColumnas[0])  //Titulo de las columnas
                            ->setCellValue('C'.$i,  $titulosColumnas[1])
                            ->setCellValue('D'.$i,  $titulosColumnas[2])
                            ->setCellValue('E'.$i,  $titulosColumnas[3])
                            ->setCellValue('F'.$i,  $titulosColumnas[4])
                            ->setCellValue('G'.$i,  $titulosColumnas[5])
                            ->setCellValue('H'.$i,  $titulosColumnas[6])
                            ->setCellValue('I'.$i,  $titulosColumnas[7]);
                $i++;
        
                foreach ($actividades as $actividad) {
                    //Compruebo si hay profesor
                    $empresa_nombre = ' ';
                    $contacto_nombre = ' ';
                    $contacto_telefono = null;
                    $profesor_nombre_completo = ' ';
                    $profesor_telefono = null;
                    $profesor_correo = ' ';

                    if( $actividad->profesor != null ){
                        $profesor_nombre_completo =  $actividad->profesor->nombre_completo;
                        $profesor_telefono =  $actividad->profesor->telefono;
                        $profesor_correo = $actividad->profesor->correo;
                        if($actividad->profesor->empresa != null){
                            $empresa_nombre = $actividad->profesor->empresa->nombre_empresa;
                    }
                        if(count($actividad->profesor->empresa->contactos) > 0)
                        {
                            $contacto_nombre = $actividad->profesor->empresa->contactos[0]->nombre_completo;
                            $contacto_telefono = $actividad->profesor->empresa->contactos[0]->telefono;
                        }
                    }

                $phpExcel   ->setActiveSheetIndex(0)
                            ->setCellValue('B'.$i, $actividad->nombre)
                            ->setCellValue('C'.$i, $actividad->responsable->nombre)
                            ->setCellValue('D'.$i, $profesor_nombre_completo)
                            ->setCellValue('E'.$i, $profesor_telefono)
                            ->setCellValue('F'.$i, $profesor_correo)
                            ->setCellValue('G'.$i, $empresa_nombre)
                            ->setCellValue('H'.$i, $contacto_nombre)
                            ->setCellValue('I'.$i, $contacto_telefono)
                            ;
                $i++;
                }
            $i++;
            }
        }

        foreach(range('B','I') as $columnID)
        {
            $phpExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }

        // Renombro la hoja de cálculo
        $phpExcel->getActiveSheet()->setTitle('General');

        // Activamos la primera hoja como activa, así Excel abre esta como primera
        $phpExcel->setActiveSheetIndex(0);

        // Cabeceras para el archivo tipo excel (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.session()->get('campaniaActiva')[0].' - Responsables.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0 

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($phpExcel, 'Xlsx');
        $writer = new Xlsx($phpExcel);
        $writer->setPreCalculateFormulas(false);
        $writer->save('php://output');

        exit;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Actividad;
use App\Evento;
use App\Aula;

use Carbon\Carbon;
class CalendarioController extends Controller
{
    /**
     * Muestra la vista del calendario.
     *
     * @return \Illuminate\Http\Response Por AJAX se obtendrán los datos
     */
    public function calendario($id = -1)
    {
        return view('calendario.index', compact('id', $id));
    }

    /**
     * Muestra la vista del calendario.
     *
     * @return \Illuminate\Http\Response Por AJAX se obtendrán los datos
     */
    public function calendarioAula($id = -1)
    {
        return view('calendario.aula', compact('id', $id));
    }

    /**
     * API para obtener todas las actividades de la campaña en curso, será devuelto en formato JSON.
     *
     * @return JSON
     */
    public function api($id = -1)
    {
        $datos = array(); //declaramos un array principal que va contener los datos
        $campania_id = session()->get('campaniaActiva')[1]; // seleccionamos la campaña activa
   
        if ($id == -1)
            $actividades_bruto = Actividad::where('campania_id',  $campania_id)->orderBy('fecha_inicio', 'asc')->get(); //take(5)->
        else
            {
                $actividades_bruto  =  Actividad::where([['campania_id',  $campania_id],
                                                        ['aula_id', $id]])->get();
            }
        $datos = $this->procesarActividades($actividades_bruto);

        return response()->json($datos);
    }

    /**
     * API para obtener todas las actividades de la campaña en curso, filtrado por centros o exterior
     * será devuelto en formato JSON.
     *
     * @return JSON
     */
    public function apiSitio($id)
    {
        $datos = array(); //declaramos un array principal que va contener los datos
        $campania_id = session()->get('campaniaActiva')[1]; // seleccionamos la campaña activa

        $centro = Aula::where('nombre', 'LIKE', '%centro%')->pluck('id');
        $casa = Aula::where('nombre', 'LIKE', '%casa%')->pluck('id');
        $matas = Aula::where('nombre', 'LIKE', '%matas%')->pluck('id');
        $exterior = Aula::where('nombre', 'LIKE', '%exterior%')->pluck('id');


        if ($id == 'centro')
            $actividades_bruto = Actividad::where('campania_id',  $campania_id)->whereIn('aula_id', $centro)->get();
        elseif ($id == 'casa')
            $actividades_bruto = Actividad::where('campania_id',  $campania_id)->whereIn('aula_id', $casa)->get();
        elseif ($id == 'matas')
            $actividades_bruto = Actividad::where('campania_id',  $campania_id)->whereIn('aula_id', $matas)->get();
        elseif ($id == 'exterior')
            $actividades_bruto = Actividad::where('campania_id',  $campania_id)->whereIn('aula_id', $exterior)->get();
        else
            $actividades_bruto  =  Actividad::where('campania_id',  $campania_id)->get();

        $datos = $this->procesarActividades($actividades_bruto);

        return response()->json($datos);
    }
    /**
     * Procesa las actividades para convertirlas en formato Fullcalendar.
     *
     * @param  array App\Actividad  $actividades_bruto Array de actividades en bruto
     * @return @var array $datos Array con los datos procesados
     */
    function procesarActividades($actividades_bruto){
        $actividades = array();
        // Debemos mirar las actividades en rango si son de un único dia, las agregamos tal cual
        // si son de un rango miramos, la variable dias y el excluye

        foreach ($actividades_bruto as $actividad_procesada) {
            $actividad_temp = new Evento;
            
            $inicio_anio =  Carbon::parse($actividad_procesada->fecha_inicio)->format('Y');
            $inicio_mes =   Carbon::parse($actividad_procesada->fecha_inicio)->format('m');
            $inicio_dia =   Carbon::parse($actividad_procesada->fecha_inicio)->format('d');

            $inicio_hora =  Carbon::parse( $actividad_procesada->hora_inicio)->format('G');
            $inicio_minuto = Carbon::parse( $actividad_procesada->hora_inicio)->format('i');
            $inicio_segundo = 0;

            $fin_anio =  Carbon::parse($actividad_procesada->fecha_fin)->format('Y');
            $fin_mes =   Carbon::parse($actividad_procesada->fecha_fin)->format('m');
            $fin_dia =   Carbon::parse($actividad_procesada->fecha_fin)->format('d');

            $fin_hora =  Carbon::parse( $actividad_procesada->hora_fin)->format('G');
            $fin_minuto = Carbon::parse( $actividad_procesada->hora_fin)->format('i');
            $fin_segundo = 0;

            $inicio = Carbon::create($inicio_anio, $inicio_mes, $inicio_dia, $inicio_hora, $inicio_minuto, $inicio_segundo);
            $fin = Carbon::create($fin_anio, $fin_mes, $fin_dia, $fin_hora, $fin_minuto, $fin_segundo);
            
            $actividad_temp->id = $actividad_procesada->id;
            $actividad_temp->nombre = $actividad_procesada->nombre;
            $actividad_temp->fecha_inicio = $inicio->toW3cString();
            $actividad_temp->fecha_fin = $fin->toW3cString();

            if($actividad_procesada->fecha_inicio == $actividad_procesada->fecha_fin){
                // La agregamos sin más
                $actividad_temp->color = $this->color($actividad_procesada->categoria_id);
                array_push($actividades, $actividad_temp);  
            }
            else{
                if($actividad_procesada->fecha_excluidos !=  null)
                    $excluidos = explode(",",$actividad_procesada->fecha_excluidos);
                else
                    $excluidos = null;
                
                $dias = explode(",",$actividad_procesada->dias);
                
                // Tenemos que calcular los días que tenemos en ese rango
                if(count($dias) == 1){
                    // En este caso sumamos 1 semana hasta el tope
                    $fin2 = Carbon::create($inicio_anio, $inicio_mes, $inicio_dia, $fin_hora, $fin_minuto, $fin_segundo);
                    do{          
                        $actividad_temp = new Evento;
                        $actividad_temp->id = $actividad_procesada->id;
                        $actividad_temp->nombre = $actividad_procesada->nombre;
                        $actividad_temp->fecha_inicio = $inicio->toW3cString();
                        $actividad_temp->fecha_fin =  $fin2->toW3cString();
                        $actividad_temp->color = $this->color($actividad_procesada->categoria_id);

                        if ($excluidos != null){
                            if (!in_array($inicio->format('Y-m-d'), $excluidos))
                                array_push($actividades, $actividad_temp);
                        }
                        else
                        {
                            array_push($actividades, $actividad_temp);
                        }
                            
                        $inicio = $inicio->copy()->addWeek();
                        $fin2 = $fin2->copy()->addWeek();
                        //dd($inicio->diffInDays($fin), $fin->diffInDays($inicio, false) );
                    }while( $inicio->diffInDays($fin, false) >= 0 );

                }

                else{
                    $n =count($dias);
                    for ($i=0; $i < $n; $i++) { 
                        if($dias[$i] == 'l'){
                            $dias[$i] = 0;
                            continue;
                        }
                        if($dias[$i] == 'm'){
                            $dias[$i] = 1;
                            continue;
                        }
                            
                        if($dias[$i] == 'x'){
                            $dias[$i] = 2;
                            continue;
                        }
                            
                        if($dias[$i] == 'j'){
                            $dias[$i] = 3;
                            continue;
                        }
                            
                        if($dias[$i] == 'v'){
                            $dias[$i] = 4;
                            continue;
                        }
                            
                        if($dias[$i] == 's'){
                            $dias[$i] = 5;
                            continue;
                        }
                            
                        if($dias[$i] == 'd'){
                            $dias[$i] = 6;
                            continue;
                        }
                            
                    }
                    //dd($n,$dias);
                    $i = 0;
                    $dia_inicio = $dias[0];
                    //dd($dias);
                     $fin2 = Carbon::create($inicio_anio, $inicio_mes, $inicio_dia, $fin_hora, $fin_minuto, $fin_segundo);
                    foreach ($dias as $dia) {
                        do{          
                            $actividad_temp = new Evento;
                            $actividad_temp->id = $actividad_procesada->id;
                            $actividad_temp->nombre = $actividad_procesada->nombre;
                            $actividad_temp->fecha_inicio = $inicio->toW3cString();
                            $actividad_temp->fecha_fin =  $fin2->toW3cString();
                            $actividad_temp->color = $this->color($actividad_procesada->categoria_id);
    
                            if ($excluidos != null){
                                if (!in_array($inicio->format('Y-m-d'), $excluidos))
                                    array_push($actividades, $actividad_temp);
                            }
                            else
                            {
                                array_push($actividades, $actividad_temp);
                            }
                                
                            $inicio = $inicio->copy()->addWeek();
                            $fin2 = $fin2->copy()->addWeek();
                            //dd($inicio->diffInDays($fin), $fin->diffInDays($inicio, false) );
                        }while( $inicio->diffInDays($fin, false) >= 0 );

                        
                        if($i < $n-1)
                            $prox_dia = $dias[$i+1] - $dia_inicio;
                        //dd($prox_dia);    
                        $primera = Carbon::create($inicio_anio, $inicio_mes, $inicio_dia, $inicio_hora, $inicio_minuto, $inicio_segundo);
                        $inicio = $primera->copy()->addDays($prox_dia);
                        $fin2 = Carbon::create($inicio_anio, $inicio_mes, $inicio_dia, $fin_hora, $fin_minuto, $fin_segundo)->addDays($prox_dia);
                        //dd($inicio);
                        $i++;
                    }

                }
                
            }

            //dd($actividades);
        }


        $i=0;
        $datos = array();
        //dd($actividades);
        foreach ($actividades as $actividad) {
            $datos[$i] = array(
                'title' => $actividad->nombre,
                'start' => $actividad->fecha_inicio,
                'end' => $actividad->fecha_fin,
                'url' => url('actividades', $actividad->id),
                'color'=> $actividad->color,    // an option!
                'textColor'=> 'white'  // an option!
            );
            $i++;
        }

        return $datos;
    }

    /**
     * Función que generea los colores web de cada categoría para el calendario.
     *
     * @param  int  $categoria_id Le pasamos el id de la categoría de la actividad
     * @return int $color
     */
    function color(int $categoria_id){
        switch ($categoria_id) {
            case 1:
                $color = '#2ca647';
                break;
            case 2:
                $color = '#0099d8';
                break;
            case 3:
                $color = '#db3128';
                break;
            case 4:
                $color = '#dab61e';
                break;
            case 5:
                $color = '#0c4370';
                break;
            case 6:
                $color = '#ec6d23';
                break;
            case 7:
                $color = '#a7a523';
                break;            
            default:
                $color = '#8a3f8e';
                break;
        }
        return $color;
    }

}

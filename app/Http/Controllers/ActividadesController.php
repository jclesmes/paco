<?php

namespace App\Http\Controllers;

use App\Campania;
use App\Actividad;
use App\Grupo;
use App\Aula;
use App\Responsable;
use App\Categoria;
use App\Profesor;
use App\Contacto;
use App\Metalico;
use Illuminate\Http\Request;
use App\Http\Requests\ActividadFormRequest;

use Session;
class ActividadesController extends Controller
{
    /**
     * Muestra todas las actividades de la campaña actual.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if(session()->has('categoria') ){
            session()->forget('categoria');
            session()->pull('categoria', 0);
        }
        else
            session()->push('categoria', 0);
        
        $campania_id = session()->get('campaniaActiva')[1];
        $actividades = Actividad::where('campania_id',  $campania_id)->orderBy('categoria_id', 'asc')->orderBy('nombre', 'asc')->get();
        $categorias = Categoria::pluck('nombre', 'id');

        return view('index', compact('actividades', 'categorias') );
    }

    /**
     * Muestra todas las actividades de la campaña actual
     * filtradas por categoría.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function index2(Request $request)
    {
        $id = $request->id;
        if ($request->session()->exists('categoria')) {
            $request->session()->forget('categoria');
            session()->push('categoria', $id);
        }
        else {
            session()->push('categoria', $id);
        }   
        if( $id == 0){
            $campania_id = session()->get('campaniaActiva')[1];
            $actividades = Actividad::where('campania_id',  $campania_id)->orderBy('categoria_id', 'asc')->orderBy('nombre', 'asc')->get();
        }
        else{
            $campania_id = session()->get('campaniaActiva')[1];
            $actividades = Actividad::where([
                                                ['categoria_id', $id], 
                                                ['campania_id', $campania_id]
                                    ])->get();
        }
        $categorias = Categoria::pluck('nombre', 'id');
        return view('index', compact('actividades', 'categorias') );
    }

    /**
     * Muestra el formulario para crear una nueva actividad.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
        $grupos = Grupo::orderBy('nombre','asc')->pluck('nombre', 'id');
        $aulas = Aula::orderBy('nombre', 'asc')->pluck('nombre', 'id');
        $responsables = Responsable::orderBy('nombre', 'asc')->pluck('nombre', 'id');
        $categorias = Categoria::pluck('nombre', 'id');
        $profesores = Profesor::orderBy('nombre_completo', 'asc')->pluck('nombre_completo', 'id');
        return view('actividades.crear', compact('responsables', 'profesores','grupos', 'aulas', 'categorias') );
    }

    /**
     * Guarda una nueva actividad.
     *
     * @param  \App\Http\Requests\ActividadFormRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ActividadFormRequest $request)
    {
        $actividad = new Actividad( $request->all());
        $actividad->save();
        return redirect('actividades/metalicos/crear/'.$actividad->id)->with('mensaje', 'Actividad '.$actividad->nombre.' guardada');
        /*        
         // Si la actividad es exterior la guardamos sin más
        if (strpos($actividad->aula->nombre, 'Exterior')) {
            $actividad->save();
            echo 'Actividad de aire libre';
        }
        // Ahora comprobamos el resto
        else{
            $campania_id = session()->get('campaniaActiva')[1];
            $actividades = Actividad::where([
                ['campania_id', $campania_id], 
                ['fecha_inicio', $actividad->fecha_inicio],
                ['aula_id', $actividad->aula_id]
                ])->get();
            // Si no hay ninguna más en esa fecha y aula guardamos
            if(count($actividades)==0){
                echo 'No hay actividades concurrentes';
                $actividad->save();
                return redirect('actividades/metalicos/crear/'.$actividad->id)->with('mensaje', 'Actividad '.$actividad->nombre.' guardada');  
            }
            // De lo contrario comprobamos todas las actividades de esa fecha y aula
            else{
                foreach ($actividades as $una) {
                    // Chapuza para poner la hora en el mismo formato
                    $una_inicio = substr($una->hora_inicio, 0, 5);
                    $una_fin = substr($una->hora_fin, 0, 5);
                    $actividad_inicio = '0'. $actividad->hora_inicio;
                    $actividad_fin = '0'. $actividad->hora_fin;

                    if(strlen($actividad_inicio) == 6)
                        $actividad_inicio = substr($actividad_inicio, 1);
                    if(strlen($actividad_fin) == 6)
                        $actividad_fin = substr($actividad_fin, 1);
                    // Fin chapuza
                    if( ( $una_inicio < $actividad_inicio ) && ( $actividad_inicio < $una_fin ) ){
                        $texto ='Hora de inicio ' . $actividad_inicio . ' está entre: ' . $una_inicio . '-' . $una_fin .' de la actividad '. $una->nombre;
                        Session::flash('error', $texto); 
                        echo 'Comprobación 1: ' .($una_inicio < $actividad_inicio) . '<br>';
                        echo 'Comprobación 2: ' . ($actividad_inicio < $una_fin). '<br>';                 
                        return back();   
                    }
                    if( ( $una_inicio < $actividad_fin ) && ( $actividad_fin < $una_fin )){
                        $texto = 'Hora de fin ' . $actividad_fin . ' está entre: ' . $una_inicio . '-' . $una_fin .' de la actividad '. $una->nombre; 
                        Session::flash('error', $texto);  
                        echo 'Comprobación 3: ' . ($una_inicio < $actividad_fin) . '<br>'; 
                        echo 'Comprobación 4: ' . ($actividad_fin < $una_fin) . '<br>'; 
                        return back();
                    }
                    if( ( $una_inicio == $actividad_inicio ) && ( $actividad_fin == $una_fin )){
                        $texto = 'Hora de inicio y fin igual que la actividad ' . $una->nombre; 
                        Session::flash('error', $texto);  
                        echo 'Comprobación 5: ' . ($una_inicio < $actividad_fin) . '<br>'; 
                        echo 'Comprobación 6: ' . ($actividad_fin < $una_fin) . '<br>'; 
                        return back();
                    }
                    if( ( $actividad_inicio < $una_inicio ) && ( $una_fin < $actividad_fin )){
                        $texto = '¿Quiere engullir a la actividad ' . $una->nombre . '? No te dejo abusón!!!! '; 
                        Session::flash('error', $texto);  
                        echo 'Comprobación 5: ' . ($una_inicio < $actividad_fin) . '<br>'; 
                        echo 'Comprobación 6: ' . ($actividad_fin < $una_fin) . '<br>'; 
                        return back();
                    }
                }
                $actividad->save();
                return redirect('actividades/metalicos/crear/'.$actividad->id)->with('mensaje', 'Actividad '.$actividad->nombre.' guardada');
            }
        }
        return redirect('actividades/metalicos/crear/'.$actividad->id)->with('mensaje', 'Actividad '.$actividad->nombre.' guardada');*/
    }

    /**
     * Muestra una actividad específica.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $actividad = Actividad::whereId($id)->firstOrFail();
        $profesor = $actividad->profesor;
        $metalicos = Metalico::where('actividad_id', $id)->get();
        if($profesor != null)
            $contactos =  Contacto::where('empresa_id', $profesor->empresa_id)->get();
        else{
            $contactos = NULL;
            }
        return view('actividades.show', compact('actividad', 'contactos', 'metalicos'));
    }

    /**
     * Muestra el formulario de una actividad.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $grupos = Grupo::orderBy('nombre','asc')->pluck('nombre', 'id');
        $aulas = Aula::orderBy('nombre', 'asc')->pluck('nombre', 'id');
        $responsables = Responsable::orderBy('nombre', 'asc')->pluck('nombre', 'id');
        $categorias = Categoria::pluck('nombre', 'id');
        $profesores = Profesor::orderBy('nombre_completo', 'asc')->pluck('nombre_completo', 'id');
        $actividad = Actividad::whereId($id)->firstOrFail();
        return view('actividades.edit', compact('actividad', 'grupos', 'aulas', 'responsables', 'categorias', 'profesores'));
    }

    /**
     * Actualiza y guarda una actividad.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ActividadFormRequest $request, $id)
    {
        $actividad = Actividad::whereId($id)->firstOrFail();
        $actividad->update($request->except(['hecha']));
        if ( $request->get('hecha')==null )
            $actividad->hecha = 0;
        else
            $actividad->hecha = 1;
        $actividad->save();
        if ($request->ajax()){
            if ($actividad) {
                $actividad->save();
                return response()->json([
                    'status'     => 'success',
                    ]);
            } else {
                return response()->json([
                    'status' => 'error',
                    'datos' => $metalico]);
            }
        }
        return redirect('/')->with('mensaje', 'Actividad '.$actividad->nombre.' modificada');
    }

    /**
     * Elimina una actividad.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $actividad = Actividad::whereId($id)->firstOrFail();
        $actividad->delete();

        return redirect('/')->with('mensaje', 'Actividad ' .$actividad->nombre.' eliminada');
    }

    /**
     * Muestra el aviso para eliminar una actividad.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function borrar($id)
    {
        $actividad = Actividad::whereId($id)->firstOrFail();
        return view('actividades.borrar', compact('actividad'));
    }

    /**
     * Muestra el formulario para duplicar una actividad.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function duplicar($id)
    {
        $grupos = Grupo::pluck('nombre', 'id');
        $aulas = Aula::pluck('nombre', 'id');
        $campanias = Campania::pluck('nombre', 'id');
        $responsables = Responsable::pluck('nombre', 'id');
        $categorias = Categoria::pluck('nombre', 'id');
        $profesores = Profesor::pluck('nombre_completo', 'id');
        $actividad = Actividad::whereId($id)->firstOrFail();
        return view('actividades.duplicar', compact('actividad', 'grupos', 'aulas', 'responsables', 'categorias', 'profesores', 'campanias'));
    }

}

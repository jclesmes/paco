<?php

namespace App\Http\Controllers;

use App\Grupo;
use App\Actividad;
use Illuminate\Http\Request;

class GruposController extends Controller
{
    /**
     * Muestra todos los grupos.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grupos = Grupo::all();
        return view('grupos.index', compact('grupos'));
    }

    /**
     * Muestra el formulario para crear un grupo nuevo.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('grupos.crear');
    }

    /**
     * Guarda un grupo nuevo.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $grupo = new Grupo;
        $grupo->nombre = $request->nombre;
        $grupo->save();
        return redirect('/grupos')->with('mensaje', 'Grupo guardado');  
    }

    /**
     * Muestra un grupo específico.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $grupo = Grupo::whereId($id)->firstOrFail();
        return view('grupos.show', compact('grupo'));
    }

    /**
     * Muestra el formulario para editar un grupo.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $grupo = Grupo::whereId($id)->firstOrFail();
        return view('grupos.edit', compact('grupo'));
    }

    /**
     * Guarda y actualiza un grupo.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $grupo = Grupo::whereId($id)->firstOrFail();
        $grupo->nombre = $request->get('nombre');
        $grupo->save();

        return redirect('/grupos')->with('mensaje', 'Grupo modificado');
    }

    /**
     * Elimina un grupo específico.
     *
     * @param  \App\Grupo  $grupo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $grupo = Grupo::whereId($id)->firstOrFail();
        $grupo->delete();

        return redirect('/grupos')->with('mensaje', 'Grupo eliminado');
    }

    /**
     * Muestra el aviso para eliminar un grupo específico.
     *
     * @param  \App\Grupo  $grupo
     * @return \Illuminate\Http\Response
     */
    public function borrar($id)
    {
        $actividades = Actividad::where('grupo_id', $id)->get();
        $grupo = Grupo::whereId($id)->firstOrFail();
        return view('grupos.borrar', compact('grupo', 'actividades'));
    }
}

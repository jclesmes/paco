<?php

namespace App\Http\Middleware;

use Closure;
use App\Campania;
class CampaniaSelect
{
    /**
     * Hace la comprobación de sesión para no meter actividades sin campaña.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $campania = Campania::All()->last();

        if(!session()->has('campaniaActiva') ){
            if($campania!=null){
                if(session()->has('sin_campania'))
                    session()->forget('sin_campania');
                session()->push('campaniaActiva', $campania->nombre);
                session()->push('campaniaActiva', $campania->id);
            }
            else {
                session(['sin_campania' => 'Debes de crear una campaña para continuar']);
            }

        }

        if($campania==null){
            session()->forget('campaniaActiva');
            session(['sin_campania' => 'Debes de crear una campaña para continuar']);
        }

        return $next($request);
    }
}

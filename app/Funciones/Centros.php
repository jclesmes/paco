<?php
/* 
*   Aquí voy metiendo las constantes de nombres
*/
namespace App\Funciones;
// Definimos centros
class Centros {
    /*
     *  Constantes para los textos de los centros.
     */
    const CASA = "En la Casa de la Juventud. (Avda. Doctor Toledo 44).";
    const MATAS = "En el Centro Cívico de las Matas. (Paseo de los Alemanes 31).";
    const JUVENTUD = "En el Centro de la Juventud. (Avda. Ntra. Sra. del Retamar 8).";
    const CASA_URL = '<a href="donde-estamos/casa-de-la-juventud" title="ir a Casa de la Juventud"><i class="fa fa-building-o" aria-hidden="true"></i> <span> En la Casa de la Juventud</span></a>';
    const JUVENTUD_URL = '<a href="donde-estamos/centro-de-la-juventud" title="ir a Centro de la Juventud"><i class="fa fa-building-o" aria-hidden="true"></i> <span> En el Centro de la Juventud</span></a>';
    const MATAS_URL = '<a href="donde-estamos/centro-civico-de-las-matas" title="ir a Centro Cívico de las Matas"><i class="fa fa-building-o" aria-hidden="true"></i> <span> En el Centro Cívico de las Matas</span></a>';
    
}

?>
<?php
/* 
*
* Aquí voy metiendo los estilos para el excel
*/
namespace App\Funciones;
// Definimos estilos
class Estilos {

    const ESTILO_TITULO_COLUMNAS = array(
        'font' => array(
            'name'  => 'Arial',
            'bold'  => true,
            'color' => array(
            'rgb' => '000000'
            )
        ),
        'alignment' =>  array(
            'horizontal'=> \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical'  => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            'wrap'      => TRUE
        ),
        'fill' => array(
            'fillType'      => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
            'rotation'      => 90,
            'startColor'    => array(
            'rgb'           => 'b2b8ce'
            ),
            'endColor' => array(
                'argb' => 'b2b8ce'
            )
        )
    );

    const ESTILO_CAMPANIA = array(
        'font' => array(
            'size' => 30,
            'name'  => 'Calibri',
            'bold'  => true,
            'color' => array(
                'rgb' => '000000'
            )
        ),
        'alignment' =>  array(
            'horizontal'=> \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical'  => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            'wrap'      => TRUE
        ),
        'fill' => array(
            'fillType'      =>  \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
            'rotation'      => 90,
            'startColor' => array(
                'rgb' => 'c3c4c9'
            ),
            'endColor' => array(
                'argb' => 'c3c4c9'
            )
        )
    );

    const ESTILO_TOTALES = array(
        'font' => array(
            'name'  => 'Calibri',
            'bold'  => false,
            'color' => array(
                'rgb' => '000000'
            )
        ),
        'alignment' =>  array(
            'horizontal'=> \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical'  => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            'wrap'      => TRUE
        ),
        'fill' => array(
            'fillType'      =>  \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
            'rotation'      => 90,
            'startColor' => array(
                'rgb' => 'c3c4c9'
            ),
            'endColor' => array(
                'argb' => 'c3c4c9'
            )
        )
    );

    const ESTILO_TOTALES_FIN = array(
        'font' => array(
            'name'  => 'Calibri',
            'bold'  => false,
            'color' => array(
                'rgb' => '000000'
            )
        ),
        'alignment' =>  array(
            'horizontal'=> \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical'  => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            'wrap'      => TRUE
        ),
        'fill' => array(
            'fillType'      =>  \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
            'rotation'      => 90,
            'startColor' => array(
                'rgb' => '2E2E2E'
            ),
            'endColor' => array(
                'argb' => 'c3c4c9'
            )
        )
    );

    const ESTILO_CATEGORIAS = array(
        '0'=>array(),
        '1'=>array(
            'font' => array(
                'name'  => 'Arial',
                'bold'  => true,
                'size' =>20,
                'color' => array(
                    'rgb' => '1F0F10'
                )
                ),
            'fill' => array(
                    'fillType'       => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
            'rotation'   => 90,
                    'startColor' => array(
                        'rgb' => '006039'
                    ),
                    'endColor' => array(
                        'argb' => '006039'
                    )
                ),
            ),
        '2' => array(
            'font' => array(
                'name'  => 'Arial',
                'bold'  => true,
                'size' =>20,
                'color' => array(
                    'rgb' => '1F0F10'
                )
                ),
            'fill' => array(
                    'fillType'       => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
            'rotation'   => 90,
                    'startColor' => array(
                        'rgb' => '0099d8'
                    ),
                    'endColor' => array(
                        'argb' => '0099d8'
                    )
                ),
            ),
        '3' => array(
            'font' => array(
                'name'  => 'Calibri',
                'bold'  => true,
                'size' =>20,
                'color' => array(
                    'rgb' => '1F0F10'
                )
                ),
            'fill' => array(
                    'fillType'       => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
            'rotation'   => 90,
                    'startColor' => array(
                        'rgb' => 'db3128'
                    ),
                    'endColor' => array(
                        'argb' => 'db3128'
                    )
                ),
            ),
        '4' => array(
            'font' => array(
                'name'  => 'Calibri',
                'bold'  => true,
                'size' =>20,
                'color' => array(
                    'rgb' => '1F0F10'
                )),
            'fill' => array(
                    'fillType'       => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
            'rotation'   => 90,
                    'startColor' => array(
                        'rgb' => 'dab61e'
                    ),
                    'endColor' => array(
                        'argb' => 'dab61e'
                    )
                ),
            ),
        '5' => array(
            'font' => array(
                'name'  => 'Calibri',
                'bold'  => true,
                'size' =>20,
                'color' => array(
                    'rgb' => '1F0F10'
                )
                ),
                'fill' => array(
                    'fillType'       => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
            'rotation'   => 90,
                    'startColor' => array(
                        'rgb' => '0c4370'
                    ),
                    'endColor' => array(
                        'argb' => '0c4370'
                    )
                ),
            ),
            '6' => array(
                'font' => array(
                    'name'  => 'Calibri',
                    'bold'  => true,
                    'size' =>20,
                    'color' => array(
                        'rgb' => '1F0F10'
                    )
                    ),
                    'fill' => array(
                        'fillType'       => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation'   => 90,
                        'startColor' => array(
                            'rgb' => 'ec6d23'
                        ),
                        'endColor' => array(
                            'argb' => 'ec6d23'
                        )
                    ),
                ),
                '7' => array(
                    'font' => array(
                        'name'  => 'Calibri',
                        'bold'  => true,
                        'size' =>20,
                        'color' => array(
                            'rgb' => '1F0F10'
                        )
                        ),
                        'fill' => array(
                            'fillType'       => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                    'rotation'   => 90,
                            'startColor' => array(
                                'rgb' => 'a7a523'
                            ),
                            'endColor' => array(
                                'argb' => 'a7a523'
                            )
                        ),
                    ),
                    '8' => array(
                        'font' => array(
                            'name'  => 'Calibri Compact',
                            'bold'  => true,
                            'size' =>20,
                            'color' => array(
                                'rgb' => '1F0F10'
                            )
                            ),
                        'fill' => array(
                            'fillType'       => \PhpOffice\PhpSpreadsheet\Style\Fill ::FILL_GRADIENT_LINEAR,
                        'rotation'   => 90,
                                'startColor' => array(
                                    'rgb' => 'c47cf2'
                                ),
                                'endColor' => array(
                                    'argb' => 'FF431a5d'
                                )
                            ),
                    )

    );

    // PARA METÁLICOS
    const ESTILO_CABECERA = array(
        'font' => array(
            'size' => 22,
            'name'  => 'calibri',
            'bold'  => true,
            'color' => array(
            'rgb' => 'FFFFFF'
            )
        ),
        'alignment' =>  array(
            'horizontal'=> \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical'  => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            'wrap'      => TRUE
        ),
        'fill' => array(
            'fillType'      => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
            'rotation'      => 90,
            'startColor'    => array(
            'rgb'           => '948A54'
            ),
            'endColor' => array(
                'argb' => '948A54'
            )
        )
    );

    const ESTILO_ACTIVIDAD = array(
        'font' => array(
            'size' => 14,
            'name'  => 'Calibri',
            'bold'  => false,
            'color' => array(
                'rgb' => 'FFFFFF'
            )
        ),
        'alignment' =>  array(
            'horizontal'=> \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical'  => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            'wrap'      => TRUE
        ),
        'fill' => array(
            'fillType'      =>  \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
            'rotation'      => 90,
            'startColor' => array(
                'rgb' => '76933C'
            ),
            'endColor' => array(
                'argb' => '76933C'
            )
        )
    );

    const ESTILO_TITULOS = array(
        'font' => array(
            'size' => 16,
            'name'  => 'Calibri',
            'bold'  => true,
            'color' => array(
                'rgb' => 'FFFFFF'
            )
        ),
        'alignment' =>  array(
            'horizontal'=> \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical'  => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            'wrap'      => TRUE
        ),
        'fill' => array(
            'fillType'      =>  \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
            'rotation'      => 90,
            'startColor' => array(
                'rgb' => '00B050'
            ),
            'endColor' => array(
                'argb' => '00B050'
            )
        )
    );

    const ESTILO_METALICO = array(
        'font' => array(
            'size' => 14,
            'name'  => 'Calibri',
            'bold'  => true,
            'color' => array(
                'rgb' => '000000'
            )
            ),
            'fill' => array(
                'fillType'       => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
        'rotation'   => 90,
                'startColor' => array(
                    'rgb' => 'D9D9D9'
                ),
                'endColor' => array(
                    'argb' => 'D9D9D9'
                )
            ),
    );

    const ESTILO_SUBTOTAL = array(
        'font' => array(
            'size' => 14,
            'name'  => 'Calibri',
            'bold'  => true,
            'color' => array(
                'rgb' => 'ffffff'
            )
            ),
            'fill' => array(
                'fillType'       => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
        'rotation'   => 90,
                'startColor' => array(
                    'rgb' => 'C00000'
                ),
                'endColor' => array(
                    'argb' => 'C00000'
                )
            ),
    );

    const ESTILO_TOTAL = array(
        'font' => array(
            'size' => 14,
            'name'  => 'Calibri',
            'bold'  => true,
            'color' => array(
                'rgb' => 'ffffff'
            )
            ),
            'fill' => array(
                'fillType'       => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
        'rotation'   => 90,
                'startColor' => array(
                    'rgb' => 'FF3300'
                ),
                'endColor' => array(
                    'argb' => 'FF3300'
                )
            ),
    );
    // PARA PREVENCIÓN DE RIESGOS
    const ESTILO_PREVENCION_CAMPANIA = array(
        'font' => array(
            'size' => 20,
            'name'  => 'Trebuchet MS',
            'bold'  => true,
            'color' => array(
                'rgb' => '000000'
            )
        ),
        'alignment' =>  array(
            'horizontal'=> \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical'  => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            'wrap'      => TRUE
        ),
        'fill' => array(
            'fillType'      =>  \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
            'rotation'      => 90,
            'startColor' => array(
                'rgb' => 'DAEEF3'
            ),
            'endColor' => array(
                'argb' => 'DAEEF3'
            )
        )
    );
    const ESTILO_PREVENCION_ROSA = array(
        'font' => array(
            'size' => 14,
            'name'  => 'Trebuchet MS',
            'bold'  => true,
            'color' => array(
                'rgb' => '000000'
            )
        ),
        'alignment' =>  array(
            'horizontal'=> \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical'  => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            'wrap'      => TRUE
        ),
        'fill' => array(
            'fillType'      =>  \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
            'rotation'      => 90,
            'startColor' => array(
                'rgb' => 'F2DCD9'
            ),
            'endColor' => array(
                'argb' => 'F2DCD9'
            )
        )
    );
    const ESTILO_PREVENCION_EMPRESA_CABECERA = array(
        'font' => array(
            'size' => 10,
            'name'  => 'Calibri',
            'bold'  => true,
            'color' => array(
                'rgb' => '000000'
            )
        ),
        'alignment' =>  array(
            'horizontal'=> \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical'  => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            'wrap'      => TRUE
        ),
        'fill' => array(
            'fillType'      =>  \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
            'rotation'      => 90,
            'startColor' => array(
                'rgb' => 'DAEEF3'
            ),
            'endColor' => array(
                'argb' => 'DAEEF3'
            )
        )
    );
    const ESTILO_PREVENCION_TITULOS = array(
        'font' => array(
            'size' => 10,
            'name'  => 'Calibri',
            'bold'  => true,
            'color' => array(
                'rgb' => '000000'
            )
        ),
        'alignment' =>  array(
            'horizontal'=> \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical'  => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            'wrap'      => TRUE
        ),
        'fill' => array(
            'fillType'      =>  \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
            'rotation'      => 90,
            'startColor' => array(
                'rgb' => 'F2F2F2'
            ),
            'endColor' => array(
                'argb' => 'F2F2F2'
            )
        )
    );
    const ESTILO_PREVENCION_EMPRESA= array(
        'font' => array(
            'size' => 8,
            'name'  => 'Calibri',
            'bold'  => false,
            'color' => array(
                'rgb' => '000000'
            )
        ),
        'alignment' =>  array(
            'horizontal'=> \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical'  => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            'wrap'      => TRUE
        ),
    );
    
    const BORDE = array(
        'borders' => array(
            'outline' => array(
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                'color' => array('argb' => '00000000'),
            ),
        ),
    );
    const BORDE_TODO = array(
        'borders' => array(
            'allBorders' => array(
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                'color' => array('argb' => '00000000'),
            ),
        ),
    );

    const BORDE_TODO_FINO = array(
        'borders' => array(
            'allBorders' => array(
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => array('argb' => '00000000'),
            ),
        ),
    );
}


?>
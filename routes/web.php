<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'as' => 'inicio',
    'uses' => 'ActividadesController@index']);

Route::post('/', [
        'as' => 'inicioFiltro',
        'uses' => 'ActividadesController@index2']);    

/*********************  RUTAS DE CAMPAÑAS  ******************************/
Route::prefix('campanias')->group(function () {
    Route::get('/', [
        'as' => 'campanias',
        'uses' => 'CampaniasController@index']);
    
    Route::get('crear', 'CampaniasController@create');
    Route::post('crear', [
        'as' => 'campaniasCrear',
        'uses' => 'CampaniasController@store']);
    
    Route::get('editar/{id}', 'CampaniasController@edit');
    Route::post('editar/{id}', [
        'as' => 'campaniasEditar',
        'uses' => 'CampaniasController@update']);
    
    Route::get('eliminar/{id}', [
        'as' => 'campaniasEliminar',
        'uses' => 'CampaniasController@destroy']);
    Route::get('borrar/{id}', [
        'as' => 'campaniasBorrar',
        'uses' => 'CampaniasController@borrar']);     
    
    Route::get('cambiar/{id}', [
        'as' => 'campaniasCambiar',
        'uses' => 'CampaniasController@cambiar']); 
        
    Route::get('{id}', 'CampaniasController@show');
}); 
/************************************************************************/

/*********************  RUTAS DE GRUPOS   *******************************/
Route::prefix('grupos')->group(function () {
    Route::get('/', [
        'as' => 'grupos',
        'uses' => 'GruposController@index']);
    
    Route::get('crear', 'GruposController@create');
    Route::post('crear', [
        'as' => 'gruposCrear',
        'uses' => 'GruposController@store']);
    
    Route::get('editar/{id}', 'GruposController@edit');
    Route::post('editar/{id}', [
        'as' => 'gruposEditar',
        'uses' => 'GruposController@update']);
    
    Route::get('eliminar/{id}', [
        'as' => 'gruposEliminar',
        'uses' => 'GruposController@destroy']);
    
    Route::get('borrar/{id}', [
        'as' => 'gruposBorrar',
        'uses' => 'GruposController@borrar']);
    
    Route::get('{id}', 'GruposController@show');
});
/************************************************************************/

/*********************** RUTAS DE CONTACTOS *****************************/
Route::prefix('contactos')->group(function () {
    Route::get('/', [
        'as'=> 'contactos',
        'uses'=>'ContactosController@index']);
    
    Route::get('crear', 'ContactosController@create');
    Route::post('crear', [
        'as' => 'contactosCrear',
        'uses' => 'ContactosController@store']);
    
    Route::get('editar/{id}', 'ContactosController@edit');
    Route::post('editar/{id}', [
        'as' => 'contactosEditar',
        'uses' => 'ContactosController@update']);
    Route::get('eliminar/{id}', [
            'as' => 'contactosEliminar',
            'uses' => 'ContactosController@destroy']);
    Route::get('borrar/{id}', [
        'as' => 'contactosBorrar',
        'uses' => 'ContactosController@borrar']);
});

/************************************************************************/

/************************* RUTAS DE AULAS *******************************/
Route::prefix('aulas')->group(function () {
    Route::get('/', [
        'as'=> 'aulas',
        'uses'=>'AulasController@index']);

    Route::get('crear', 'AulasController@create');
    Route::post('crear', [
        'as' => 'aulasCrear',
        'uses' => 'AulasController@store']);   

    Route::get('editar/{id}', 'AulasController@edit');
    Route::post('editar/{id}', [
        'as' => 'aulasEditar',
        'uses' => 'AulasController@update']);

    Route::get('eliminar/{id}', [
            'as' => 'aulasEliminar',
            'uses' => 'AulasController@destroy']);

    Route::get('borrar/{id}', [
        'as' => 'aulasBorrar',
        'uses' => 'AulasController@borrar']);

    Route::get('{id}', [
        'as' => 'aulasVer',
        'uses' => 'AulasController@show']); 
});
/************************************************************************/

/************************** RUTAS DE CATEGORIAS *************************/
Route::prefix('categorias')->group(function () {
    Route::get('/', [
        'as' => 'categorias',
        'uses' => 'CategoriasController@index']);
    
    Route::get('crear', 'CategoriasController@create');
    Route::post('categorias/crear', [
        'as' => 'categoriasCrear',
        'uses' => 'CategoriasController@store']);
    
    Route::get('/editar/{id}', 'CategoriasController@edit');
    Route::post('editar/{id}', [
        'as' => 'categoriasEditar',
        'uses' => 'CategoriasController@update']);
    
    Route::get('eliminar/{id}', [
        'as' => 'categoriasEliminar',
        'uses' => 'CategoriasController@destroy']);
    Route::get('borrar/{id}', [
        'as' => 'categoriasBorrar',
        'uses' => 'CategoriasController@borrar']);

    Route::get('{id}', 'CategoriasController@show');
});
/************************************************************************/

/********************** RUTAS DE RESPONSABLES ***************************/
Route::prefix('responsables')->group(function () {
    Route::get('/', [
        'as' => 'responsables',
        'uses' => 'ResponsablesController@index']);
    
    Route::get('crear', 'ResponsablesController@create');
    Route::post('crear', [
        'as' => 'responsablesCrear',
        'uses' => 'responsablesController@store']);
 
    Route::get('editar/{id}', 'ResponsablesController@edit');
    Route::post('editar/{id}', [
        'as' => 'responsablesEditar',
        'uses' => 'ResponsablesController@update']);

    Route::get('eliminar/{id}', [
        'as' => 'responsablesEliminar',
        'uses' => 'ResponsablesController@destroy']);
    
    Route::get('borrar/{id}', [
        'as' => 'responsablesBorrar',
        'uses' => 'ResponsablesController@borrar']);
       
    Route::get('{id}', [
        'as' => 'responsablesVer',
        'uses' =>'ResponsablesController@show']);    
});
/*********************************************************************/

/*********************** RUTAS DE ACTIVIDADES ************************/ 
Route::prefix('actividades')->group(function () {
    Route::get('crear', 'ActividadesController@create');
    Route::post('crear', [
        'as' => 'actividadesCrear',
        'uses' => 'ActividadesController@store']);
    
    Route::get('editar/{id}', 'ActividadesController@edit');
    Route::post('editar/{id}', [
        'as' => 'actividadesEditar',
        'uses' => 'ActividadesController@update']);
    
    Route::get('eliminar/{id}', [
        'as' => 'actividadesEliminar',
        'uses' => 'ActividadesController@destroy']);
    
    Route::get('borrar/{id}', [
        'as' => 'actividadesBorrar',
        'uses' => 'ActividadesController@borrar']);
        
    Route::get('duplicar/{id}', [
            'as' => 'actividadesDuplicar',
            'uses' => 'ActividadesController@duplicar']);    
                /*********************** RUTAS DE METÁLICOS ************************/ 
                Route::prefix('metalicos')->group(function () {
                    Route::get('/', [
                        'as' => 'metalicosTodos',
                        'uses' => 'MetalicoController@index'
                    ]);
                    Route::get('crear/{id}', [
                        'as' => 'metalicosCrear',
                        'uses' => 'MetalicoController@create'
                    ]);
                    Route::post('crear', [
                        'as' => 'metalicos',
                        'uses' => 'MetalicoController@store']);
                    Route::get('/{id}',[
                        'as' => 'verMetalicos',
                        'uses' => 'MetalicoController@show'
                    ]);
                    Route::get('editar/{id}', 'MetalicoController@edit');
                    Route::post('editar/{id}', [
                        'as' => 'metalicosEditar',
                        'uses' => 'MetalicoController@update']);
                    
                    Route::get('borrar/{id}', [
                            'as' => 'metalicosBorrar',
                            'uses' => 'MetalicoController@borrar']);
                    
                    Route::get('eliminar/{id}', [
                        'as' => 'metalicosEliminar',
                        'uses' => 'MetalicoController@destroy']);    
                });
    Route::get('{id}', [
        'as' => 'actividadesVer',
            'uses' => 'ActividadesController@show']);
});   
/********************************************************************/





/*************************** RUTAS DESCARGAS ********************************/
Route::prefix('descargas')->group(function () {
    Route::get('/', function(){
        return view('descargas.index');
    });
    Route::get('word/', [
        'as' => 'wordTodos',
        'uses' => 'WordController@index']);
    
    Route::get('word/{id}', [
        'as' => 'word',
        'uses' => 'WordController@show']);
    /********* RUTAS PDF's *********/
    Route::prefix('pdf')->group(function () {
        Route::get('/', [
            'as' => 'pdfTodos',
            'uses' => 'PdfController@index']);  
        Route::get('/dosier', [
            'as' => 'dosier',
            'uses' => 'DosierController@index']);
        Route::get('/dosier/rellenar/{id}', [
            'as' => 'dosierRellenar',
            'uses' => 'DosierController@create']);
        Route::post('/dosier/crear/{id}', [
            'as' => 'dosierGuardar',
            'uses' => 'DosierController@store']);    
        Route::get('/dosier/{id}', [
            'as' => 'dosierCrear',
            'uses' => 'DosierController@crearDosier']);
        Route::get('/dosier1/{id}', [
            'as' => 'prueba',
            'uses' => 'DosierController@crearDosier1']);
       
        Route::get('/{id}', [
            'as' => 'pdf',
            'uses' => 'PdfController@show']);
    });
    /**********************************/
    /********** RUTAS EXCEL ***********/
    Route::prefix('excel')->group(function () {
        Route::get('/', [
            'as' => 'excelTodos',
            'uses' => 'ExcelController@index']);

        Route::get('folleto', [
            'as' => 'folleto',
            'uses' => 'ExcelFolletoController@menu']);   
            
        Route::get('folleto/{categoria_id}', [
            'as' => 'excelFolleto',
            'uses' => 'ExcelFolletoController@folleto']);   
        
        Route::get('responsables', [
            'as' => 'excelResponsables',
            'uses' => 'ExcelResponsableFolletoController@responsables']);

        Route::get('metalicos', [
            'as' => 'excelMetalicos',
            'uses' => 'ExcelMetalicosController@crearExcel']);
            
        Route::get('prevencion', [
            'as' => 'excelPrevencion',
            'uses' => 'ExcelPrevencionController@prevencion']);   
    });       
    /***********************************/
    /************** XML ****************/
    Route::prefix('xml')->group(function () {
        Route::post('/', [
            'as' => 'xml',
            'uses' => 'XmlWebController@crearXml']);
        Route::get('/', [
            'as' => 'xmlCrear',
            'uses' => 'XmlWebController@xml']); 
    });
    /***********************************/
});

/************************** EMPRESAS ***************************************/
Route::prefix('empresas')->group(function () {
    Route::get('/', [
        'as' => 'empresas',
        'uses' => 'EmpresasController@index']);
    
    Route::get('crear', 'EmpresasController@create');
    Route::post('crear', [
        'as' => 'empresaCrear',
        'uses' => 'EmpresasController@store']);
    
    Route::get('editar/{id}', 'EmpresasController@edit');
    
    Route::get('borrar/{id}', [
            'as' => 'empresasBorrar',
            'uses' => 'EmpresasController@borrar']);
    
    Route::post('editar/{id}', [
        'as' => 'empresasEditar',
        'uses' => 'EmpresasController@update']);   
        
    Route::get('eliminar/{id}', [
        'as' => 'empresasEliminar',
        'uses' => 'EmpresasController@destroy']);  
    
    Route::get('{id}', [
        'as' => 'empresasVer',
            'uses' => 'EmpresasController@show']);
});
/***************************************************************************/

/********************************* PROFESORES ******************************/
Route::prefix('profesores')->group(function () {
    Route::get('/', [
        'as' => 'profesores',
        'uses' => 'ProfesorsController@index']);
    
    Route::get('crear', [
        'as' => 'profesorCrear',
        'uses' => 'ProfesorsController@create']);
    Route::post('crear', [
        'as' => 'profesoresStore',
        'uses' => 'ProfesorsController@store']);
    
    Route::get('{id}', [
        'as' => 'profesoresVer',
            'uses' => 'ProfesorsController@show']);
    
    Route::get('editar/{id}', 'ProfesorsController@edit');
    Route::post('editar/{id}', [
        'as' => 'profesoresEditar',
        'uses' => 'ProfesorsController@update']);
    
    Route::get('borrar/{id}', [
            'as' => 'profesoresBorrar',
            'uses' => 'ProfesorsController@borrar']);
    
    Route::get('eliminar/{id}', [
        'as' => 'profesoresEliminar',
        'uses' => 'ProfesorsController@destroy']);    
});
/**************************************************************************/


Route::get('/backup', function() {
    $exitCode = Artisan::call('db:backup');
    print_r($exitCode);
});

Route::get('/vendor', function() {
    $exitCode = shell_exec('php ../composer.phar update');
    print_r($exitCode);
});

Route::get('/calendario/aula/{id?}', [
    'as' => 'calendarioAula',
    'uses' => 'CalendarioController@calendarioAula'
]);

Route::get('/calendario/{id?}', [
    'as' => 'calendario',
    'uses' => 'CalendarioController@calendario'
]);

Route::get('calendario/api/sitio/{id}','CalendarioController@apiSitio');
Route::get('calendario/api/{id?}','CalendarioController@api'); //ruta que nos devuelve los eventos en formato json

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

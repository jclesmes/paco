## PACO

Aplicación PACO (Programación de Actividades por Campañas Online)


## Errores

Si encuentras alguno, por favor envíame un correo a Fracnisco Piedras Pérez [francisco.piedras83@gmail.com](mailto:francisco.piedras83@gmail.com).

## License

The PACO is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).

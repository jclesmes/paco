@extends('master')
@section('title', 'Borrar contacto')
@section('content')
<div class="container">
        <h1 class="text-center text-warning">Contacto a borrar</h1>
        <h2 class="text-center text-danger">Atención esta acción no se puede revertir, será eliminado para siempre</h2>
        <p>DATO:</p>
        <p>{!! $contacto->nombre_completo !!}</p>
        <a class="btn btn-danger" href="{!! action('ContactosController@destroy', $contacto->id) !!}">BORRAR</a>
</div>
<br>
@endsection
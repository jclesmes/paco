@extends('master')
@section('title', 'Editar contacto') 
@section('content')
<div class="container">
    <h1 class="text-center text-success">Datos del contacto a modificar</h1>
    <div class="form-group">
            {!! Form::open(['action' => ['ContactosController@update',  $contacto->id]]) !!}
        <div class="form-group">
            {!! Form::label('nombre_completo', 'Nombre completo:') !!}
            {!! Form::text('nombre_completo',  $contacto->nombre_completo, ['placeholder' => 'Nombre Apellido1 Apellido2',
                                                'class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('telefono') !!}
            {!! Form::number('telefono', $contacto->telefono, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('movil') !!}
            {!! Form::number('movil', $contacto->movil, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">    
            {!! Form::label('correo') !!}
            {!! Form::email('correo', $contacto->correo, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('puesto') !!}
            {!! Form::text('puesto', $contacto->puesto, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('empresa_id', 'Empresa') !!}
            {!! Form::select('empresa_id',  $empresas, $contacto->empresa->id , ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::hidden('id', $contacto->id)!!}
            {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>
<br>
@endsection
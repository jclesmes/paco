@extends('master')
@section('title', 'Crear contacto') 
@section('content')
<div class="container">
        <h1 class="text-center text-success">Datos del contacto</h1>
        <div class="form-group">
            {!! Form::open(['url' => 'contactos/crear', 'method'=> 'post']) !!}
            @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>{{ $error }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endforeach
            <div class="form-group">
                {!! Form::label('nombre_completo', 'Nombre completo') !!}
                {!! Form::text('nombre_completo', '', ['placeholder' => 'Nombre Apellido1 Apellido2',
                                                    'class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('telefono') !!}
                {!! Form::number('telefono', '', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('movil') !!}
                {!! Form::number('movil', '', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">    
                {!! Form::label('correo') !!}
                {!! Form::email('correo', '', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('puesto') !!}
                {!! Form::text('puesto', '', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('empresa_id', 'Empresa a la que pertenece') !!}
                {!! Form::select('empresa_id', $empresas, null, ['placeholder' => 'Selecciona una empresa ...', 'class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::submit('Crear contacto', ['class' => 'btn btn-primary']) !!}
                {!! Form::close() !!}
            </div>
        </div>
</div>
<br>
@endsection
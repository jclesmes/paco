@extends('master')
@section('title', 'Contactos existentes')
@section('content')
<div class="container">
    @if ($contactos->isEmpty())
    <h1 class="text-center text-warning">No hay contactos</h1>
    @else
    <h1 class="text-center text-success">Datos de los contactos</h1>
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Teléfono</th>
                    <th>Móvil</th>
                    <th>Correo</th>
                    <th>Puesto</th>
                    <th>Editar</th>
                    <th>Eliminar</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($contactos as $contacto)
                <tr>
                    <td>{!! $contacto->id !!}</td>
                    <td>{!! $contacto->nombre_completo !!}</td>
                    <td>{!! $contacto->telefono !!}</td>
                    <td>{!! $contacto->movil !!}</td>
                    <td>{!! $contacto->correo !!}</td>
                    <td>{!! $contacto->puesto !!}</td>
                    <td><a class="btn btn-success" href="{!! action('ContactosController@edit', $contacto->id) !!}"><i class="fas fa-edit"></i> editar</a></td>
                    <td><a class="btn btn-danger" href="{!! action('ContactosController@borrar', $contacto->id) !!}"><i class="fas fa-trash-alt"></i> borrar</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $contactos->render() }}
    @endif
</div>
<br>
@endsection
@extends('master')
@section('title', 'Borar categoria')
@section('content')
<div class="container">
    <h1 class="text-center text-warning">Categoría a borrar</h1>
    <h2 class="text-center text-danger">Atención esta acción no se puede revertir, será eliminado para siempre</h2>
    <h3>DATO:</h3>
    <p><strong>{!! $categoria->nombre !!}</strong></p>
    <a class="btn btn-danger" href="{!! action('CategoriasController@destroy', $categoria->id) !!}">BORRAR</a>
    @if(count($actividades) < 0)
        <div class="alert alert-warning">
            <strong>Sin categorias</strong>
        </div>                                      
    @else
        <h3>Actividades que se borrarán:</h3>
        <ol>
        @foreach($actividades as $actividad)
            <li>Actividad: {{ $actividad->nombre }}</li>
        @endforeach
        </ol>
    @endif
</div>
<br>
@endsection
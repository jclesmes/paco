@extends('master')
@section('title', 'Categorias existentes')
@section('content')
<div class="container">
    @if ($categorias->isEmpty())
        <h1 class="text-center text-warning">No hay categorías</h1>
    @else
    <h1 class="text-center text-success">Datos de las categorías</h1>
        <table class="table">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Editar</th>
                    <th>Borrar</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categorias as $categoria)
                <tr>
                    <td>{!! $categoria->nombre !!}</td>
                    <td><a class="btn btn-success" href="{!! action('CategoriasController@edit', $categoria->id) !!}"><i class="fas fa-edit"> editar</a></td>
                    <td><a class="btn btn-danger" href="{!! action('CategoriasController@borrar', $categoria->id) !!}"><i class="fas fa-trash-alt"> borrar</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    @endif
</div>
<br>
@endsection
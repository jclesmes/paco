@extends('master')
@section('title', 'Editar categoría') 
@section('content')
<div class="container">
    <h1 class="text-center text-success">Datos de la campaña a modificar</h1>
    <div class="form-group">
        {!! Form::open(['action' => ['CategoriasController@update',  $categoria->id]]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('nombre') !!}
        {!! Form::text('nombre',  $categoria->nombre, ['class' => 'form-control', 'value' => $categoria->nombre]) !!}
    </div>
    {!! Form::hidden('id', $categoria->id)!!}
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}
</div>
<br>
@endsection
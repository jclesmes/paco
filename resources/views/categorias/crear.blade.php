@extends('master')
@section('title', 'Crear categoría') 
@section('content')
<div class="container">
        <h1 class="text-center text-success">Datos de la categoria</h1>
    <div class="form-group">
        {!! Form::open(['url' => 'categorias/crear', 'method'=> 'post']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('nombre', 'Nombre de la categoria: ') !!}
        {!! Form::text('nombre', '', ['class' => 'form-control']) !!}
    </div>
    {!! Form::submit('Crear categoria', ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}
</div>
<br>
@endsection
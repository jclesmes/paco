@extends('master')
@section('content')
<div class="container">
        <h1 class="text-center text-success">Datos de los responsables</h1>
    <div class="form-group">
        {!! Form::open(['url' => 'responsables/crear', 'method'=> 'post']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('nombre', 'Nombre del responsable: ') !!}
        {!! Form::text('nombre', '', ['class' => 'form-control']) !!}
    </div>
        {!! Form::submit('Crear responsable', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}
</div>
<br>
@endsection
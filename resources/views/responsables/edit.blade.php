@extends('master')
@section('content')
<div class="container">
    <h1 class="text-center text-success">Datos del responsable a modificar</h1>
    <div class="form-group">
        {!! Form::open(['action' => ['ResponsablesController@update',  $responsable->id]]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('nombre') !!}
        {!! Form::text('nombre',  $responsable->nombre, ['class' => 'form-control', 'value' => $responsable->nombre]) !!}
    </div>
    {!! Form::hidden('id', $responsable->id)!!}
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}
</div>
<br>
@endsection
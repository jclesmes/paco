@extends('master')
@section('title', 'Borar responsable')
@section('content')
<div class="container">
        <h1 class="text-center text-warning">responsable a borrar</h1>
        <h2 class="text-center text-danger">Atención esta acción no se puede revertir, será eliminado para siempre</h2>
        <p>DATO:</p>
        <p>{!! $responsable->nombre !!}</p>
        <a class="btn btn-danger" href="{!! action('ResponsablesController@destroy', $responsable->id) !!}">BORRAR</a>
</div>
<br>
@endsection
@extends('master')
@section('title','Resumen '. $responsable->nombre)
@section('content')
<div class="container">
        <h1 class="text-center text-success">Datos del responsable</h1>
        <h2>Responsable: <strong>{!! $responsable->nombre !!}</strong></h2>
        @if(count($actividades) < 1)
        <div class="alert alert-warning">
                <strong>Sin actividades a cargo</strong>
        </div>                                      
        @else
        <h3>Actividades a cargo:</h3>
        <table class="table">
                <thead class="thead-dark">
                        <tr>
                        <th scope="col">Actividad</th>
                        <th scope="col">Fecha inicio</th>
                        <th scope="col">Fecha fin</th>
                        <th scope="col">Dias</th>
                        </tr>
                </thead>
                <tbody>
                @foreach($actividades as $actividad)
                <tr>
                        <th scope="row"><a href="{{ route('actividadesEditar', $actividad->id) }}"><i class="fas fa-edit"></i></a>{{ $actividad->nombre }}</th>
                        <td scope="row">{{ $actividad->fecha_inicio }}</td>
                        <td scope="row">{{ $actividad->fecha_fin }}</td>
                        <td scope="row">{{ $actividad->dias }}</td>
                </tr>
                @endforeach         
                </tbody>
        </table>
        @endif
</div>
<br>
@endsection
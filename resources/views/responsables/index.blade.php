@extends('master')
@section('title', 'Responsables existentes')
@section('content')
<div class="container">
    @if ($responsables->isEmpty())
        <h1 class="text-center text-warning">No hay responsables</h1>
    @else
        <h1 class="text-center text-success">Datos de los responsables</h1>
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>RELACIÓN ACTIVIDADES</th>
                    <th>Nombre</th>
                    <th>Editar</th>
                    <th>Borrar</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($responsables as $responsable)
                <tr>
                    <td><a class="btn btn-primary" href="{!! action('ResponsablesController@show', $responsable->id) !!}">VER ACTIVIDADES</a></td>
                    <td>{!! $responsable->nombre !!}</td>
                    <td><a class="btn btn-success" href="{!! action('ResponsablesController@edit', $responsable->id) !!}"><i class="fas fa-edit"> editar</a></td>
                    <td><a class="btn btn-danger" href="{!! action('ResponsablesController@borrar', $responsable->id) !!}"><i class="fas fa-trash-alt"> borrar</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    @endif
</div>
<br>
@endsection
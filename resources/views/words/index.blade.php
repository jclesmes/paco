@extends('master')
@section('title', 'Lista de actividades en Word')
@section('content')
<div class="container">
    @if ($count<1)
        <h2 class="text-center">No hay actividades que mostar</h2>
    @else
        <h2 class="text-center">Actividades - Word</h2>
        <ol>
        @foreach ($files as $filename)
            @if($filename=="." || $filename==".." || $filename=="download.php" || $filename==".htaccess")
                {{--  Nada que hacer  --}}
            @else       
             <li>
                <a href="{{url('words')}}/download.php?filename={{$filename}}">{{$filename}}</a>
                <a href="{{url('words')}}/download.php?filename={{$filename}}" class='btn text-success'><i class='fa  fa-download'></i></a></br>
             </li>
            @endif
        @endforeach
    </ol>
    @endif
</div>
<br>
@endsection
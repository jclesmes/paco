<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('/css/pdfs.css') }}">
</head>

<body>
    <div class="envo_nombre">
        <span class="nombre">{{$actividad->nombre}}</span><br>
    </div>
    <div class="envoltorio">
        <p class="encabezado">Datos administrativos</p>
        <div class="texto">
            <b>Categoria: </b>{{$actividad->categoria->nombre}}<br>
            <b>Actividad: </b>&#09;{{$actividad->nombre}}<br>
            <b>Grupo: </b>&#09;{{$actividad->grupo->nombre}}<br>
            <b>Precio: </b>{{$actividad->precio}} €<br>
            <b>Coste: </b>{{$actividad->coste_estimado_maximo}} €<br>
            <b>Responsable: </b>{{$actividad->responsable->nombre}}<br>
            <b>Profesor: </b>{{$actividad->profesor->nombre_completo or 'Sin profesor'}}<br>
            <b>Tfno. profesor: </b>{{$actividad->profesor->telefono or ''}}<br>
            <b>Empresa: </b>{{$actividad->profesor->empresa->nombre_empresa or 'Sin empresa'}}<br>
            <b>Tfno. empresa: </b>{{$actividad->profesor->empresa->telefono or ''}}<br>
        </div>
        <br>
        <p class="encabezado"><b>Datos Fechas, horarios y lugar</b></p>
        <div class="texto">
            <b>Fecha inicio: </b>{{ $actividad->fecha_inicio }}<br>
            <b>Fecha finalización :</b>{{ $actividad->fecha_fin }}<br>
            <b>Grupo: </b>{{ $actividad->grupo->nombre }}<br>
            <b>Días: </b>{{ $actividad->dias }}<br>
            <b>Coste: </b>{{ $actividad->coste }}<br>
            <b>Hora inicio: </b>{{ $actividad->hora_inicio }}<br>
            <b>Excepto: </b>{{ $actividad->fecha_excluidos }}<br>
            <b>Duración: </b>{{ $actividad->horas }} h<br>
            <b>Aula: </b>{{ $actividad->aula->nombre }}<br>
            <b>Ubicación (actividades de exterior): </b>{{ $actividad->lugar }}<br>
            <b>Fin de inscripciones: </b>{{ $actividad->fecha_inscripciones }}<br>
        </div>
    </div>
    </div>
</body>

</html>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('/css/dosier.css') }}">
</head>

<body>
    <h1>{{ $actividad->nombre }}</h1>
    <div class="texto">
        <p><span class="mini-titulo">Fechas y horarios:</span> {{ $inicio_fin }}.</p>
        <p><span class="mini-titulo">Edades:</span> {{ $edad }}.</p>
        <p><span class="mini-titulo">Lugar:</span> {{ $lugar }}</p>   
        <p><span class="mini-titulo">Precio:</span> {{ $actividad->precio }} €.</p>
        <p><span class="mini-titulo">Incluye:</span> {{ $actividad->incluye }}</p>     
    </div>
    <div class="imagen">
        <img class="imagenp" sr="{{ $actividad->dosier->url_imagen }}" alt="" srcset="">
    </div>
</body>

</html>
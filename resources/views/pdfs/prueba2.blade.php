<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <style>
        .envoltorio {
            border-style: solid;
            border-width: 2px;
            width: 100%;
        }
        
        .nombre {
            margin: 0 auto;
            border-style: solid;
            border-width: 1px;
            text-align: center;
            border: bold;
            border-color: black;
            font-size: 25px;
            max-width: 80%;
        }
        .encabezado {
            background-color: rgb(53, 102, 181);
            color: white;
            font-size: 18px;
            font-weight: bold;
            vertical-align: middle;
        }
    </style>
    <div class="nombre"><b>{{$actividad->nombre}}</b></div>
        <p class="encabezado"><b>Participantes</b></p>
        <p><b>Participantes mínimos: </b>{{$actividad->par_min}}</p>
        <p><b>Participantes máximos: </b>{{$actividad->par_max}}</p>
        <p><b>Edad mínima:</b>{{$actividad->edad_minima}}</p>
        <p><b>Edad máxima:</b>{{$actividad->edad_maxima}}</p>
        <p class="encabezado"><b>Textos</b></p>
        <p><b>Incluye (para folleto):</b>{{$actividad->incluye}}</p>
        <p><b>Texto previo del folleto:</b>{{$actividad->texto_previo_folleto}}</p>
        <p><b>Observaciones para informadores:</b>{{$actividad->observaciones}}</p>
    </div>
</body>

</html>
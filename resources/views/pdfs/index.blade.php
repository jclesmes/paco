@extends('master')
@section('title', 'Lista de actividades en PDF')
@section('content')
<div class="container">
    @if ($count<1)
        <h2 class="text-center">No hay actividades que mostar</h2>
    @else
        <h2 class="text-center">Actividades - PDF</h2>
        <ol>
        @foreach ($files as $filename)
            @if($filename=="." || $filename==".." || $filename=="download.php" || $filename==".htaccess")
                {{--  Nada que hacer  --}}
            @else       
             <li>
                <a href="{{url('pdfs')}}/download.php?filename={{$filename}}">{{$filename}}</a>
                <a href="{{url('pdfs')}}/download.php?filename={{$filename}}" class='btn text-success'><i class='fas fa-download'></i></a></br>
             </li>
            @endif
        @endforeach
    </ol>
    @endif
</div>
<br>
@endsection
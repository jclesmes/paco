<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('/css/dosier.css') }}">
</head>

<body>
    <div class="columnaI">
        <h1>{{ $actividad->nombre }}</h1>
        <div class="texto">
            <p><span class="mini-titulo">Fechas y horarios:</span> {{ $inicio_fin }}.</p>
            <p><span class="mini-titulo">Edades:</span> {{ $edad }}.</p>
            <p><span class="mini-titulo">Lugar:</span> {{ $lugar }}</p>   
            <p><span class="mini-titulo">Precio:</span> {{ $actividad->precio }} €.</p>
            <p><span class="mini-titulo">Incluye:</span> {{ $actividad->incluye }}</p>     
        </div>
        <div class="imagen">
            <img class="imagenp" sr="{{ $actividad->dosier->url_imagen }}" alt="" srcset="">
        </div>
    </div>
    <div class="columnaD">
        <div class="categoria">{{ $categoria }}</div>
        <div class="texto descripcion">
            <span class="mini-titulo">Descripción de la actividad:</span><br>
                {{ $actividad->dosier->descripcion_dosier }}
        </div>
        <div class="texto contenido">
            <span class="mini-titulo">Contenidos:</span><br>
            {!! $actividad->dosier->contenido_dosier !!}
        </div>
        <div class="fijo-derecha">
            <div class="info">
                <p><span class="enfasis">Comunicaciones a los participantes:</span>La Concejalía utilizará los datos facilitados en la
                            inscripción para el envío de SMS o e-mails para mantenerte informado del proceso de inscripción o durante el desarrollo de la actividad así como de otras actividades.</p>
                <p><span class="enfasis">Devolución:</span>A tenor del artículo 47.2 de la ley de Reguladora de las Haciendas Locales 39/88. y el artículo 27.5 de la Ley de Tasas y Precios Públicos 8/1989. 
                            Sólo se tendrá de-recho a devolución del importe pagado, por la realización de la actividad a la que se ha inscrito, cuando ésta no se lleve a cabo por motivos imputables a esta administración.</p>
            </div>
            <h2>INFORMACIÓN E INSCRIPCIONES</h2>
            <div class="centro">
                <span class="centros">Centro de la Juventud</span>
                <div class="direccion">
                    Avda. Ntra. Sra. del Retamar 8. <br>
                    28232 Las Rozas. <br>
                    <img class="img-tlf" src="{{ asset('/img/telf.png') }}"> 91 757 96 00 <br>
                </div>
                <div class="horario">
                    <p>Horario:</p>
                    Lunes a viernes de 9.30 a 14 h.<br>
                    y de 16.30 a 20 h.<br>
                    Sábados de 10 a 14 h.
                </div>
            </div>
            <div class="centro">
                <span class="centros">Casa de la Juventud</span>
                <div class="direccion">
                    Avda. Dr. Toledo 44. <br>
                    28231 Las Rozas. <br>
                    <img class="img-tlf" src="{{ asset('/img/telf.png') }}"> 91 757 96 50 <br>
                </div>
                <div class="horario">
                    <p>Horario:</p>
                    Lunes a viernes de 9.30 a 14 h.<br>
                    y de 16.30 a 20 h.<br>
                    Sábados de 10 a 14 h.
                </div>
            </div>
        
            <div class="centro">
                <span class="centros">Centro Cívico de Las Matas</span>
                <div class="direccion">
                    Paseo de los Alemanes 31. <br>
                    28290 Las Matas. <br>
                    <img  class="img-tlf" src="{{ asset('/img/telf.png') }}"> 91 757 97 66 <br>
                </div>
                <div class="horario">
                    <p>Horario:</p>
                    Lunes a viernes de 9.30 a 14 h.<br>
                    y de 16.30 a 20 h.<br>
                    Sábados de 10 a 14 h.
                </div>
            </div>
            <div class="logo">
                <div class="logo-centro izq">www.rozasjoven.es</div>
                <div class="logo-centro ">juventud@lasrozas.es</div>
                <div class="logo-centro der">www.lasrozas.es</div>
            </div>
        </div>
    </div>
</body>

</html>
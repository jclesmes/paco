@extends('master')
@section('title', 'Creación de Dosiers')
@section('content')
<div class="container">
    <h1 class="text-success text-center">Página de administración de dosieres</h1>
    <table class="table table-bordered">
        <tr>
            <th>Actividad</th>
            <th>Descripción de la actividad</th>
            <th>Contenido</th>
            <th>Imagen</th>
            <th>Editar</th>
            <th>Generar</th>
        </tr>
        @foreach ($actividades as $actividad)
        <tr>
            <td>{{ $actividad->nombre }}</td>
            <td>{{ $actividad->dosier->descripcion_dosier or "Sin descripción" }}</td>
            <td>{!! $actividad->dosier->contenido_dosier or "Sin contenido" !!}</td>
            <td><img src="{!! $actividad->dosier->url_imagen!!}" alt="" width="100"></td>
            <td><a href="{{route('dosierRellenar', $actividad->id)}}" class='btn text-success'><i class='far fa-edit'></i></a></td>
            <td><a href="{{route('dosierCrear', $actividad->id)}}" class='btn text-success'><i class='far fa-file-pdf'></i></a></td>
        </tr>
        @endforeach
    </table>
</div>
<br>
@endsection
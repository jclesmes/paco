<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('/css/pdfs.css') }}">
</head>

<body>
    <div class="envo_nombre">
        <span class="nombre">{{$actividad->nombre}}</span><br>
    </div>
    <div class="envoltorio">
        <p class="encabezado">Participantes</p>
        <div class="texto">
            <b>Participantes mínimos: </b>{{$actividad->par_min}}<br>
            <b>Participantes máximos: </b>{{$actividad->par_max}}<br>
            <b>Edad mínima:</b>{{$actividad->edad_minima}}<br>
            <b>Edad máxima:</b>{{$actividad->edad_maxima}}<br>
        </div>
        <br>
        <p class="encabezado"><b>Textos</b></p>
        <div class="texto">
            <b>Incluye (para folleto):</b>{{$actividad->incluye}}<br>
            <b>Texto previo del folleto:</b>{{$actividad->texto_previo_folleto}}<br>
            <b>Observaciones para informadores:</b>{{$actividad->observaciones}}<br>
        </div>
    </div>
</body>

</html>
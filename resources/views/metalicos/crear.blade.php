@extends('master')
@section('title', 'Añadir metálicos')
@section('content')
<div class="container">
    <div class="alert alert-danger error" style="display:none"></div>
    <h1 class="text-center text-success">Añadir metálicos</h1>
    {!! Form::open(['url' => 'metalicos/crear', 'method'=> 'post', 'id' => 'formulario']) !!}

    <div class="row">
        <div class="col-md-4 form-group">
            {!! Form::label('concepto', 'Concepto') !!}
            {!! Form::text('concepto', '', ['class' => 'form-control']) !!}
        </div>
        <div class="col-md-4 form-group">
            {!! Form::label('importe', 'Importe') !!}
            {!! Form::number('importe','', ['class' => 'form-control', 'step' => '.01', 'placeholder' => 'ejem 20,50' ]) !!}
        </div>
        <div class="col-md-4 form-group">
            <label for="fecha_necesidad">Fecha de necesidad</label>
            <br>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fas fa-calendar-alt" aria-hidden="true"></i>
                </span>
                <input class="fecha form-control" type="text" name="fecha_necesidad" id="fecha_necesidad">
            </div>
        </div>
        <hr>
        <div class="col-md-4 form-group">
            {!! Form::label('notas', 'Notas') !!}
            {!! Form::text('notas','', ['class' => 'form-control']) !!}
        </div>
        <div class="col-md-4 form-group">
            {!! Form::label('dinero_gastado', 'Dinero gastado') !!}
            {!! Form::number('dinero_gastado','', ['class' => 'form-control', 'step' => '.01', 'placeholder' => 'ejem 18,50']) !!}
        </div>
        <div class="col-md-4 form-group">
            <label for="fechaInicio">Fecha justificacion</label>
            <br>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fas fa-calendar-alt" aria-hidden="true"></i>
                </span>
                <input class="fecha form-control" type="text" name="fecha_justificacion">
            </div>
        </div>
    </div>  
    <input type="hidden" name="actividad_id" value="{{ $id }}">   
    <div class="form-group">
        <input class="btn btn-info float-right" type="submit" value="Añadir" id="ajax">
        <a class="btn btn-danger float-left" href="{{ route('metalicosTodos') }}">Salir</a>
        <br>
        <br>
        <div id="success" class="alert alert-success alert-dismissible fade " role="alert">
            <strong>Metálico añadido</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
    {!! Form::close() !!}

</div>
</div>
<br>
@endsection
@section('js')
<script>
$("#ajax").click(function(event) {
    event.preventDefault();
    $('.alert.error').text('');
    var url = $('form').attr('accion');
    var datos = $('form').serialize();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': '{!! csrf_token() !!}'
        }
    })

    $.ajax({
        type: "post",
        url: "{{ url('actividades/metalicos/crear') }}",
        dataType: "json",
        data: datos,
        success: function(data){
            console.log('Añadido');
            $('.alert').addClass('show');
            $("#formulario")[0].reset();
            $("#success").fadeTo(2000, 500).slideUp(500, function(){
            $("#success").slideUp(500);   
            });
        },
        error: function(data){
                var error = data.responseJSON.errors;
                $('.alert.error').text('');
                console.log(error);
                for(var i in error){
                    for(var j in error[i]){
                        var message = error[i][j];

                        $('.alert.error').append(message).append('<br>');
                        //console.log(error[i][j]);
                    }
                }
        $('.alert.error').slideDown(200);
        }
    });
});
</script>
@endsection
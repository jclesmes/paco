@extends('master') 
@section('title', 'Metálicos de '.$actividad->nombre) 
@section('content')
<div class="container">
  <br>
  <a class="btn btn-success" href="{{ route('metalicosCrear', $actividad->id) }}"> AÑADIR METÁLICOS</a>
  <h1 class="text-center text-success">Metálicos de la actividad: {{ $actividad->nombre }} </h1>
  @if (count($metalicos)<1)
    <h2 class="text-center text-warning">No hay datos de metálicos</h2>
  @else
    <h2 class="text-center text-info">Dispones de los siguientes</h2>
    <table  class="table table-bordered">
      <thead class="thead-dark">
          <tr class="text-center">
              <th class="align-middle">#</th>
              <th class="align-middle">Concepto</th>
              <th class="align-middle">Importe</th>
              <th class="align-middle">Fecha necesidad</th>
              <th class="align-middle">Fecha inicio</th>
              <th class="align-middle">Dinero gastado</th>
              <th class="align-middle">Fecha justificacion</th>
              <th class="align-middle">Dinero sobrante</th>
              <th class="align-middle">Notas</th>
              <th class="align-middle">Editar</th>
              <th class="align-middle">Borrar</th>
          </tr>
      </thead>
      <tbody>
          @foreach ($metalicos as $metalico)
          <tr>
            <td>{{ $metalico->id }}</td>
            <td>{{ $metalico->concepto }}</td>
            <td>{{ $metalico->importe }}</td>
            <td>{{ $metalico->fecha_necesidad }}</td>
            <td>{{ $actividad->fecha_inicio }}</td>
            <td>{{ $metalico->dinero_gastado }}</td>
            <td>{{ $metalico->fecha_justificacion }}</td>
            <td>{{ ($metalico->importe-$metalico->dinero_gastado) }}</td>
            <td>{{ $metalico->notas }}</td>
            <td> <a class="btn btn-warning text-white" href="{!! route('metalicosEditar', $metalico->id) !!}"><i class="fas fa-edit"></i></a></td>
            <td> <a class="btn btn-danger text-white" href="{!! route('metalicosBorrar', $metalico->id) !!}"><i class="fas fa-trash-alt"></i></a></td>
          @endforeach
        </tr>
      </tbody>
    </table>
  @endif
</div>
<br>
@endsection
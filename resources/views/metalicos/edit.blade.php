@extends('master')
@section('title', 'Añadir metálicos')
@section('content')
<div class="container">
    <div class="alert alert-danger error" style="display:none"></div>
    <h1 class="text-center text-success">Añadir</h1>
        {!! Form::open(['action' => ['MetalicoController@update', $metalico->id]]) !!}
    <input type="hidden" name="id" value="{{ $metalico->id }}">
    <div class="row">
        <div class="col-md-4 form-group">
            {!! Form::label('concepto', 'Concepto') !!}
            {!! Form::text('concepto', $metalico->concepto, ['class' => 'form-control']) !!}
        </div>
        <div class="col-md-4 form-group">
            {!! Form::label('importe', 'Importe') !!}
            {!! Form::number('importe',$metalico->importe, ['class' => 'form-control', 'step' => '.01', 'placeholder' => 'ejem 20,50' ]) !!}
        </div>
        <div class="col-md-4 form-group">
            <label for="fecha_necesidad">Fecha de necesidad</label>
            <br>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fas fa-calendar-alt" aria-hidden="true"></i>
                </span>
                <input class="fecha form-control" type="text" name="fecha_necesidad" id="fecha_necesidad" value="{{ $metalico->fecha_necesidad }}">
            </div>
        </div>
        <hr>
        <div class="col-md-4 form-group">
            {!! Form::label('notas', 'Notas') !!}
            {!! Form::text('notas', $metalico->notas, ['class' => 'form-control']) !!}
        </div>
        <div class="col-md-4 form-group">
            {!! Form::label('dinero_gastado', 'Dinero gastado') !!}
            {!! Form::number('dinero_gastado', $metalico->dinero_gastado, ['class' => 'form-control', 'step' => '.01', 'placeholder' => 'ejem 18,50']) !!}
        </div>
        <div class="col-md-4 form-group">
            <label for="fechaInicio">Fecha justificacion</label>
            <br>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fas fa-calendar-alt" aria-hidden="true"></i>
                </span>
                <input class="fecha form-control" type="text" name="fecha_justificacion" value="{{ $metalico->fecha_justificacion }}">
            </div>
        </div>
    </div>  
      
    <div class="form-group">
        <input class="btn btn-info float-right" type="submit" value="Añadir" id="ajax">
        <a class="btn btn-danger float-left" href="{{ route('inicio') }}">Salir</a>
        <br>
        <br>
        <div id="success" class="alert alert-success alert-dismissible fade " role="alert">
            <strong>Metálico añadido</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
    {!! Form::close() !!}

</div>
</div>
<br>
@endsection
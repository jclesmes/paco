@extends('master')
@section('title', 'Borrar metálico: '.$metalico->concepto)
@section('content')
<div class="container">
        <h1 class="text-center text-warning">Metálico a borrar</h1>
        <h2 class="text-center text-danger">Atención esta acción no se puede revertir, será eliminado para siempre</h2>
        <p>DATO:</p>
        <p>{!! $metalico->concepto !!}</p>
        <a class="btn btn-danger" href="{!! action('MetalicoController@destroy', $metalico->id) !!}">BORRAR</a>
</div>
<br>
@endsection
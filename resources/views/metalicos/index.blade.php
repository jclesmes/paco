@extends('master')
@section('title', 'Resumen metálicos')
@section('content')
<div class="container-fluid">
    @if (count($actividades) < 1 )
        <h1 class="text-center text-warning">No hay metálicos en ninguna actividad</h1>
    @else
        <h1 class="text-center text-success">Resumen de los metálicos</h1>
        <div class="horizontal">
        <table id="tabla" class="table table-bordered">
            @foreach ($actividades as $actividad)
            <thead class="thead-dark">
                <tr>
                    <th>{{ $actividad->nombre }}</th>
                    <th>Concepto</th>
                    <th>importe</th>
                    <th>Fecha necesidad</th>
                    <th>Gastado</th>
                    <th>Fecha justif.</th>
                    <th>Notas</th>
                    <th>Editar</th>
                    <th>Borrar</th>
                </tr>
            </thead>
            <tbody>
                @php ($i = 0)
                @foreach ($actividad->metalicos as $metalico)
                <tr>
                    @php ($i++)
                    <td>{{ $i }}</td>
                    <td>{!! $metalico->concepto !!}</td>
                    <td>{!! $metalico->importe !!}</td>
                    <td>{!! $metalico->fecha_necesidad !!}</td>
                    <td>{!! $metalico->gastado !!}</td>
                    <td>{!! $metalico->fecha_justificacion !!}</td>
                    <td>{!! $metalico->notas !!}</td>
                    <td><a class="btn btn-success" href="{!! action('MetalicoController@edit', $metalico->id) !!}"><i class="fas fa-edit"></i> editar</a></td>
                    <td><a class="btn btn-danger" href="{!! action('MetalicoController@borrar', $metalico->id) !!}"><i class="fas fa-trash-alt"> borrar</a></td>
                </tr>
                @endforeach
            </tbody>
            @endforeach
        </table>
    </div>
    @endif
</div>
</br>
@endsection
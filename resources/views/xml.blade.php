@extends('master')
@section('title', 'Generar XML para Web') 
@section('content')
<div class="container">
    <h1 class="text-center text-success">Generador XML para web J2XML</h1>
    <h2 class="text-center text-info">Pasos a seguir:</h2>
    <ol class="list-group">
        <li class="list-group-item">Abrir la administración web de rozasjoven.es <a target="blank" href="https://www.rozasjoven.es/administrator/">AQUÍ</a></li>
        <li class="list-group-item">Ir a contenido->artículos</li>
        <li class="list-group-item">Pulsar en limpiar para quitar los filtros si los hubiese</li>
        <li class="list-group-item">En la derecha ordenar por: ID - Descendente</li>
        <li class="list-group-item">Apuntar aquí el último ID existente</li>
    </ol>
    <div class="form-group">
        {!! Form::open(['url' => 'descargas/xml', 'method'=> 'post']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('id', 'Id del último artículo creado en la web: ') !!}
        {!! Form::number('id', '', ['class' => 'form-control', 'required', 'min'=>1, 'step'=>1]) !!}
    </div>
    {!! Form::submit('Descargar XML', ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}
    <hr>
    <ol class="list-group">
        <li class="list-group-item">En rozasjoven.es ir a componentes -> J2XML</li>
        <li class="list-group-item">Open y buscar el archivo descargado</li>
        <li class="list-group-item">Seleccionar el archivo</li>
        <li class="list-group-item">Pulsar import</li>
    </ol>
</div>
<br>
@endsection
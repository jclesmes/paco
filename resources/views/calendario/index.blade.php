@extends('master')
@section('title', 'Calendario de actividades')
@section('estilos')
    <link rel="stylesheet" href="{{ asset('/css/fullcalendar/fullcalendar.css') }}">
    {{--  <link rel="stylesheet" href="{{ asset('/css/fullcalendar/fullcalendar.print.min.css') }}">  --}}
@endsection
@section('content')
    <div class="container">
        <div class="form-group col-md-3">
            <label for="centro">Selecciona centro</label>
            <select class="form-control" id="centro">
                <option value="todas">Todas</option>
                <option value="centro">Centro de juventud</option>
                <option value="casa">Casa de juventud</option>
                <option value="matas">Las Matas</option>
                <option value="exterior">Exterior</option>
            </select>
        </div>
        <br>
        {{--  Modal que aparece cuando no hay datos  --}}
        <div id="modal" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Actividades</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>No hay actividades para el filtro seleccionado.</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
              </div>
            </div>
          </div>
          {{-- Fin del modal --}}
        <div class="col-md-12" id="calendar"></div>
    </div>
@endsection
@section('js')
    <script src="{{ asset('/js/fullcalendar/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('/js/fullcalendar/locale/es.js') }}"></script>
    <script src="{{ asset('/js/fullcalendar/gcal.min.js') }}"></script>

    <script>

        // Llamada a la API dependiendo de la selección
        function acceso(sitio){

            var centro = sitio;
            var url = null;
            console.log('El sitio es: ', centro);
            if (centro == 'todas')
                url = 'calendario/api/sitio/todas';
            if (centro == 'centro')
                url = 'calendario/api/sitio/centro';
            if (centro == 'casa')
                url = 'calendario/api/sitio/casa';
            if (centro == 'matas')
                url = 'calendario/api/sitio/matas';
            if (centro == 'exterior')
                url = 'calendario/api/sitio/exterior';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{!! csrf_token() !!}'
                }
            })
            
            $.ajax({
                type: "get",
                url: url,
                dataType: "json",
                success: function(json){
                    $('#calendar').fullCalendar( 'destroy' );
                    console.info('Petición correcta');
                    obj = JSON.stringify(json);
                    if(JSON.parse(obj).length == 0)
                        $('#modal').modal('show');    
                        //alert ('No hay actividades');
                    calendario();
                },
                error: function(xhr, err){
                    console.error('Error, algo salió mal al pedir los datos');
                    alert("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
                    alert("responseText: " + xhr.responseText);
                }
                });
            };
        
        // Pinta el calendario
        function calendario(){
            var initialLocaleCode = 'es';

            $('#calendar').fullCalendar({
                header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listMonth'
                },
                locale: initialLocaleCode,
                buttonIcons: true, // show the prev/next text
                weekNumbers: false,
                navLinks: true, // can click day/week names to navigate views
                editable: false,
                
                //events: window.$datos,
                events: JSON.parse(obj),
                timeFormat: 'H:mm'
            });            
        }
        
        
        $(document).ready(function() {
            acceso('todas');
        
        });
        
        
        $.each($.fullCalendar.valor, function(zonaCambiada) {
            $('#centro').append(
            $('<option/>')
                .attr('value', valor)
                .prop('selected', valor == zonaCambiada)
                .text(valor)
            );
        });
        
        // when the selected option changes, dynamically change the calendar option
        $('#centro').on('change', function() {
            if (this.value) {
                acceso(this.value);
            }
        });

        
    </script>


@endsection
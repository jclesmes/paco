@extends('master')
@section('title', 'Calendario de actividades de aula')
@section('estilos')
    <link rel="stylesheet" href="{{ asset('/css/fullcalendar/fullcalendar.css') }}">
    {{--  <link rel="stylesheet" href="{{ asset('/css/fullcalendar/fullcalendar.print.min.css') }}">  --}}
@endsection
@section('content')
    <div class="container">
        <br>
        <div class="col-md-12" id="calendar"></div>
    </div>
@endsection
@section('js')
    <script src="{{ asset('/js/fullcalendar/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('/js/fullcalendar/locale/es.js') }}"></script>
    <script src="{{ asset('/js/fullcalendar/gcal.min.js') }}"></script>

    <script>

        // Mi funcion
        function acceso(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{!! csrf_token() !!}'
                }
            })
            
            $.ajax({
                type: "get",
                url: "{{ url('calendario/api/')}}/{{$id}}",
                dataType: "json",
                success: function(json){
                    console.log('correcto');
                    //$datos = json;
                    //return $datos;
                    obj = JSON.stringify(json);
                    if(JSON.parse(obj).length == 0)
                        alert ('No hay actividades');
                    calendario();
                },
                error: function(xhr, err){
                    console.log('error');
                    alert("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
                    alert("responseText: " + xhr.responseText);
                }
                });
            };
        

        function calendario(){
            var initialLocaleCode = 'es';

            $('#calendar').fullCalendar({
                header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listMonth'
                },
                locale: initialLocaleCode,
                buttonIcons: true, // show the prev/next text
                weekNumbers: false,
                navLinks: true, // can click day/week names to navigate views
                editable: false,
                
                //events: window.$datos,
                events: JSON.parse(obj),
                timeFormat: 'H:mm'
            });            
        }
        
        acceso();
        /*$(document).ready(function() {
            $.when(acceso()).then(calendario());
            acceso();
            window.setTimeout(function(){calendario()}, 1000);
        });*/
        
        /*inicializamos el calendario al cargar la pagina
        $(document).ready(function() {
            acceso();
           window.setTimeout(function(){calendario()}, 500);
        });*/
        
    </script>


@endsection
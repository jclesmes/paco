@extends('master')
@section('title', 'Descarga de archivos')
@section('content')
<div class="container">
    <div class="h1 text-center">Enlaces para descargas</div>
    <ul class="list-group">
        <li class="list-group-item list-group-item-primary"><a href="{{ route('excelTodos') }}">Descargar Excel Actividades</a></li>
        <li class="list-group-item list-group-item-secondary"><a href="{{ route('pdfTodos') }}">Descargar Fichas en PDF</a></li>
        <li class="list-group-item list-group-item-secondary"><a href="{{ route('wordTodos') }}">Descargar Fichas en Word</a></li>
        <li class="list-group-item list-group-item-success"><a href="{{ route('excelResponsables') }}">Descargar Excel Responsables</a></li>
        <li class="list-group-item list-group-item-danger"><a href="{{ route('excelMetalicos') }}">Descargar Excel Metálicos</a></li>
        <li class="list-group-item list-group-item-warning"><a href="{{ route('excelPrevencion') }}">Descargar Excel Prevención Riesgos</a></li>
        <li class="list-group-item list-group-item-dark"><a href="{{ route('xml') }}">Descargar XML para Joomla-Rozasjoven.es </a></li>
        <li class="list-group-item list-group-item-dark"><a href="{{ route('folleto') }}">Descargar Adobe InDesign CC‎ Actividades</a></li>
    </ul>
</div>
<br>
@endsection
@extends('master')
@section('title', 'Descarga de archivos')
@section('content')
<div class="container">
    <div class="h1 text-center">Enlaces para descargas</div>
    <ul class="list-group">
        <li class="list-group-item list-group-item-success"><a href="{{ url('descargas/excel/folleto/0') }}">COMPLETO</a></li>
        @foreach ($categorias as $categoria)
         <li class="list-group-item list-group-item-primary"><a href="{{ route('excelFolleto', $categoria->id) }}">{{ $categoria->nombre }}</a></li>
        @endforeach
   </ul>
</div>
<br>
@endsection
@extends('master')
@section('title', 'Campañas existentes')
@section('content')
<div class="container">
    @if ($campanias->isEmpty())
        <h1 class="text-center text-warning">No hay campañas</h1>
        <p class="text-center"><a class="btn btn-primary" href="{{ route('campaniasCrear') }}">Crear nueva campaña</a></p>
    @else
    <h1 class="text-center text-success">Datos de las campañas</h1>
        <table class="table">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Usar esta campaña</th>
                    <th>Editar</th>
                    <th>Borrar</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($campanias as $campania)
                <tr>
                    <td>{!! $campania->nombre !!}</td>
                    <td><a class="btn btn-primary" href="{!! action('CampaniasController@cambiar', $campania->id) !!}"><i class="fas fa-exchange-alt"> usar</a></td>
                    <td><a class="btn btn-success" href="{!! action('CampaniasController@edit', $campania->id) !!}"><i class="fas fa-edit"> editar</a></td>
                    <td><a class="btn btn-danger" href="{!! action('CampaniasController@borrar', $campania->id) !!}"><i class="fas fa-trash-alt"> borrar</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    @endif
</div>
<br>
@endsection
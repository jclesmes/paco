@extends('master')
@section('title', 'Editar campaña') 
@section('content')
<div class="container">
    <h1 class="text-center text-success">Datos de la campaña a modificar</h1>
    <div class="form-group">
        {!! Form::open(['action' => ['CampaniasController@update',  $campania->id]]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('nombre') !!}
        {!! Form::text('nombre',  $campania->nombre, ['class' => 'form-control', 'value' => $campania->nombre]) !!}
    </div>
    {!! Form::hidden('id', $campania->id)!!}
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}
</div>
<br>
@endsection
@extends('master')
@section('title', 'Borrar campaña')
@section('content')
<div class="container">
    <h1 class="text-center text-warning">Campaña a borrar</h1>
    <h2 class="text-center text-danger">Atención esta acción no se puede revertir, serán eliminados para siempre la campaña y sus actividades</h2>
    <h3>DATO:</h3>
    <p><strong>{!! $campania->nombre !!}</strong></p>
    <a class="btn btn-danger" href="{!! action('CampaniasController@destroy', $campania->id) !!}">BORRAR</a>
</div>
<div class="container">
    @if(count($actividades) < 1)
        <br>
        <div class="alert alert-warning">
            <strong>Sin actividades</strong>
        </div>                                      
    @else
        <br>
        <h3>Actividades que se borrarán:</h3>
        <ol>
    @foreach($actividades as $actividad)
        <li>Actividad: {{ $actividad->nombre }}</li>
    @endforeach
        </ol>
    @endif
</div>
<br>
@endsection
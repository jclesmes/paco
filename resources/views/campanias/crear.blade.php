@extends('master')
@section('title', 'Crear campaña') 
@section('content')
<div class="container">
        <h1 class="text-center text-success">Datos de la campaña</h1>
    <div class="form-group">
        {!! Form::open(['url' => 'campanias/crear', 'method'=> 'post']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('nombre', 'Nombre de la campaña: ') !!}
        {!! Form::text('nombre',  '', ['class' => 'form-control']) !!}
    </div>
    {!! Form::submit('Crear campaña', ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}
</div>
<br>
@endsection
@extends('master')
@section('title', 'Duplicando actividad')
@section('content')
<div class="container">
    @foreach ($errors->all() as $error)
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>{{ $error }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endforeach
    <h1 class="text-center text-success">Datos de la actividad a modificar</h1>
    @if(Session::has('error'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>{{ Session::get('error') }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <div class="form-group">
        {!! Form::model($actividad, ['action' => ['ActividadesController@store']]) !!}
        <!-- PRIMERA FILA -->
        <div class="form-row">
            <div class="col-md-4">
                {!! Form::label('nombre', 'Nombre: ') !!}
                {!! Form::text('nombre' , $actividad->nombre, ['class' => 'form-control']); !!}
            </div>
            <div class="col-md-3">
                {!! Form::label('horas', 'Horas: ') !!}
                {!! Form::text('horas' , $actividad->horas, ['class' => 'form-control', 'pattern' => '[0123456789,]{1,}', 'placeholder' => 'Ej. 2,5']); !!}
            </div>
            <div class="col-md-5">
                <label for="fecha_excluidos">Fechas excluidas</label>
                <br>
                 <div class="input-group">
                    <span class="input-group-prepend">
                        <i class="input-group-text fas fa-calendar-alt" aria-hidden="true"></i>
                    </span>
                    <input class="form-control fechaExcluidos" type="text" name="fecha_excluidos" id="fecha_excluidos" value="{{ $actividad->fecha_excluidos }}">
                </div>  
            </div>   
        </div>
        <!-- SEGUNDA FILA -->
        <div class="form-row">
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('grupo_id', 'Grupo') !!}
                    {!! Form::select('grupo_id',  $grupos, $actividad->grupo->id , ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('aula_id', 'Aula') !!}
                    {!! Form::select('aula_id',  $aulas, $actividad->aula->id , ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <div class="form-group">
                        {!! Form::label('responsable_id', 'Responsable') !!}
                        {!! Form::select('responsable_id',  $responsables, $actividad->responsable->id , ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('coste_estimado_maximo', 'Coste empresa (AD)') !!}
                        {!! Form::number('coste_estimado_maximo', $actividad->coste_estimado_maximo, ['class' => 'form-control', 'step'=>'.01', 'min'=>0]) !!}
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="fechaInicio">Fecha de inicio</label>
                    <br>
                    <div class="input-group">
                        <span class="input-group-prepend">
                            <i class="input-group-text fas fa-calendar-alt" aria-hidden="true"></i>
                        </span>
                        <input class="form-control fecha" type="text" name="fecha_inicio" id="fecha_inicio" value="{{ $actividad->fecha_inicio }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="fechaFin">Fecha de fin</label>
                    <br>
                    <div class="input-group">
                        <span class="input-group-prepend">
                            <i class="input-group-text fas fa-calendar-alt" aria-hidden="true"></i>
                        </span>
                        <input class="form-control fecha" type="text" name="fecha_fin" id="fecha_fin" value="{{ $actividad->fecha_fin }}">
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                        {!! Form::label('dias', 'Dias ') !!}
                        {!! Form::text('dias' , $actividad->dias, ['class' => 'form-control', 'pattern' => '[lmxjvsd,]{1,}', 'title' => 'Sólo válido l m x j v s d  separados por comas sin espacios'] ) !!}  </div>
                <div class="form-group">
                        {!! Form::label('precio', 'Precio ') !!}
                        {!! Form::text('precio' , $actividad->precio, ['class' => 'form-control']) !!} </div>
            </div>
        </div>

        <!-- TERCERA FILA -->
        <div class="form-row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="horaInicio">Hora de inicio</label>
                    <br>
                    <div class="input-group">
                        <span class="input-group-prepend">
                            <i class="input-group-text fa far fa-clock" aria-hidden="true"></i>
                        </span>
                        <input class="form-control hora" type="text" name="hora_inicio" id="hora_inicio" value="{{ $actividad->hora_inicio }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="horaFin">Hora de fin</label>
                    <br>
                    <div class="input-group">
                        <span class="input-group-prepend">
                            <i class="input-group-text fa far fa-clock" aria-hidden="true"></i>
                        </span>
                        <input class="form-control hora" type="text" name="hora_fin" id="hora_fin" value="{{ $actividad->hora_fin }}">
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="parMin">Par.Min.</label>
                    <br>
                    <div class="input-group">
                        <span class="input-group-prepend">
                            <i class="input-group-text fas fa-users" aria-hidden="true"></i>
                        </span>
                        <input class="form-control" type="number" id="par_min" name="par_min" value="{{ $actividad->par_min }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="parMax">Par.Max.</label>
                    <br>
                    <div class="input-group">
                        <span class="input-group-prepend">
                            <i class="input-group-text fas fa-users" aria-hidden="true"></i>
                        </span>
                        <input class="form-control" type="number" id="par_max" name="par_max" value="{{ $actividad->par_max }}">
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="edadMinima">Edad mínima</label>
                    <br>
                    <div class="input-group">
                        <span class="input-group-prepend">
                            <i class="input-group-text fas fa-birthday-cake" aria-hidden="true"></i>
                        </span>
                        <input class="form-control" type="number" name="edad_minima" id="edad_minima" value="{{ $actividad->edad_minima }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="edadMaxima">Edad máxima</label>
                    <br>
                    <div class="input-group">
                        <span class="input-group-prepend">
                            <i class="input-group-text fas fa-birthday-cake" aria-hidden="true"></i>
                        </span>
                        <input class="form-control" type="number" name="edad_maxima" id="edad_maxima" value="{{ $actividad->edad_maxima }}">
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="fechaInscripciones">Fecha fin de inscripciones</label>
                    <br>
                    <div class="input-group">
                        <span class="input-group-prepend">
                            <i class="input-group-text fas fa-calendar-alt" aria-hidden="true"></i>
                        </span>
                        <input class="form-control fecha" type="text" name="fecha_inscripciones" id="fecha_inscripciones" value="{!! $actividad->fecha_inscripciones !!}"/>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('categoria_id', 'Categoría/Sección') !!}
                    {!! Form::select('categoria_id',  $categorias, $actividad->categoria->id , ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <!-- CUARTA FILA -->
        <div class="form-row">
            <div class="form-group col-md-12">
                {!! Form::label('incluye', 'Incluye (solo para folleto)') !!}
                {!! Form::textarea('incluye' , $actividad->incluye, ['class' => 'form-control', 'rows' => 2]) !!}
           </div>
            <div class="form-group col-md-12">
                {!! Form::label('texto_previo_folleto', 'Texto previo folleto ') !!}
                {!! Form::textarea('texto_previo_folleto' , $actividad->texto_previo_folleto, ['class' => 'form-control', 'rows' => 2]) !!}  </div>
            <div class="form-group col-md-12">
                {!! Form::label('lugar_celebracion', '* Lugar de celebración (Solo para actividades de exterior)') !!}
                {!! Form::text('lugar_celebracion' , $actividad->lugar_celebracion, ['class' => 'form-control']) !!} 
            </div>
            <div class="form-group col-md-12">
                {!! Form::label('observacinones', 'Observaciones ') !!}
                {!! Form::text('observaciones' , $actividad->observaciones, ['class' => 'form-control']) !!} 
            </div>
        </div>
        <!-- QUINTA FILA -->
        <div class="form-group">
            @if(isset($actividad->profesor->nombre_completo))
                {!! Form::label('profesor_id', 'Profesor') !!}
                {!! Form::select('profesor_id',  $profesores, $actividad->profesor->id , ['class' => 'form-control']) !!}
            @else
                {!! Form::label('profesor_id', 'Profesor') !!}
                {!! Form::select('profesor_id',  $profesores, null , ['placeholder' => 'Selecciona un profesor...','class' => 'form-control']) !!}
            @endif
        </div>
        <div class="form-group">
            {!! Form::label('campania_id', 'Campaña') !!}
            {!! Form::select('campania_id',  $campanias, null , ['placeholder' => 'Selecciona una campaña...','class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            <input class="btn btn-primary float-right" type="submit" value="Guardar">
            <br>
        </div>
        </div>
            {!! Form::close() !!}
        </div>
    </div>
<br>
@endsection
@extends('master')
@section('title', 'Crear nueva actividad')
@section('content')
<div class="container">
    @foreach ($errors->all() as $error)
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>{{ $error }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endforeach
    @if(Session::has('error'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>{{ Session::get('error') }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <h1 class="text-center text-success">Actividad a crear</h1>
    {!! Form::model(['url' => 'actividades/crear', 'method'=> 'post']) !!}
    <!-- PRIMERA FILA -->
    <div class="form-row">
        <div class="col-md-4">
            {!! Form::label('nombre', 'Actividad') !!}
            {!! Form::text('nombre', '', ['class' => 'form-control', 'required']) !!}
        </div>
        <div class="col-md-3">
            {!! Form::label('horas', 'Horas: ') !!}
            {!! Form::text('horas' , '', ['class' => 'form-control','pattern' => '[0123456789,]{1,}', 'placeholder' => 'Ej. 2,5']); !!}
        </div>
        <div class="col-md-5">
            <label for="fecha_excluidos">Fechas excluidas</label>
            <br>
             <div class="input-group">
                <span class="input-group-prepend">
                    <i class="input-group-text fas fa-calendar-alt" aria-hidden="true"></i>
                </span>
                <input class="form-control fechaExcluidos" type="text" name="fecha_excluidos" id="fecha_excluidos" value="{{ old('fecha_excluidos') }}">
            </div>  
        </div>  
    </div>
    <!-- SEGUNDA FILA -->
    <div class="form-row">
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('grupo_id', 'Grupo') !!}
                {!! Form::select('grupo_id', $grupos, null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('aula_id', 'Aula') !!}
                {!! Form::select('aula_id', $aulas, null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('responsable_id', 'Responsable') !!}
                {!! Form::select('responsable_id', $responsables, null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('coste_estimado_maximo', 'Coste empresa (AD)') !!}
                {!! Form::number('coste_estimado_maximo', '', ['class' => 'form-control', 'step'=>'.01', 'min'=>0]) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="fechaInicio">Fecha de inicio</label>
                <br>
                <div class="input-group">
                    <span class="input-group-prepend">
                        <i class="input-group-text fas fa-calendar-alt" aria-hidden="true"></i>
                    </span>
                    <input required class="form-control" type="text" name="fecha_inicio" id="fecha_inicio" value="{{ old('fecha_inicio') }}">
                </div>
            </div>
            <div class="form-group">
                <label for="fechaFin">Fecha fin</label>
                <br>
                <div class="input-group">
                    <span class="input-group-prepend">
                        <i class="input-group-text fas fa-calendar-alt" aria-hidden="true"></i>
                    </span>
                    <input required class="form-control" type="text" name="fecha_fin" id="fecha_fin" value="{{ old('fecha_fin') }}">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('dias', 'Dias') !!}
                {!! Form::text('dias', '', ['class' => 'form-control', 'pattern' => '[lmxjvsd,]{1,}', 'title' => 'Sólo válido l m x j v s d  separados por comas sin espacios', 'required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('precio', 'Precio') !!}
                {!! Form::number('precio', '', ['class' => 'form-control', 'step'=>'.01', 'min'=>0]) !!}
            </div>
        </div>
    </div>
    <!-- TERCERA FILA -->
    <div class="form-row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="horaInicio">Hora de inicio</label>
                <br>
                <div class="input-group">
                    <span class="input-group-prepend">
                        <i class="input-group-text far fa-clock" aria-hidden="true"></i>
                    </span>
                    <input class="hora form-control" type="text" name="hora_inicio" id="hora_inicio" value="{{ old('hora_inicio') }}">
                </div>
            </div>
            <div class="form-group">
                <label for="horaFin">Hora de fin</label>
                <br>
                <div class="input-group">
                    <span class="input-group-prepend">
                        <i class="input-group-text far fa-clock" aria-hidden="true"></i>
                    </span>
                    <input class="form-control hora" type="text" name="hora_fin" id="hora_fin" value="{{ old('hora_fin') }}">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="parMin">Par.Min.</label>
                <br>
                <div class="input-group">
                    <span class="input-group-prepend">
                        <i class="input-group-text fas fa-users" aria-hidden="true"></i>
                    </span>
                    <input class="form-control" type="number" id="par_min" name="par_min" value="{{ old('par_min') }}">
                </div>
            </div>
            <div class="form-group">
                <label for="parMax">Par.Max.</label>
                <br>
                <div class="input-group">
                    <span class="input-group-prepend">
                        <i class="input-group-text fas fa-users" aria-hidden="true"></i>
                    </span>
                    <input class="form-control" type="number" id="par_max" name="par_max" value="{{ old('par_max') }}">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="edadMinima">Edad mínima</label>
                <br>
                <div class="input-group">
                    <span class="input-group-prepend">
                        <i class="input-group-text fas fa-birthday-cake" aria-hidden="true"></i>
                    </span>
                    <input class="form-control" type="number" name="edad_minima" id="edad_minima" value="{{ old('edad_minima') }}">
                </div>
            </div>
            <div class="form-group">
                <label for="edadMaxima">Edad máxima</label>
                <br>
                <div class="input-group">
                    <span class="input-group-prepend">
                        <i class="input-group-text fas fa-birthday-cake" aria-hidden="true"></i>
                    </span>
                    <input class="form-control" type="number" name="edad_maxima" id="edad_maxima" value="{{ old('edad_maxima') }}">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="fechaInscripciones">Fecha fin de inscripciones</label>
                <br>
                <div class="input-group">
                    <span class="input-group-prepend">
                        <i class="input-group-text fas fa-calendar-alt" aria-hidden="true"></i>
                    </span>
                    <input required class="form-control fecha" type="text" name="fecha_inscripciones" id="fecha_inscripciones" value="{{ old('fecha_inscripciones') }}"/>
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('categoria_id', 'Categoría/Sección') !!}
                {!! Form::select('categoria_id', $categorias, null, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
    <!-- CUARTA FILA -->
    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="incluye">Incluye (para folleto)</label>
            <textarea class="form-control" name="incluye" id="incluye" rows="2" value="{{ old('incluye') }}"></textarea>
        </div>
        <div class="form-group col-md-12">
                {!! Form::label('texto_previo_folleto', 'Texto previo del folleto') !!}
                {!! Form::textarea('texto_previo_folleto', '', ['class' => 'form-control', 'rows' => 2]) !!}
        </div>
        <div class="form-group col-md-12">
            {!! Form::label('lugar_celebracion', '* Lugar de celebración (Solo para actividades de exterior)') !!}
            {!! Form::text('lugar_celebracion', '', ['class' => 'form-control']) !!}
        </div>
        <div class="form-group col-md-12">
            {!! Form::label('observaciones', 'Observaciones para informadores') !!}
            {!! Form::textarea('observaciones', '', ['class' => 'form-control', 'rows' => 2]) !!}
        </div>
    </div>
    <!-- QUINTA FILA -->
    <div class="form-group">
            {!! Form::label('profesor_id', 'Profesor') !!}
            {!! Form::select('profesor_id', $profesores, null, ['placeholder' => 'Selecciona un profesor ...', 'class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        <input class="btn btn-primary float-right" type="submit" value="Guardar">
        <br>
    </div>
    <input type="hidden" name="campania_id" value="{!! session('campaniaActiva')[1] !!}">
    {!! Form::close() !!}

</div>
<br>
@endsection
@extends('master') 
@section('title', 'Actividad '.$actividad->nombre) 
@section('content')
<div class="container">
  <h1 class="text-center text-success">Datos de la actividad</h1>
  <br>
  <a class="btn btn-info" href="{!! route('verMetalicos', $actividad->id) !!}">METÁLICOS</a>
  <a class="btn btn-success float-right" href="{!! route('actividadesEditar', $actividad->id) !!}">EDITAR ACTIVIDAD</a>
  <hr>
  
  <div class="card text-white bg-primary mb-3">
    <div class="card-header">Actividad</div>
    <div class="card-body">
      <h4 class="card-title">
        {!! $actividad->nombre !!}
      </h4>
      <p class="card-text">
        {!! '<b>Descripción:</b>  <i>'.$actividad->texto_previo_folleto.'</i>'!!}
      </p>
      <p class="card-text">
          {!! '<b>Incluye: </b><i>' .    $actividad->incluye.'</i>' !!} 
      </p>
      <p class="card-text">
          {!! '<b>Observaciones:</b> <i>' .  $actividad->observaciones.'</i>'!!}
      </p>
    </div>
  </div>
  <div class="card text-white bg-success mb-3">
    <div class="card-header">Lugar y horarios</div>
    <div class="card-body">
      <p class="card-text">
          {!! '<b>Lugar de celebración:</b> <i>' .    $actividad->lugar_celebracion.'</i>'!!}
      </p>
      <p class="card-text">
        {!! '<b>Fecha de inicio:</b> <i>' . $actividad->fecha_inicio .'</i>'!!}
      </p>
      <p class="card-text">
        {!! '<b>Fecha de fin:</b> <i>' . $actividad->fecha_fin .'</i>'!!}
      </p>
      <p class="card-text">
        {!! '<b>Excluidos:</b> <i>' . ($actividad->fecha_excluidos).'</i>'!!}
    </p>
      <p class="card-text">
          {!! '<b>Hora de inicio:</b> <i>' . Carbon\Carbon::parse( $actividad->hora_inicio)->format('G:i').'</i>' !!}
      </p>
      <p class="card-text">
        {!! '<b>Hora de finalización: </b><i>' . Carbon\Carbon::parse( $actividad->hora_fin)->format('G:i').'</i>' !!}
      </p>
      <p class="card-text">
        {!! '<b>Horas: </b><i>' . $actividad->horas . '</i>' !!}
      </p>
      <p class="card-text">
        {!! '<b>Dias:</b> <i>' .    $actividad->dias.'</i>' !!}
      </p>
    </div>
  </div>
  <div class="card text-white bg-secondary mb-3">
    <div class="card-header">Datos</div>
    <div class="card-body">
      <p class="card-text">
        {!! '<b>Precio:</b> <i>' . $actividad->precio . '€</i>' !!}
      </p>
      <p class="card-text">
        {!! '<b>Participantes mínimos:</b> <i>' . $actividad->par_min.'</i>' !!}
        
      </p>
      <p class="card-text">
        {!! '<b>Participantes máximos:</b> <i>' . $actividad->par_max.'</i>' !!}
      </p>
        {!! '<b>Edad mínima:</b> <i>' . $actividad->edad_minima.'</i>' !!}
      <p class="card-text">
      </p>
      <p class="card-text">
        {!! '<b>Edad máxima:</b> <i>' . $actividad->edad_maxima.'</i>' !!}
      </p>
    </div>
  </div>
  
  @if(isset($actividad->profesor->nombre_completo))
  <div class="card text-white bg-info w-100">
    <div class="card-header">Profesor de la actividad</div>
    <div class="card-body">
      <h4 class="card-title">
          {!! '<b>Profesor</b> ' . $actividad->profesor->nombre_completo . ' | <b>Teléfono:</b> ' . 
              $actividad->profesor->telefono . ' | <b>Correo:</b> ' . $actividad->profesor->correo !!}
      </h4>
    </div>
  </div>
  <br>
  <div class="card text-white bg-dark w-100">
    <div class="card-header">Empresa</div>
    <div class="card-body">
      <h4 class="card-title">
          {!! $actividad->profesor->empresa->nombre_empresa !!}
      </h4>
      <p class="card-text"></p>
      <p class="card-text">
          {!! '<b>Razón social:</b> ' . $actividad->profesor->empresa->razon_social  . 
        ' | <b>CIF:</b> ' . $actividad->profesor->empresa->cif . ' | <b>Dirección:</b> ' .
        $actividad->profesor->empresa->direccion . " , " . $actividad->profesor->empresa->cp . " ," . 
        $actividad->profesor->empresa->actividad . " (" . $actividad->profesor->empresa->provincia . "), " . 
        $actividad->profesor->empresa->pais !!}
      </p>
      <p class="card-text">
        {!! '<b>Web:</b> ' . $actividad->profesor->empresa->web . ' | <b>Actividad:</b> ' .
         $actividad->profesor->empresa->actividad .  '| <b>Teléfono:</b> ' .
          $actividad->profesor->empresa->telefono . '| <b>Fax:</b> ' .  $actividad->profesor->empresa->fax !!}
      </p>
      <div class="row">
          @foreach($contactos as $contacto)  
          <div class="col-sm-4">
          <div class="card text-white bg-primary mb-3">
              <div class="card-header">Contacto</div>
              <div class="card-body">
                <h4 class="card-title">
                  {!! $contacto->nombre_completo !!}
                </h4>
                <p class="card-text">
                  {!! '<b>Telefono:</b> ' . $contacto->telefono !!}
                </p>
                <p class="card-text">
                  {!! '<b>Móvil:</b> ' . $contacto->movil !!}
                </p>
                <p class="card-text">
                    {!! '<b>Correo:</b> ' . $contacto->correo !!}
                    </p>
                    <p class="card-text">
                      {!!'<b>Puesto:</b><i> ' . $contacto->puesto.'</i>' !!}
                    </p>
              </div>
            </div>
          </div>
      @endforeach
        </div>
    </div>
  </div>
  @endif
  <div class="card text-white bg-primary mb-12">
    <div class="card-header">Tesorería</div>
    <div class="card-body">
      <h4 class="card-title">
        Coste estimado máximo: {!! $actividad->coste_estimado_maximo !!} €</h4>
        @if (count($metalicos)<1)
        <h6 class="text-center">No hay datos de metálicos</h6>
        @else
        <h6 class="text-center">Metálicos</h6>
          <table  class="table table-bordered">
            <thead class="thead-dark">
                <tr class="text-center">
                    <th class="align-middle">Concepto</th>
                    <th class="align-middle">Importe</th>
                    <th class="align-middle">Notas</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($metalicos as $metalico)
                <tr>
                  <td>{{ $metalico->concepto }}</td>
                  <td class="text-right">{{ $metalico->importe }} €</td>
                  <td>{{ $metalico->notas }}</td>
                @endforeach
              </tr>
            </tbody>
          </table>
        @endif
      </div>
    </div>
</div>
<br>
@endsection
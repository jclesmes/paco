@extends('master')
@section('title', 'Borrar actividad: '.$actividad->nombre)
@section('content')
<div class="container">
        <h1 class="text-center text-warning">Actividad a borrar</h1>
        <h2 class="text-center text-danger">Atención esta acción no se puede revertir, será eliminado para siempre</h2>
        <p>ACTIVIDAD A BORRAR:</p>
        <p>{!! $actividad->nombre !!}</p>
        <a class="btn btn-danger" href="{!! action('ActividadesController@destroy', $actividad->id) !!}">BORRAR</a>
</div>
<br>
@endsection
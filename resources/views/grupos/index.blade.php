@extends('master')
@section('title', 'Grupos existentes')
@section('content')
<div class="container">
    @if ($grupos->isEmpty())
        <h1 class="text-center text-warning">No hay grupos</h1>
    @else
    <h1 class="text-center text-success">Datos de los grupos</h1>
        <table class="table">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Editar</th>
                    <th>Borrar</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($grupos as $grupo)
                <tr>
                    <td>{!! $grupo->nombre !!}</td>
                    <td><a class="btn btn-success" href="{!! action('GruposController@edit', $grupo->id) !!}"><i class="fas fa-edit"> editar</a></td>
                    <td><a class="btn btn-danger" href="{!! action('GruposController@borrar', $grupo->id) !!}"><i class="fas fa-trash-alt"> borrar</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    @endif
</div>
<br>
@endsection
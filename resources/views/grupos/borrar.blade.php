@extends('master')
@section('title', 'Borar grupo')
@section('content')
<div class="container">
    <h1 class="text-center text-warning">Grupo a borrar</h1>
    <h2 class="text-center text-danger">Atención esta acción no se puede revertir, será eliminado para siempre</h2>
    <h3>DATO:</h3>
    <p><strong>{!! $grupo->nombre !!}</strong></p>
    <a class="btn btn-danger" href="{!! action('GruposController@destroy', $grupo->id) !!}">BORRAR</a>
    @if(count($actividades) < 0)
        <div class="alert alert-warning">
            <strong>Sin actividades</strong>
        </div>                                      
    @else
        <h3>Actividades que se borrarán:</h3>
        <ol>
        @foreach($actividades as $actividad)
            <li>Actividad: {{ $actividad->nombre }}</li>
        @endforeach
        </ol>
    @endif
</div>
<br>
@endsection
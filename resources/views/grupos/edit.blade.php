@extends('master')
@section('title', 'Editar grupo') 
@section('content')
<div class="container">
    <h1 class="text-center text-success">Datos del grupo a modificar</h1>
    <div class="form-group">
        {!! Form::open(['action' => ['GruposController@update',  $grupo->id]]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('nombre') !!}
        {!! Form::text('nombre',  $grupo->nombre, ['class' => 'form-control', 'value' => $grupo->nombre]) !!}
    </div>
    {!! Form::hidden('id', $grupo->id)!!}
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}
</div>
<br>
@endsection
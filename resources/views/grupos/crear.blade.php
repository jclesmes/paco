@extends('master')
@section('title', 'Crear nuevo grupo') 
@section('content')
<div class="container">
        <h1 class="text-center text-success">Datos del grupo</h1>
    <div class="form-group">
        {!! Form::open(['url' => 'grupos/crear', 'method'=> 'post']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('nombre', 'Nombre del grupo: ') !!}
        {!! Form::text('nombre', '', ['class' => 'form-control']) !!}
    </div>
        {!! Form::submit('Crear grupo', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}
</div>
<br>
@endsection
@extends('master')
@section('title', 'Profesores existentes')
@section('content')
<div class="container">
    @if ($profesores->isEmpty())
        <h1 class="text-center text-warning">No hay profesores</h1>
    @else
        <h1 class="text-center text-success">Datos de los profesores</h1>
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Teléfono</th>
                    <th>Correo</th>
                    <th>Empresa asociado</th>
                    <th>Editar</th>
                    <th>Borrar</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($profesores as $profesor)
                <tr>
                    <td>{!! $profesor->nombre_completo !!}</td>
                    <td>{!! $profesor->telefono !!}</td>
                    <td>{!! $profesor->correo !!}</td>
                    <td>{!! $profesor->empresa->nombre_empresa !!}</td>
                    <td><a class="btn btn-success" href="{!! action('ProfesorsController@edit', $profesor->id) !!}"><i class="fas fa-edit"></i> editar</a></td>
                    <td><a class="btn btn-danger" href="{!! action('ProfesorsController@borrar', $profesor->id) !!}"><i class="fas fa-trash-alt"> borrar</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $profesores->render() }}
    @endif
</div>
<br>
@endsection
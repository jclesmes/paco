@extends('master')
@section('title', 'Borrar profesor')
@section('content')
<div class="container">
        <h1 class="text-center text-warning">Profesor a borrar</h1>
        <h2 class="text-center text-danger">Atención esta acción no se puede revertir, será eliminado para siempre</h2>
        <p>DATO:</p>
        <p><strong>{!! $profesor->nombre_completo !!}</strong></p>
        <a class="btn btn-danger" href="{!! action('ProfesorsController@destroy', $profesor->id) !!}">BORRAR</a>
</div>
<br>
@endsection
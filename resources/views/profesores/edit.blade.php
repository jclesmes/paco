@extends('master')
@section('title', 'Profesor modificar') 
@section('content')
<div class="container">
    <h1 class="text-center text-success">Datos del profesor a modificar</h1>
    <div class="form-group">
        {!! Form::open(['action' => ['ProfesorsController@update',  $profesor->id]]) !!}
    <div class="form-group">
        {!! Form::label('nombre_completo', 'Nombre completo') !!}
        {!! Form::text('nombre_completo',  $profesor->nombre_completo, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('telefono', 'Teléfono') !!}
        {!! Form::number('telefono',  $profesor->telefono, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('correo', 'Correo') !!}
        {!! Form::email('correo',  $profesor->correo, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
            {!! Form::label('empresa_id', 'Empresa') !!}
            {!! Form::select('empresa_id',  $empresas, $profesor->empresa->id , ['placeholder' => 'Selecciona la empresa ...', 'class' => 'form-control']) !!}
    </div>
    {!! Form::hidden('id', $profesor->id)!!}
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}
</div>
</br>
@endsection
@extends('master')
@section('title', 'Nuevo profesor') 
@section('content')
<div class="container">
    <h1 class="text-center text-success">Datos del nuevo profesor</h1>
    @foreach ($errors->all() as $error)
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>{{ $error }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endforeach
    <div class="form-group">
        {!! Form::open(['url' => 'profesores/crear', 'method'=> 'post']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('nombre_completo', 'Nombre completo del profesor: ') !!}
        {!! Form::text('nombre_completo', '', ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('telefono', 'Teléfono: ') !!}
        {!! Form::number('telefono', '', ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('correo', 'Correo: ') !!}
        {!! Form::text('correo', '', ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('empresa_id', 'Empresa a la que pertenece') !!}
        {!! Form::select('empresa_id', $empresas, null, ['placeholder' => 'Selecciona su empresa ...', 'class' => 'form-control']) !!}
    </div>
        {!! Form::submit('Crear profesor', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}
    </div>
<br>
@endsection
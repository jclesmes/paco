@extends('master')
@section('title', 'Crear dosier')
@section('content')
<div class="container">
    @foreach ($errors->all() as $error)
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>{{ $error }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endforeach
    <h1 class="text-center text-success">Rellenar dosier</h1>
    {!! Form::open(['url' => 'descargas/pdf/dosier/crear/'.$actividad->id, 'method' => 'post', 'files' => true]) !!}
    @php
        $descripcion = '';
        $contenido =  '';

    if($actividad->dosier != null){
        if ($actividad->dosier->descripcion_dosier != null)
            $descripcion = $actividad->dosier->descripcion_dosier;
        if ($actividad->dosier->contenido_dosier != null)
            $contenido = $actividad->dosier->contenido_dosier;
    }

    @endphp
    <!-- PRIMERA FILA -->
    <div class="form-group">
        {!! Form::label('descripcion_dosier', 'Descripción dosier') !!}
        {!! Form::textarea('descripcion_dosier', $descripcion, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('contenido_dosier', 'Contenido dosier') !!}
        {!! Form::textarea('contenido_dosier', $contenido, ['class' => 'form-control']) !!}
    </div>
        {!! Form::hidden('actividad_id', request()->route('id')) !!}
    <div class="form-group">
        {{ Form::label('url_imagen', 'Imágen') }}
        {{ Form::file('url_imagen') }}    
    </div>
    <div class="form-group">
        {!! Form::submit('Guardar dosier', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}
    </div>
    </div>
</div>
<br> 
@endsection
@section('js')
    <script src="{{ asset('/ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.config.height = 400;
        CKEDITOR.config.width = 'auto';
        CKEDITOR.replace('contenido_dosier');
    </script>
@endsection
@extends('master')
@section('title', 'Aula a crear') 
@section('content')
<div class="container">
        <h1 class="text-center text-success">Datos de la actividads</h1>
    <div class="form-group">
        {!! Form::open(['url' => 'aulas/crear', 'method'=> 'post']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('nombre', 'Nombre del aula: ') !!}
        {!! Form::text('nombre', '', ['class' => 'form-control']) !!}
    </div>
    {!! Form::submit('Crear aula', ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}
</div>
<br>
@endsection
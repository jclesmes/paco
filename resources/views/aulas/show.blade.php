@extends('master')
@section('title', 'Aula: '.$aula->nombre) 
@section('content')
<div class="container">
        <h1 class="text-center text-success">Datos del aula</h1>
        <h2>{!! $aula->nombre !!}</h2>
        <p>
                <a class="btn btn-info" href="{{ route('calendarioAula') }}/{{$aula->id}}">Calendario de {{ $aula->nombre }}</a>
        </p>
        <table class="table table-dark">
                <tr>
                        <th scope="row">Actividad</th>
                        <th scope="row">Grupo</th>
                        <th scope="row">Fecha inicio</th>
                        <th scope="row">Fecha fin</th>
                        <th scope="row">Días</th>
                        <th scope="row">Hora inicio</th>
                        <th scope="row">Hora fin</th>
                </tr>
                @foreach($actividades as $actividad)
                <tr>
                        <td>{{$actividad->nombre}}</td>
                        <td>{{$actividad->grupo->nombre}}</td>
                        <td>{{ Carbon\Carbon::parse($actividad->fecha_inicio)->format('Y-m-d') }}</td>
                        <td>{{ Carbon\Carbon::parse($actividad->fecha_fin)->format('Y-m-d') }}</td>
                        <td>{{$actividad->dias}}</td>
                        <td>{{ Carbon\Carbon::parse( $actividad->hora_inicio)->format('G:i') }}</td>
                        <td>{{ Carbon\Carbon::parse( $actividad->hora_fin)->format('G:i') }}</td>
                </tr>
                @endforeach
        </table>
</div>
<br>
@endsection
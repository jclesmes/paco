@extends('master')
@section('title', 'Borrar aula')
@section('content')
<div class="container">
        <h1 class="text-center text-warning">Aula a borrar</h1>
        <h2 class="text-center text-danger">Atención esta acción no se puede revertir, será eliminado para siempre</h2>
        <p>DATO:</p>
        <p>{!! $aula->nombre !!}</p>
        <a class="btn btn-danger" href="{!! action('AulasController@destroy', $aula->id) !!}">BORRAR</a>
</div>
<br>
@endsection
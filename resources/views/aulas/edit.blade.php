@extends('master')
@section('title', 'Editar '.$aula->nombre) 
@section('content')
<div class="container">
    <h1 class="text-center text-success">Datos del aula a modificar</h1>
        <div class="form-group">
            {!! Form::open(['action' => ['AulasController@update',  $aula->id]]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('nombre') !!}
            {!! Form::text('nombre',  $aula->nombre, ['class' => 'form-control', 'value' => $aula->nombre]) !!}
        </div>
    {!! Form::hidden('id', $aula->id)!!}
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}
</div>
<br>
@endsection
@extends('master')
@section('title', 'Aulas existentes')
@section('content')
<div class="container">
    @if ($aulas->isEmpty())
        <h1 class="text-center text-warning">No hay aulas</h1>
    @else
        <h1 class="text-center text-success">Datos de las aulas</h1>
        <table class="table">
            <thead>
                <tr>
                    <th>OCUPACIÓN</th>
                    <th>Nombre</th>
                    <th>Editar</th>
                    <th>Borrar</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($aulas as $aula)
                <tr>
                    <td><a class="btn btn-dark" href="{!! action('AulasController@show', $aula->id) !!}">VER</a></td>
                    <td>{!! $aula->nombre !!}</td>
                    <td><a class="btn btn-success" href="{!! action('AulasController@edit', $aula->id) !!}"><i class="fa fa-file-text"></i> editar</a></td>
                    <td><a class="btn btn-danger" href="{!! action('AulasController@borrar', $aula->id) !!}"><i class="fa fa-trash"> borrar</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    @endif
</div>
<br>
@endsection
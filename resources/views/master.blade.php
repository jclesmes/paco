<!DOCTYPE html>
    <html lang="{{ app()->getLocale() }}">

    <head>
        <title>@yield('title')</title>
        <!-- Required meta tags -->
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="author" href="{ asset('/humans.txt') }}" />
        
        <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('/img/apple-touch-icon.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/img/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/img/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ asset('/img/manifest.json') }}">
        <link rel="mask-icon" href="{{ asset('/img/safari-pinned-tab.svg') }}" color="#5bbad5">
        <link href="{{ asset('/img/favicon.ico') }}" rel="icon">
        
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" href="{{ asset('/css/jquery-ui.multidatespicker.css') }}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.6/css/all.css">
        <link rel="stylesheet" href="{{ asset('/css/bootstrap-datetimepicker.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/sticky-footer.css') }}">
        <!-- Otros estilos -->
        @yield('estilos')
    </head>

    <body>
        @include('navbar')

        @if(session('sin_campania'))
        <div class="alert alert-danger fade show" role="alert">
                <strong>{{ session('sin_campania') }}</strong>
        </div>
        @endif

        @if (session('mensaje'))
        <div id="success-alert" class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('mensaje') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
        </div>
        @endif
        <a class="calendario-fijo" href="{{ url('calendario') }}">
                <i class="far fa-calendar-alt fa-3x"></i>
        </a>
        @yield('content')
    <footer class="footer" id="myFooter">
        <hr>
            <div class="container">
                    <div class="row">
                        <div class="col-sm-3 myCols">
                            <h5>Empecemos</h5>
                            <ul>
                                <li><a href="{{ url('descargas') }}">Descargas</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-3 myCols">
                            <h5>Sobre nosotros</h5>
                            <ul>
                                <li><a href="#">Informacion</a></li>
                                <li><a href="#">Contacta</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-3 myCols">
                            <h5>Soporte</h5>
                            <ul>
                                <li><a href="#">FAQ</a></li>
                                <li><a href="#">Ayuda</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-3 myCols">
                            <h5>Legal</h5>
                            <ul>
                                <li><a href="#">Terminos de uso</a></li>
                                <li><a href="#">Privacidad</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
        <div class="social-networks">
            <a href="https://twitter.com/rozasjoven" class="twitter"><i class="fab fa-twitter"></i></a>
            <a href="https://www.facebook.com/Rozasjoven" class="facebook"><i class="fab fa-facebook"></i></a>
            <a href="https://plus.google.com/+rozasjoven" class="google"><i class="fab fa-google-plus"></i></a>
        </div>
        <div class="footer-copyright">
            <p>© 2017-<?php echo date('Y') ?> Concejalia de la Juventud | Las Rozas  </p>
        </div>
        <a id="return-to-top" class="go-top2" href="#"><i class="fas fa-chevron-up"></i></a>        
    </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="{{ asset('/js/jquery-ui.multidatespicker.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
      crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.3/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.3/locale/es.js"></script>
    <script src="{{ asset('/js/bootstrap-datetimepicker.js') }}"></script>
    <script src="{{ asset('/js/datepicker-es.js') }}"></script>
    <script src="{{ asset('/js/tiempo.js') }}"></script>
    <script src="{{ asset('/js/laravel.js') }}"></script>
    @yield('js')
    <script>
		$(document).ready(function() {
			// Show or hide the sticky footer button
			$(window).scroll(function() {
				if ($(this).scrollTop() > 50) {
                    $('.go-top').fadeIn(200);
                    $('.calendario-fijo').css("color", "black");
				} else {
                    $('.go-top').fadeOut(200);
                    $('.calendario-fijo').css("color", "white");
				}
			});

			// Animate the scroll to top
			$('.go-top').click(function(event) {
				event.preventDefault();

				$('html, body').animate({scrollTop: 0}, 300);
			})
        });
        
        $(window).scroll(function() {
            if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
                $('#return-to-top').fadeIn(200);    // Fade in the arrow
            } else {
                $('#return-to-top').fadeOut(200);   // Else fade out the arrow
            }
        });
        $('#return-to-top').click(function() {      // When arrow is clicked
            $('body,html').animate({
                scrollTop : 0                       // Scroll to top of body
            }, 500);
        });
	</script>
    </body>

    </html>
@extends('master')
@section('title', 'Empresas existentes')
@section('content')
<div class="container-fluid">
    @if ($empresas->isEmpty())
        <h1 class="text-center text-warning">No hay empresas</h1>
    @else
        <h1 class="text-center text-success">Datos de las empresas</h1>
        <div class="horizontal">
        <table id="tabla" class="table table-bordered">
            <thead class="thead-dark">
                <tr>
                    <th>Más datos</th>
                    <th>Nombre</th>
                    <th>CIF</th>
                    <th>Razón social</th>
                    <th>Dirección</th>
                    <th>Provincia</th>
                    <th>Ciudad</th>
                    <th>País</th>
                    <th>CP</th>
                    <th>Web</th>
                    <th>Actividad</th>
                    <th>Teléfono</th>
                    <th>Fax</th>
                    <th>Editar</th>
                    <th>Borrar</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($empresas as $empresa)
                <tr>
                    <td>
                        <a class="btn btn-primary" href="{!! action('EmpresasController@show', $empresa->id) !!}">DATOS</a>
                        <a class="btn btn-success" href="{!! action('EmpresasController@edit', $empresa->id) !!}"><i class="fa fa-pencil"></i></a>
                        <a class="btn btn-danger" href="{!! action('EmpresasController@borrar', $empresa->id) !!}"><i class="fa fa-trash"></i></a>
                    </td>
                    <td>{!! $empresa->nombre_empresa !!}</td>
                    <td>{!! $empresa->cif !!}</td>
                    <td>{!! $empresa->razon_social !!}</td>
                    <td>{!! $empresa->direccion !!}</td>
                    <td>{!! $empresa->provincia !!}</td>
                    <td>{!! $empresa->ciudad !!}</td>
                    <td>{!! $empresa->pais !!}</td>
                    <td>{!! $empresa->cp !!}</td>
                    <td>{!! $empresa->web !!}</td>
                    <td>{!! $empresa->actividad !!}</td>
                    <td>{!! $empresa->telefono !!}</td>
                    <td>{!! $empresa->fax !!}</td>
                    <td><a class="btn btn-success" href="{!! action('EmpresasController@edit', $empresa->id) !!}"><i class="fas fa-edit"></i> editar</a></td>
                    <td><a class="btn btn-danger" href="{!! action('EmpresasController@borrar', $empresa->id) !!}"><i class="fas fa-trash-alt"></i> borrar</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $empresas->render() }}
    @endif
</div>
</div>
</br>
@endsection
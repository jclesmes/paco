@extends('master')
@section('title', 'Crear nueva empresa')
@section('content')
<div class="container">
    @foreach ($errors->all() as $error)
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>{{ $error }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endforeach
    <h1 class="text-center text-success">Empresa a crear</h1>
    {!! Form::open(['url' => 'empresas/crear', 'method'=> 'post']) !!}
    <!-- PRIMERA FILA -->
    <div class="form-group">
        {!! Form::label('nombre_empresa', 'Nombre') !!}
        {!! Form::text('nombre_empresa', '', ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
            {!! Form::label('razon_social', 'Razón social') !!}
            {!! Form::text('razon_social', '', ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
            {!! Form::label('direccion', 'Dirección') !!}
            {!! Form::text('direccion', null, ['class' => 'form-control']) !!}
    </div>
    <!-- SEGUNDA FILA -->
    <div class="form-row">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('cif', 'CIF') !!}
                {!! Form::text('cif', '', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('cp', 'CP') !!}
                {!! Form::number('cp', '', ['class' => 'form-control', 'pattern' => '[LMXJVSD,]{1,}', 'title' => 'Sólo válido L M X J V S D separados por comas']) !!}
            </div>
            <div class="form-group">
                 {!! Form::label('fax', 'Fax') !!}
                {!! Form::number('fax', null, ['class' => 'form-control']) !!}
            </div>    
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <div class="form-group">
                    {!! Form::label('provincia', 'Provincia') !!}
                    {!! Form::text('provincia', null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="form-group">
                    {!! Form::label('ciudad', 'Ciudad') !!}
                    {!! Form::text('ciudad', null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="form-group">
                    {!! Form::label('pais', 'País') !!}
                    {!! Form::text('pais', 'España', ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                {!! Form::label('actividad', 'Actividad') !!}
                {!! Form::text('actividad', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('telefono', 'Teléfono') !!}
                {!! Form::number('telefono', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                {!! Form::label('web', 'Página web') !!}
                {!! Form::text('web', '', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                    {!! Form::label('correo', 'Correo electrónico') !!}
                    {!! Form::text('correo', '', ['class' => 'form-control']) !!}
            </div>
        </div>
            <div class="form-group">
                {!! Form::submit('Crear empresa', ['class' => 'btn btn-primary']) !!}
                {!! Form::close() !!}
        </div>
    </div>
    </div>
<br> 
@endsection
@extends('master')
@section('title', 'Editar empresa')
@section('content')
<div class="container">
    <h1 class="text-center text-success">Datos de la empresa a modificar</h1>
    <div class="form-group">
            {!! Form::model($empresa, ['action' => ['EmpresasController@update', $empresa->id]]) !!}
        <div class="form-group">
            {!! Form::label('nombre_empresa', 'Nombre: ') !!}
            {!! Form::text('nombre_empresa' , $empresa->nombre_empresa, ['class' => 'form-control']); !!}
        </div>
        <div class="form-group">
            {!! Form::label('cif', 'CIF: ') !!}
            {!! Form::text('cif' , $empresa->cif, ['class' => 'form-control']); !!}
        </div>
        <div class="form-group">
            {!! Form::label('razon_social', 'Razón social: ') !!}
            {!! Form::text('razon_social' , $empresa->razon_social, ['class' => 'form-control']); !!}
        </div>
        <div class="form-group">
            {!! Form::label('direccion', 'Dirección: ') !!}
            {!! Form::text('direccion' , $empresa->direccion, ['class' => 'form-control']); !!}
        </div>
        <div class="form-group">
            {!! Form::label('provincia', 'Provincia: ') !!}
            {!! Form::text('provincia' , $empresa->provincia, ['class' => 'form-control']); !!}
        </div>
        <div class="form-group">
            {!! Form::label('pais', 'Pais: ') !!}
            {!! Form::text('pais' , $empresa->pais, ['class' => 'form-control']); !!}
        </div>
        <div class="form-group">
            {!! Form::label('web', 'Web: ') !!}
            {!! Form::text('web' , $empresa->web, ['class' => 'form-control']); !!}
        </div>      
        <div class="form-group">
            {!! Form::label('correo', 'Correo: ') !!}
            {!! Form::text('correo' , $empresa->correo, ['class' => 'form-control']); !!}
        </div> 
        <div class="form-group">
            {!! Form::label('actividad', 'Actividad: ') !!}
            {!! Form::text('actividad' , $empresa->actividad, ['class' => 'form-control']); !!}
        </div>   
        <div class="form-group">
            {!! Form::label('telefono', 'Teléfono: ') !!}
            {!! Form::text('telefono' , $empresa->telefono, ['class' => 'form-control']); !!}
        </div> 
        <div class="form-group">
            {!! Form::label('fax', 'Fax: ') !!}
            {!! Form::text('fax' , $empresa->fax, ['class' => 'form-control']); !!}
        </div>   
        <div class="form-group">
            {{--  {!! Form::hidden('id', $empresa->id)!!}  --}}
            {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
            {!! Form::close() !!}
        </div>  
        {!! Form::close() !!}
    </div>



</div>
<br>
@endsection
@extends('master') 
@section('title', 'Empresa '.$empresa->nombre_empresa ) 
@section('content')
<div class="container">
  <h1 class="text-center text-success">Datos de la empresa</h1>
  <div class="card-deck">
  <div class="card text-white bg-dark w-100">
    <div class="card-header">Empresa</div>
      <div class="card-body">
        <h4 class="card-title">
          {!! $empresa->nombre_empresa !!}
        </h4>
        <p class="card-text">
          {!! 'Razón social: ' . $empresa->razon_social  . 
        ' | CIF: ' . $empresa->cif . ' | Dirección: ' .
          $empresa->direccion . " , " . $empresa->cp . " ," . 
          $empresa->empresa . " (" . $empresa->provincia . "), " . 
          $empresa->pais !!}
        </p>
        <p class="card-text">
          {!! 'Web: ' . $empresa->web . ' | Correo: ' .
          $empresa->correo . ' | Actividad: ' .
          $empresa->actividad .  '| Teléfono: ' .
          $empresa->telefono . '| Fax: ' .  $empresa->fax !!}
        </p>
      </div>
      <div class="card-header">Contactos</div>
        <div class="row">
          @foreach($contactos as $contacto)  
            <div class="col-sm-4">
              <div class="card text-white bg-primary mb-3">
                <div class="card-header">Contacto</div>
                <div class="card-body">
                  <h4 class="card-title">
                    {!! $contacto->nombre_completo !!}
                  </h4>
                  <p class="card-text">
                    {!! 'Telefono: ' . $contacto->telefono !!}
                  </p>
                  <p class="card-text">
                    {!! 'Móvil: ' . $contacto->movil !!}
                  </p>
                  <p class="card-text">
                    {!! 'Correo: ' . $contacto->correo !!}
                  </p>
                  <p class="card-text">
                    {!!'Puesto: ' . $contacto->puesto !!}
                  </p>
                </div>
              </div>
            </div>
          @endforeach
        </div>

        <div class="card-header">Profesores</div>
          <div class="row">
            @foreach($profesores as $profesor)  
              <div class="col-sm-4">
              <div class="card text-white bg-danger mb-3">
                <div class="card-header">Profesor</div>
                <div class="card-body">
                  <h4 class="card-title">
                    {!! $profesor->nombre_completo !!}
                  </h4>
                  <p class="card-text">
                    {!! 'Telefono: ' . $profesor->telefono !!}
                  </p>
                  <p class="card-text">
                      {!! 'Correo: ' . $profesor->correo !!}
                      </p>

                </div>
                </div>
              </div>
            @endforeach
          </div>
        </div>
     </div>
  </div>
<br>
@endsection
@extends('master') 
@section('title', 'Borar empresa') 
@section('content')
<div class="container">
        <h1 class="text-center text-warning">Empresa a borrar</h1>
        <h2 class="text-center text-danger">Atención esta acción no se puede revertir, serán eliminados para siempre la empresa y sus contactos y profesores</h2>
        <h3>DATO:</h3>
        <p>
                <strong>{!! $empresa->nombre_empreas !!}</strong>
        </p>
        <a class="btn btn-danger" href="{!! action('EmpresasController@destroy', $empresa->id) !!}">BORRAR</a>
        <br> 
        @if(count($profesores) < 1) 
                <div class="alert alert-warning"><strong>Sin profesores</strong></div>
        @else
                <h3>Profesores que se borrarán:</h3>
                <ol>
                        @foreach($profesores as $profesor)
                        <li>Profesor: {{ $profesor->nombre_completo }}</li>
                        @endforeach
                </ol>
        @endif 
        @if(count($contactos) < 1) 
                <div class="alert alert-warning"><strong>Sin contactos</strong></div>
        @else
                <h3>Contactos que se borrarán:</h3>
        <ol>
                @foreach($contactos as $contacto)
                <li>Contacto: {{ $contacto->nombre_completo }}</li>
                @endforeach
        </ol>
        @endif
</div>
<br> 
@endsection
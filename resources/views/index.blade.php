@extends('master')
@section('title', 'Actividades | Rozas Joven')
@section('content')
<div class="container-fluid">
    @if (count($actividades)<1)
    <h1 class="text-center text-warning">No hay actividades</h1>
    {!! Form::open(['action' => ['ActividadesController@index2'],'method'=>'post','class'=>'form-inline justify-content-center' ]) !!}
        <div class="form-row">
            <div class="input-group align-items-center">
                {!! Form::select('id', $categorias, null, ['placeholder' => 'TODAS', 'class' => 'form-control']) !!}   
            </div>
        </div>
        <div class="col-auto">
            {!! Form::submit('Filtrar', ['class' => 'btn btn-primary']) !!}
            <a class="btn btn-secondary" href="{!! route('excelTodos') !!}"> <i class="fas fa-download"></i> EXCEL</a>
        </div>
            {!! Form::close() !!}
    </div>
    <br>
    <p class="text-center"><a class="btn btn-primary" href="{{ route('actividadesCrear') }}">Crear nueva actividad</a></p>
    @else
    <div class="container-fluid">
        <h1 class="text-center text-success">Datos de las actividades</h1>
        {!! Form::open(['action' => ['ActividadesController@index2'],'method'=>'post','class'=>'form-inline justify-content-center' ]) !!}
            <div class="form-row">
                <div class="input-group align-items-center">
                    {!! Form::select('id', $categorias, null, ['placeholder' => 'TODAS', 'class' => 'form-control']) !!}   
                </div>
            </div>
            <div class="col-auto">
                {!! Form::submit('Filtrar', ['class' => 'btn btn-primary']) !!}
                <a class="btn btn-secondary" href="{!! route('excelTodos') !!}"> <i class="far fa-file-excel"></i> EXCEL</a>
            </div>       
        {!! Form::close() !!}
        </div>
        <br>
        <div class="horizontal">
            <table id="tabla" class="table table-bordered">
                <thead class="thead-dark">
                    <tr class="text-center">
                        <th class="align-middle">#</th>
                        <th class="align-middle">Actividad</th>
                        <th class="align-middle">Categoria</th>
                        <th class="align-middle">Grupo</th>
                        <th class="align-middle">Aula</th>
                        <th class="align-middle">Responsable</th>
                        <th class="align-middle">Fecha de inicio</th>
                        <th class="align-middle">Fecha de fin</th>
                        <th class="align-middle">Días</th>
                        <th class="align-middle">Horas</th>
                        <th class="align-middle">Precio</th>
                        <th class="align-middle">Coste máx.</th>
                        <th class="align-middle">Hora de inicio</th>
                        <th class="align-middle">Hora de fin</th>
                        <th class="align-middle">Par. Mín</th>
                        <th class="align-middle">Par. Máx</th>
                        <th class="align-middle">Edad mín</th>
                        <th class="align-middle">Edad máx</th>
                        <th class="align-middle">Fecha fin inscripciones</th>
                        <th class="align-middle">Incluye</th>
                        <th class="align-middle">Texto</th>
                        <th class="align-middle">Lugar</th>
                        <th class="align-middle">Observaciones</th>
                        <th class="align-middle">Borrar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($actividades as $actividad)
                        <tr class="
                        @switch($actividad->categoria_id)
                            @case(1)
                                aire
                                @break
                            @case(2)
                                musica
                                @break
                            @case(3)
                                imagen
                                @break    
                            @case(4)
                                habilidades
                                @break    
                            @case(5)
                                formacion
                                @break 
                            @case(6)
                                ocio
                                @break 
                            @case(7)
                                certamen
                                @break  
                            @default
                                desconocido     
                        @endswitch
                    text-center">
                            <td>
                                <a title="Descargar Word" class="text-primary" href="descargas/word/{{$actividad->id}}"><i class="far fa-file-word"></i></a>
                                <a title="Descargar PDF" class="text-danger" href="descargas/pdf/{{$actividad->id}}"><i class="fas fa-file-pdf"></i></a>
                                <a title="Editar actividad" class="text-success" href="{!! route('actividadesEditar', $actividad->id) !!}"><i class="fas fa-edit"></i></a>
                                <a title="Duplicar actividad" class="text-warning" href="{!! route('actividadesDuplicar', $actividad->id) !!}"><i class="fas fa-copy"></i></a>
                            </td>
                            <td>
                               <a title="Ver los datos de la actividad" href="{!! route('actividadesVer', $actividad->id) !!}"><small><i class="fas fa-certificate"></i></small> {{ $actividad->nombre}}</a> 
                            </td>
                            <td>
                                <a href="{!! route('actividadesEditar', $actividad->id) !!}">{{ $actividad->categoria->nombre }}</a>
                            </td>
                            <td>
                                <a href="{!! route('actividadesEditar', $actividad->id) !!}">{{ $actividad->grupo->nombre }}</a>
                            </td>
                            <td>
                                <a title="Ver actividades en aula" href="{!! route('aulasVer', $actividad->aula->id) !!}"><small><i class="fas fa-certificate"></i></small> {{ $actividad->aula->nombre }}
                            </td>
                            <td>
                                <a title="Ver actividades del responsable" href="{!! route('responsablesVer', $actividad->responsable->id) !!}"><small><i class="fa fa-certificate"></i></small> {{ $actividad->responsable->nombre }}
                            </td>
                            <td>
                                <a href="{!! route('actividadesEditar', $actividad->id) !!}">{{ $actividad->fecha_inicio }}</a>
                            </td>
                            <td>
                                <a href="{!! route('actividadesEditar', $actividad->id) !!}">{{ $actividad->fecha_fin }}</a>
                            </td>
                            <td>
                                <a href="{!! route('actividadesEditar', $actividad->id) !!}">{{ $actividad->dias }}</a>
                            </td>
                            <td>
                                <a href="{!! route('actividadesEditar', $actividad->id) !!}">{{ $actividad->horas }} h</a>
                            </td>
                            <td>
                                <a href="{!! route('actividadesEditar', $actividad->id) !!}">{{ $actividad->precio }} €</a>
                            </td>
                            <td>
                                <a href="{!! route('actividadesEditar', $actividad->id) !!}">{{ $actividad->coste_estimado_maximo }} €</a>
                            </td>
                            <td>
                                <a href="{!! route('actividadesEditar', $actividad->id) !!}">{{  Carbon\Carbon::parse( $actividad->hora_inicio)->format('G:i') }}h</a>
                            </td>
                            <td>
                                <a href="{!! route('actividadesEditar', $actividad->id) !!}">{{  Carbon\Carbon::parse( $actividad->hora_fin)->format('G:i') }}h</a>
                            </td>
                            <td>
                                <a href="{!! route('actividadesEditar', $actividad->id) !!}">{{ $actividad->par_min }}</a>
                            </td>
                            <td>
                                <a href="{!! route('actividadesEditar', $actividad->id) !!}">{{ $actividad->par_max }}</a>
                            </td>
                            <td>
                                <a href="{!! route('actividadesEditar', $actividad->id) !!}">{{ $actividad->edad_minima }}</a>
                            </td>
                            <td>
                                <a href="{!! route('actividadesEditar', $actividad->id) !!}">{{ $actividad->edad_maxima }}</a>
                            </td>
                            <td>
                                <a href="{!! route('actividadesEditar', $actividad->id) !!}">{{ $actividad->fecha_inscripciones }}</a>
                            </td>
                            <td>
                                <a href="{!! route('actividadesEditar', $actividad->id) !!}">{{ $actividad->incluye }}</a>
                            </td>
                            <td>
                                <a href="{!! route('actividadesEditar', $actividad->id) !!}"> {{ $actividad->texto_previo_folleto }}</a>
                            </td>
                            <td>
                                <a href="{!! route('actividadesEditar', $actividad->id) !!}"> {{ $actividad->lugar_celebracion }}</a>
                            </td>
                            <td>
                                <a href="{!! route('actividadesEditar', $actividad->id) !!}">{{ $actividad->observaciones }}</a>
                            </td>
                            <td>
                                <a class="btn btn-danger text-white" href="{!! route('actividadesBorrar', $actividad->id) !!}"><i class="fas fa-trash-alt"></i> Borrar</a>
                            </td>
                            @endforeach
                        </tr>
                </tbody>
            </table>
        </div>
    @endif
</div>
<br>
@endsection
<!--Navbar-->

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    @if( Session::has('campaniaActiva') )
        <a class="navbar-brand" href="{!! route('inicio') !!}"><img src= "{{asset('/img/logo.png')}}">  <h3><span class="text-dark">{!! session('campaniaActiva')[0] !!}</span></h3></a>
    @else
        <a class="navbar-brand" href="{!! route('campaniasCrear') !!}"><img src= "{{asset('/img/logo.png')}}"> <h3><span class="text-danger">CREA UNA CAMPAÑA</span></h3></a>
    @endif
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
        aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav mr-auto">
            <li class="{{ (Request::is( '/') ? 'nav-item active' : 'nav-item') }} nav-item dropdown navbar-left">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">Inicio</a>
                <div class="dropdown-menu mr-sm-2" aria-labelledby="navbarDropdown">
                    <a class="{{ (Request::is( '/') ? 'dropdown-item active' : 'dropdown-item') }}" href="{!! route('inicio') !!}">Actividades</a>
                    <a class="{{ (Request::is( 'actividades/metalicos') ? 'dropdown-item active' : 'dropdown-item') }}" href="{!! route('metalicosTodos') !!}">Metálicos</a>
                </div>
            </li>
            <li class="{{ (Request::is( 'actividades/crear') ? 'nav-item active' : 'nav-item') }}">
                    <a class="nav-link" href="{!! route('actividadesCrear') !!}">Nueva actividad</a>
            </li>
            <li class="{{ (Request::is( 'campanias') ? 'nav-item active' : 'nav-item') }}">
                <a class="nav-link" href="{!! route('campanias') !!}">Campañas</a>
            </li>
            <li class="{{ (Request::is( 'aulas') ? 'nav-item active' : 'nav-item') }}">
                <a class="nav-link" href="{!! route('aulas') !!}">Aulas</a>
            </li>
            <li class="{{ (Request::is( 'responsables') ? 'nav-item active' : 'nav-item') }}">
                    <a class="nav-link" href="{!! route('responsables') !!}">Responsables</a>
            </li>
            <li class="{{ (Request::is( 'categorias') ? 'nav-item active' : 'nav-item') }}">
                    <a class="nav-link" href="{!! route('categorias') !!}">Categorias</a>
                </li>
                <li class="{{ (Request::is( 'grupos') ? 'nav-item active' : 'nav-item') }}">
                    <a class="nav-link" href="{!! route('grupos') !!}">Grupos</a>
                </li>
        </ul>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown navbar-right">
                <a class="nav-link dropdown-toggle text-white bg-dark" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">Nuev@</a>
                <div class="dropdown-menu mr-sm-2" aria-labelledby="navbarDropdown">
                    <a class="{{ (Request::is( 'actividades/crear') ? 'dropdown-item active' : 'dropdown-item') }}" href="{!! route('actividadesCrear') !!}">Actividad</a>
                    <a class="{{ (Request::is( 'campanias/crear') ? 'dropdown-item active' : 'dropdown-item') }}" href="{!! route('campaniasCrear') !!}">Campaña</a>
                    <a class="{{ (Request::is( 'aulas/crear') ? 'dropdown-item active' : 'dropdown-item') }}" href="{!! route('aulasCrear') !!}">Aula</a>
                    <a class="{{ (Request::is( 'contactos/crear') ? 'dropdown-item active' : 'dropdown-item') }}" href="{!! route('contactosCrear') !!}">Contacto</a>
                    <a class="{{ (Request::is( 'categorias/crear') ? 'dropdown-item active' : 'dropdown-item') }}" href="{!! route('categoriasCrear') !!}">Categoria</a>
                    <a class="{{ (Request::is( 'grupos/crear') ? 'dropdown-item active' : 'dropdown-item') }}" href="{!! route('gruposCrear') !!}">Grupo</a>
                    <a class="{{ (Request::is( 'responsables/crear') ? 'dropdown-item active' : 'dropdown-item') }}" href="{!! route('responsablesCrear') !!}">Responsable</a>
                    <a class="{{ (Request::is( 'profesores/crear') ? 'dropdown-item active' : 'dropdown-item') }}" href="{!! route('profesorCrear') !!}">Profesor</a>
                    <a class="{{ (Request::is( 'empresas/crear') ? 'dropdown-item active' : 'dropdown-item') }}" href="{!! route('empresaCrear') !!}">Empresa</a>
                </div>
            </li>
            <li class="{{ (Request::is( 'profesores') ? 'nav-item active' : 'nav-item') }}">
                <a class="nav-link bg-danger text-white" href="{!! route('inicio') !!}">Cierre de campaña</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">Datos empresas</a>
                <div class="dropdown-menu mr-sm-2" aria-labelledby="navbarDropdown">
                    <a class="{{ (Request::is( 'empresas') ? 'dropdown-item active' : 'dropdown-item') }}" href="{!! route('empresas') !!}">Empresas</a>
                    <a class="{{ (Request::is( 'profesores') ? 'dropdown-item active' : 'dropdown-item') }}" href="{!! route('profesores') !!}">Profesores</a>
                    <a class="{{ (Request::is( 'contactos') ? 'dropdown-item active' : 'dropdown-item') }}" href="{!! route('contactos') !!}">Contactos</a>
                 </div>
            </li>
        </ul>
    </div>
</nav>
<!--/.Navbar-->
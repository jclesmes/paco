$('.fecha').datetimepicker({
    locale: 'es',
    //format: 'DD/MM/YYYY',
    format: 'YYYY-MM-DD',
    icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-arrow-up",
        down: "fa fa-arrow-down",
        next: "fa fa-arrow-right",
        previous: "fa fa-arrow-left"
    }
});

$('.hora').datetimepicker({
    icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-arrow-up",
        down: "fa fa-arrow-down"
    },
    format: 'LT'
});


var dias = [
    "L,M,X,J,V,S,D",
    ];
$( "#dias" ).autocomplete({
    source: dias
});

$('#eliminar').submit(function(e){
     BootstrapDialog.confirm('Hi Apple, are you sure?', function(result){
            if(!result) {
               return false;
            }
     }); 
});
<?php

use Sami\Sami;
use Sami\RemoteRepository\GitHubRemoteRepository;
use Symfony\Component\Finder\Finder;

$iterator = Finder::create()
->files()
->name('*.php')
->in('./app')
;

return new Sami($iterator, array(
    'title'                => 'PACO API',
));
//return new Sami\Sami('./app');
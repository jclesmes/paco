$('.fecha').datetimepicker({
    locale: 'es',
    //format: 'DD/MM/YYYY',
    format: 'YYYY-MM-DD',
    icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-arrow-up",
        down: "fa fa-arrow-down",
        next: "fa fa-arrow-right",
        previous: "fa fa-arrow-left"
    }
});

$.datepicker.regional['es'] = {
    closeText: 'Cerrar',
    prevText: '< Ant',
    nextText: 'Sig >',
    currentText: 'Hoy',
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
    dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
    weekHeader: 'Sm',
    dateFormat: 'yy-mm-dd',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
    };
$.datepicker.setDefaults($.datepicker.regional['es']); 
$('#fecha_excluidos').multiDatesPicker({
    dateFormat: 'yy-mm-dd',
    changeMonth: true,
    changeYear: true,
    multidate: true,
}); 


/* $('#fecha_excluidos').datetimepicker({
    locale: 'es',
    format: 'YYYY-MM-DD',
    //multidate: true,
    icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-arrow-up",
        down: "fa fa-arrow-down",
        next: "fa fa-arrow-right",
        previous: "fa fa-arrow-left"
    }
}); */


$(function () {
    $('#fecha_inicio').datetimepicker({
        locale: 'es',
        //format: 'DD/MM/YYYY',
        format: 'YYYY-MM-DD',
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down",
            next: "fa fa-arrow-right",
            previous: "fa fa-arrow-left"
        }
    });
    $('#fecha_fin').datetimepicker({
        useCurrent: false, //Important! See issue #1075
        locale: 'es',
        //format: 'DD/MM/YYYY',
        format: 'YYYY-MM-DD',
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down",
            next: "fa fa-arrow-right",
            previous: "fa fa-arrow-left"
        }
    });
    $("#fecha_inicio").on("dp.change", function (e) {
        $('#fecha_fin').data("DateTimePicker").minDate(e.date);
        $('#fecha_inscripciones').val(moment(e.date).add('days', -7).format('YYYY-MM-DD'));
    });
    $("#fecha_fin").on("dp.change", function (e) {
        $('#fecha_inicio').data("DateTimePicker").maxDate(e.date);
    });

    $("#fecha_inicio").on("dp.change", function (e) {
        //moment(e.date).add('days', 5)
        $('#fecha_inscripciones').data("DateTimePicker").maxDate(moment(e.date).add('days', -7));
    });
    
});

$('.hora').datetimepicker({
    icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-arrow-up",
        down: "fa fa-arrow-down"
    },
    defaultDate:moment().hours(0).minutes(0),
    format: 'LT'
});


var dias = [
    "l",
    "m",
    'x',
    "j",
    "v",
    "s",
    "d",
    "l,m,x,j,v,s,d",
    ];
$( "#dias" ).autocomplete({
    source: dias
});

$('#eliminar').submit(function(e){
    BootstrapDialog.confirm('¿Estás seguro de eliminar?', function(result){
           if(!result) {
              return false;
           }
    }); 
});


$("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
    $("#success-alert").slideUp(500);
});